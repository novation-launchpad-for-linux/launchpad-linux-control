/*
 *  'launchpadctrl' allows you to use your Novation launchpad (NVLPD01)
 *  Copyright (C) 2014-2017  Vincent Deca. known as Bigcake@ubuntu-fr.org
 *  (user@forum not email)
 *
 *  vincent.deca@neutralite.org
 *
 *  This file is part of 'launchpadctrl'.
 *
 * 'launchpadctrl' is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  'launchpadctrl' is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with 'launchpadctrl'.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app.h"

/* Enregistrement des serveurs */
static unsigned char      SaveKeysToFile(int fd)
{
  unsigned char           a, b;
  char                    buff[9];
  t_key                   *key;
  t_lpd                   *lpd;
  size_t                  len;

  lpd = app->Launchpad[app->CurrLpd];
  buff[0] = 'K';
  buff[8] = ':';
  for (a = 0; a < GRID_VOLUME; ++a)
    for (b = 0; b < 72; ++b)
      if (!(lpd->Grid[a][b].Flags & KEY_FLG_NOT_INIT)) {
        key = &lpd->Grid[a][b];
        LOCK_KEY(key);
	/* Tempo */
	if (key->Tempo)
	  buff[1] = '1';
	else
	  buff[1] = '0';
        /* Grille */
        if (a == GRID_USER1)
          buff[2] = 'A';
        else if (a == GRID_USER2)
          buff[2] = 'B';
        else
          buff[2] = a + '0';
        /* Position dans la ligne */
        buff[3] = b % 9 + '1';
        /* Position dans la colonne */
        buff[4] = b / 9 + '1';
        /* Mode de fonctionnement */
        if (key->Flags & KEY_FLG_KEEP)
          buff[5] = 'K';
        else if (key->Flags & KEY_FLG_SINGLE)
          buff[5] = 'S';
        else if (key->Flags & KEY_FLG_LOOP)
          buff[5] = 'L';
        else if (key->Flags & KEY_FLG_HOLD_IT)
          buff[5] = 'H';
        else if (key->Flags & KEY_FLG_PAUSE)
          buff[5] = 'P';
        else
          buff[5] = '0';
        /* Mode de dépendance */
        if ((key->Flags & KEY_FLG_COL_STOP) && (key->Flags & KEY_FLG_LINE_STOP))
          buff[6] = 'B';
        else if (key->Flags & KEY_FLG_COL_STOP)
          buff[6] = 'C';
        else if (key->Flags & KEY_FLG_LINE_STOP)
          buff[6] = 'L';
        else
          buff[6] = '0';
        /* executable / audio */
        if (key->Type == KEY_TYPE_AUDIO)
          buff[7] = 'A';
        else if (key->Type == KEY_TYPE_EXEC)
          buff[7] = 'E';
        else if (key->Type == KEY_TYPE_SERVER)
          buff[7] = 'S';
        /* Ecritures dans le fichier */
        if (write(fd, buff, 9) != 9)
          goto end_SaveKeysToFile;
        /* Ecriture de l'uid du serveur */
        if (key->Type == KEY_TYPE_SERVER) {
          UShortToStr(key->Count, &buff[2]);
          if (write(fd, &buff[2], strlen(&buff[2])) == -1)
            goto end_SaveKeysToFile;
          if (write(fd, ":", 1) == -1)
            goto end_SaveKeysToFile;
        }
        if (key->CmdFile) {
	  if (key->CmdTab)
	    for (len = 0; len < key->CmdLen; ++len)
	      if (key->CmdFile[len] == 0)
		key->CmdFile[len] = ' ';
          if (write(fd, key->CmdFile, key->CmdLen) == -1)
            goto end_SaveKeysToFile;
	  if (key->CmdTab)
	    for (len = 0; len < key->CmdLen; ++len) {
	      if (key->CmdFile[len] == ' ')
		key->CmdFile[len] = 0;
	      else if (key->CmdFile[len] == '"') {
		for (++len; len < key->CmdLen && key->CmdFile[len] != '"'; ++len);
		if (!key->CmdFile[len])
		  --len;
	      }
	    }
	}
        if (write(fd, "\n", 1) != 1)
          goto end_SaveKeysToFile;
	/* Enregistrement des liens entre les touches */
	if (key->Link && key->Link->Id < key->Id) {
	  buff[0] = 'L';
	  buff[1] = '1' + (key->Id % 16);
	  buff[2] = '1' + (key->Id / 16);
	  buff[3] = ':';
	  buff[4] = '1' + (key->Link->Id % 16);
	  buff[5] = '1' + (key->Link->Id / 16);
	  if (key->Flags & KEY_FLG_LNK_SIMUL)
	    buff[6] = 'S';
	  else if (key->Flags & KEY_FLG_LNK_LOOP)
	    buff[6] = 'L';
	  else
	    buff[6] = '0';
	  buff[7] = '\n';
	  if (write(fd, buff, 8) != 8)
	    goto end_SaveKeysToFile;
	  buff[0] = 'K';
	}
        UNLOCK_KEY(key);
      }
  return (0);
 end_SaveKeysToFile:
  UNLOCK_KEY(key);
  return (1);
}

/* Enregistrement des serveurs */
static unsigned char      SaveServersToFile(int fd)
{
  unsigned short          a;
  t_svr                   *svr;
  size_t                  len;
  char                    buff[8];

  memset(buff, 0, 8);
  for (a = 0; a < app->MaxServers; ++a)
    if ((svr = app->Servers[a])) {
      len = 1;
      buff[0] = 'S';
      LOCK_MUTEX(svr->Lock);
      UShortToStr(svr->Uid, &buff[1]);
      len += strlen(&buff[1]);
      buff[len] = ':';
      ++len;
      if (write(fd, buff, len) != len)
        goto end_SaveServersToFile;
      len = strlen(svr->Host);
      if (write(fd, svr->Host, len) != len)
        goto end_SaveServersToFile;
      if (write(fd, ":", 1) != 1)
        goto end_SaveServersToFile;
      len = strlen(svr->Port);
      if (write(fd, svr->Port, len) != len)
        goto end_SaveServersToFile;
      if (write(fd, ":", 1) != 1)
        goto end_SaveServersToFile;
      if (svr->Buff)
        if (write(fd, svr->Buff, svr->Size) != svr->Size)
          goto end_SaveServersToFile;
      UNLOCK_MUTEX(svr->Lock);
      if (write(fd, "\n", 1) != 1)
        return (1);
    }
  return (0);
 end_SaveServersToFile:
  UNLOCK_MUTEX(svr->Lock);
  return (1);
}

/* Enregistrement des serveurs */
static unsigned char      SaveVolumeToFile(int fd)
{
  unsigned char           a;
  t_lpd                   *lpd;
  char                    buff[12];

  memset(buff, 0, 9);
  lpd = app->Launchpad[app->CurrLpd];
  buff[0] = 'V';
  buff[2] = ':';
  for (a = 0; a < 8; ++a) {
    buff[1] = '0' + a + 1;
    FloatToStr(&buff[3], lpd->Mixer.Vol[a]);
    if (write(fd, buff, strlen(buff)) == -1)
      return (1);
    if (write(fd, "\n", 1) != 1)
      return (1);
  }
  return (0);
}

/* Enregistrement des modes */
static unsigned char      SaveModeToFile(int fd)
{
  t_lpd                   *lpd;

  lpd = app->Launchpad[app->CurrLpd];
  if (lpd->Flags & LPD_FLG_RANDOM_USER)
    if (write(fd, "M:R\n", 4) != 4)
      return (1);
  if (app->Flags & APP_FLG_QUIET)
    if (write(fd, "M:Q\n", 4) != 4)
      return (1);
  if (app->IdTempo)
    if (write(fd, "M:T\n", 4) != 4)
      return (1);
  return (0);
}

/* commande 'save' */
void                      CmdSave(char *param)
{
  int                     fd;

  if (!param || (*param == '?')) {
    CmdHelp("save");
    return;
  }
  if ((fd = open(param, O_CREAT | O_WRONLY | O_TRUNC, S_IRUSR | S_IWUSR)) > 0) {
    if (write(fd, SIG_SAV_V1, sizeof(SIG_SAV_V1) - 1) != sizeof(SIG_SAV_V1) - 1)
      goto end_CmdSave;
    /* Enregistrement des servers */
    if (SaveServersToFile(fd))
      goto end_CmdSave;
    /* Enregistrement des modes */
    if (SaveModeToFile(fd))
      goto end_CmdSave;
    /* Enregistrement des touches */
    if (SaveKeysToFile(fd))
      goto end_CmdSave;
    /* Enregistrement des volumes */
    if (SaveVolumeToFile(fd))
      goto end_CmdSave;
    close (fd);
    LOCK_APP(LockOutput);
    printf("%s '%s'\n", app->Strs[112], param);
    UNLOCK_APP(LockOutput);
  }
  else
    PrintError(68, ERR_TYPE_ERRNO, 0);
  return;
 end_CmdSave:
  close (fd);
  PrintError(86, ERR_TYPE_ERRNO, 0);
}
