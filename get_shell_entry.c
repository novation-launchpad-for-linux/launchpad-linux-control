/*
 *  'launchpadctrl' allows you to use your Novation launchpad (NVLPD01)
 *  Copyright (C) 2014-2017  Vincent Deca. known as Bigcake@ubuntu-fr.org
 *  (user@forum not email)
 *
 *  vincent.deca@neutralite.org
 *
 *  This file is part of 'launchpadctrl'.
 *
 * 'launchpadctrl' is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  'launchpadctrl' is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with 'launchpadctrl'.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app.h"

/* Renvoi le nombre de caractère visible */
size_t               StrLen(const char *str)
{
  size_t             nb;
  
  for (nb = 0; *str != '\0'; ++str)
    if ((*str & 0xc0) != 0x80)
      ++nb;
  return (nb);
}

/* Touche 'end' ou ctrl+e */
static int         ShellKeyEnd(const char *entry, int pos)
{
  int              len;

  if ((len = strlen(&entry[pos]))) {
    LOCK_APP(LockOutput);
    write(1, &entry[pos], len);
    UNLOCK_APP(LockOutput);
  }
  return (strlen(entry));
}

/* Gestion de la touche 'del' / 'backspace' */
static int           ShellKeyBackspace(char *entry, int pos)
{
  int                len, rlen;
  char               diff;

  if (!pos)
    return (0);
  /* Gestion des caractèes multi-bytes */
  for (diff = 1; (pos - diff) && ((entry[pos - diff] & 0xc0) == 0x80); ++diff);
  len = strlen(&entry[pos]);
  rlen = StrLen(&entry[pos]);
  pos -= diff;
  /* Suppression du caractère */
  LOCK_APP(LockOutput);
  if (len) {
    write(1, "\b", 1);
    memmove(&entry[pos], &entry[pos + 1], len + diff);
    write(1, &entry[pos], len);
    write(1, " ", 1);
    for (++rlen; rlen; --rlen)
      write(1, "\b", 1);
  }
  else {
    write(1, "\b \b", 3);
    entry[pos] = 0;
  }
  UNLOCK_APP(LockOutput);
  return (pos);
}

/* Gestion de la flèche gauche */
static int           ShellKeyLeft(char *entry, int pos)
{
  if (!pos)
    return (0);
  --pos;
  LOCK_APP(LockOutput);
  write(1, "\b", 1);
  UNLOCK_APP(LockOutput);
  /* Gestion des caractèes multi-bytes */
  for (; pos && ((entry[pos] & 0xc0) == 0x80); --pos);
  return (pos);
}

/* Completion des commandes avec la touche tab */
static int           TabCompleteCommand(char *entry, int pos)
{
  int                a, len, elen, cpos;
  char               *cmd_tab[] = LIST_CMDTAB;

  for (cpos = pos; cpos && entry[cpos] != ';' && entry[cpos] != ' ' && entry[cpos] != '\t'; --cpos);
  if (entry[cpos] == ' ' || entry[cpos] == '\t')
    ++cpos;
  if (pos - cpos < 0)
    return (pos);
  for (a = 0; cmd_tab[a]; ++a) {
    if (!strncmp(cmd_tab[a], &entry[cpos], pos - cpos)) {
      len = strlen(cmd_tab[a]);
      elen = strlen(entry);
      memcpy(&entry[pos], &cmd_tab[a][pos - cpos], len - pos + cpos);
      len += cpos;
      entry[len++] = ' ';
      LOCK_APP(LockOutput);
      write(1, &entry[pos], len - pos);
      entry[len] = 0;
      if (len < elen) {
	//# déplacer au lieu d'écraser
	for (a = elen; elen != len; --elen) write(1, " ", 1);
	for (elen = a; elen != len; --elen) write(1, "\b", 1);
      }
      UNLOCK_APP(LockOutput);
      return (len);
    }
  }
  return (pos);
}

/* Gestion de la touche tab */
static int           ShellKeyTab(char *entry, int pos)
{
  char               *dir, *name, flg;
  int                a, len, elen;
  unsigned int       count;
  DIR                *dirp;
  struct dirent      *dp;

  /* on determine s'il faut auto completer une commande ou un fichier/dossier */
  for (flg = 0, a = pos; a && (entry[a] == ' ' || entry[a] == '\t'); --a);
  for (; a && entry[a] != ' ' && entry[a] != ';'; --a);
  if (entry[a] == ';')
    flg = 1;
  if (a)
    for (--a; a && entry[a] == ' ' && entry[a] != ';'; --a);
  if (!a || entry[a] == ';')
    flg = 1;

  if (flg) /* Complétion des commandes */
    return (TabCompleteCommand(entry, pos));
  else { /* Complétion des fichiers/dossiers */
#ifdef DEV_MODE
    ASSERT(!pos, entry);
#endif
    for (a = pos; a && entry[a] != ' ' && entry[a] != '\t' && entry[a] != '/'; --a);
    if ((name = malloc(pos - a + 1)) == NULL) {
      PrintError(9, ERR_TYPE_ERRNO, 0);
      return (pos);
    }
    memcpy(name, &entry[a + 1], pos - a - 1);
    name[pos - a - 1] = 0;
    if (entry[a] == '/') {
      if (a) {
	for (len = a; a && entry[a] != ' ' && entry[a] != '\t'; --a);
	if (entry[a] == ' ' || entry[a] == '\t')
	  ++a;
      }
      else 
	len = 0;
      if ((dir = malloc(len - a + 2)) == NULL) {
	free(name);
	PrintError(9, ERR_TYPE_ERRNO, 0);
	return (pos);
      }
      memcpy(dir, &entry[a], len - a + 1);
      dir[len - a + 1] = 0;
    }
    else if ((dir = strdup(".")) == NULL) {
      free(name);
      PrintError(9, ERR_TYPE_ERRNO, 0);
      return (pos);
    }
    /* Recherche d'une correspondance de 'name' dans le dossier 'dir' */
    if ((dirp = opendir(dir))) {
      a = strlen(name);
      count = 0;
      while ((dp = readdir(dirp)) > 0) {
	if (a) {
	  if (!strncmp(dp->d_name, name, a)) {
	    if (count < app->TabCount) {
	      ++count;
	      continue;
	    }
	    len = strlen(dp->d_name) - a;
	    memcpy(&entry[pos], &dp->d_name[a], len + 1);
	    LOCK_APP(LockOutput);
	    write(1, &dp->d_name[a], len);
	    elen = strlen(entry) - a;
	    a = elen;
	    for (; elen; --elen) write(1, " ", 1);
	    for (len += a; len; --len) write(1, "\b", 1);
	    UNLOCK_APP(LockOutput);
	    ++app->TabCount;
	    break;
	  }
	}
	else if (count < app->TabCount)
	  ++count;
	else {
	  len = strlen(dp->d_name);
	  memcpy(&entry[pos], dp->d_name, len + 1);
	  pos += len;
	  LOCK_APP(LockOutput);
	  write(1, dp->d_name, len);
	  UNLOCK_APP(LockOutput);
	  ++app->TabCount;
	  break;
	}
      } /* while */
      if (!dp)
	app->TabCount = 0;
      closedir(dirp);
    } /* opendir */
    free(name);
    free(dir);
  }
  return (pos);
}

/* Gestion de la flèche droite */
static int           ShellKeyRight(char *entry, int pos)
{
  if (!entry[pos])
    return (pos);
  if (app->TabCount)
    return (ShellKeyEnd(entry, pos));
  LOCK_APP(LockOutput);
  write(1, &entry[pos], 1);
  /* Gestion des caractèes multi-bytes */
  for (++pos; entry[pos] && ((entry[pos] & 0xc0) == 0x80); ++pos)
    write(1, &entry[pos], 1);
  UNLOCK_APP(LockOutput);
  return (pos);
}

/* Ajout des caractères affichable */
static int           ShellKeyOther(char *entry, int pos, char *buff, int *a, int rd)
{
  int                len, b;
  unsigned char      clen;

  for (clen = 1; (*a + clen <= SIZE_BUFFER_SHELL) && (buff[*a + clen] & 0xc0) == 0x80; ++clen);
  if (entry[pos]) {
    len = strlen(&entry[pos]);
    if (pos + len + clen >= SIZE_BUFFER_SHELL)
      return (pos);
    for (b = pos + len + clen; b > pos; --b)
      entry[b] = entry[b - clen];
    memcpy(&entry[pos], &buff[*a], clen);
    //#muli-char sur 2 buff
    entry[pos + len + clen] = 0;
    LOCK_APP(LockOutput);
    write(1, &entry[pos], len + clen);
    len = StrLen(&entry[pos]);
    for (--len; len; --len) write(1, "\b", 1);
    UNLOCK_APP(LockOutput);
    pos += clen;
    *a += (clen - 1);
  }
  else {
    if (pos + clen >= SIZE_BUFFER_SHELL)
      return (pos);
    memcpy(&entry[pos], &buff[*a], clen);
    pos += clen;
    entry[pos] = 0;
    LOCK_APP(LockOutput);
    write(1, &buff[*a], clen);
    UNLOCK_APP(LockOutput);
    *a += (clen - 1);
  }
  return (pos);
}

/* Flèche du haut */
static int          ShellKeyUp(char *entry, int pos)
{
  int               rlen;
  
  if (app->ShellHistPos)
    --app->ShellHistPos;
  else 
    return (pos);
  if (!app->ShellHistory[app->ShellHistPos])
    return (pos);
  /* Effacement de la ligne actuelle */
  rlen = StrLen(entry);
  LOCK_APP(LockOutput);
  if (entry[pos])
    write(1, &entry[pos], strlen(&entry[pos]));
  for (; rlen; --rlen) write(1, "\b \b", 3);
  /* écriture de l'historique */
  pos = strlen(app->ShellHistory[app->ShellHistPos]);
  memcpy(entry, app->ShellHistory[app->ShellHistPos], pos + 1);
  write(1, entry, pos);
  UNLOCK_APP(LockOutput);
  return (pos);
}

/* Touche 'début' ou ctrl+a */
static int         ShellKeyHome(char *entry, int pos)
{
  if (pos) {
    pos = StrLen(entry) - StrLen(&entry[pos]);
    LOCK_APP(LockOutput);
    for (; pos; --pos) write(1, "\b", 1);
    UNLOCK_APP(LockOutput);
  }
  return (0);
}

/* ctrl + l */
static void        ShellKeyCtrlL()
{
  printf("\033[H\033[2J");
  PrintLPEntry();
}

/* ctrl + d */
static void        ShellKeyCtrlD()
{
  if (app->Flags & APP_FLG_CTRLD)
    QuitApp(0);
  app->Flags |= APP_FLG_CTRLD;
  LOCK_APP(LockOutput);
  printf("\n%s\n", app->Strs[21]);
  UNLOCK_APP(LockOutput);
  PrintLPEntry();
}

/* ctrl + U */
static int         ShellKeyCtrlU(char *entry, int pos)
{
  int              llen, nlen, sav;
  
  if (!pos)
    return (0);
  memcpy(app->ShellBuffer, entry, pos);
  app->ShellBuffer[pos] = 0;
  sav = nlen = StrLen(&entry[pos]);
  llen = StrLen(entry) - nlen;
  LOCK_APP(LockOutput);
  for (; nlen; --nlen) write(1, " ", 1);
  for (nlen = sav + llen; nlen; --nlen) write(1, "\b", 1);
  for (nlen = sav + llen; nlen; --nlen) write(1, " ", 1);
  for (nlen = sav + llen; nlen; --nlen) write(1, "\b", 1);
  if (!sav)
    *entry = 0;
  else {
    nlen = strlen(&entry[pos]);
    memcpy(entry, &entry[pos], nlen + 1);
    write(1, entry, nlen);
    nlen = StrLen(entry);
    for (; nlen; --nlen) write(1, "\b", 1);
  }
  UNLOCK_APP(LockOutput);
  return (0);
}

/* ctrl + Y */
static int         ShellKeyCtrlY(char *entry, int pos)
{
  int              len, len2;
  
  if (!(len = strlen(app->ShellBuffer)))
    return (pos);
  if (pos + len >= SIZE_BUFFER_SHELL)
    if (!(len = SIZE_BUFFER_SHELL - pos))
      return (pos);
  if (!(len2 = strlen(&entry[pos]))) {
    memcpy(&entry[pos], app->ShellBuffer, len + 1);
    LOCK_APP(LockOutput);
    write(1, app->ShellBuffer, len + 1);
  }
  else if ((pos + len + len2 >= SIZE_BUFFER_SHELL) && !(len2 = SIZE_BUFFER_SHELL - len - pos)) {
    memcpy(&entry[pos], app->ShellBuffer, len);
    entry[pos + len] = 0;
    LOCK_APP(LockOutput);
    write(1, &entry[pos], len);
  }
  else {
    memcpy(&entry[pos + len], &entry[pos], len2 + 1);
    memcpy(&entry[pos], app->ShellBuffer, len);
    LOCK_APP(LockOutput);
    write(1, &entry[pos], len + len2);
    len2 = StrLen(&entry[pos + len]);
    for (; len2; --len2) write(1, "\b", 1);
  }
  UNLOCK_APP(LockOutput);
  return (pos + len);
}

/* ctrl + k */
static void        ShellKeyCtrlK(char *entry, int pos)
{
  int              len, sav;
  
  if (!(sav = len = StrLen(&entry[pos])))
    return;
  LOCK_APP(LockOutput);
  for (; len; --len) write(1, " ", 1);
  for (len = sav; len; --len) write(1, "\b", 1);
  UNLOCK_APP(LockOutput);
  memcpy(app->ShellBuffer, &entry[pos], strlen(&entry[pos]) + 1);
  entry[pos] = 0;
}

/* ctrl + t */
static int         ShellKeyCtrlT(char *entry, int pos)
{
  int              last, next;
  char             swp[6];

  if (!pos || !entry[pos])
    return (0);
  for (last = 1; (pos - last) && ((entry[pos - last] & 0xc0) == 0x80); ++last);
  for (next = 1; entry[pos + next] && ((entry[pos + next] & 0xc0) == 0x80); ++next);
  memcpy(swp, &entry[pos - last], last);
  memcpy(&swp[3], &entry[pos], next);
  memcpy(&entry[pos - last], &swp[3], next);
  memcpy(&entry[pos - last + next], swp, last);
  LOCK_APP(LockOutput);
  write(1, "\b", 1);
  write(1, &entry[pos - last], last + next);
  UNLOCK_APP(LockOutput);
  return (pos + next);
}

/* Touche 'suppr' */
static void         ShellKeySuppr(char *entry, int pos)
{
  int               len, next;
  
  if (!(len = strlen(&entry[pos])))
    return;
  LOCK_APP(LockOutput);
  if (len == 1) {
    entry[pos] = 0;
    write(1, " \b", 2);
  }
  else {
    for (next = 1; entry[pos + next] && ((entry[pos + next] & 0xc0) == 0x80); ++next);
    memmove(&entry[pos], &entry[pos + next], len - next);
    entry[pos + len - next] = 0;
    write(1, &entry[pos], len - next);
    write(1, " ", 1);
    len = StrLen(&entry[pos]) + 1;
    for (; len; --len) write(1, "\b", 1);
  }
  UNLOCK_APP(LockOutput);
}
  
/* Flèche du bas */
static int          ShellKeyDown(char *entry, int pos)
{
  int               rlen;
  
  if (app->ShellHistPos < DEFAULT_NB_HISTORY && app->ShellHistory[app->ShellHistPos])
    ++app->ShellHistPos;
  if (!pos)
    return (0);
  if (!app->ShellHistory[app->ShellHistPos])
    return (pos);
  /* Effacement de la ligne actuelle */
  rlen = StrLen(entry);
  LOCK_APP(LockOutput);
  if (entry[pos])
    write(1, &entry[pos], strlen(&entry[pos]));
  for (; rlen; --rlen) write(1, "\b \b", 3);
  if (!app->ShellHistory[app->ShellHistPos]) {
    *entry = 0;
    UNLOCK_APP(LockOutput);
    return (0);
  }
  /* écriture de l'historique */
  pos = strlen(app->ShellHistory[app->ShellHistPos]);
  memcpy(entry, app->ShellHistory[app->ShellHistPos], pos + 1);
  write(1, entry, pos);
  UNLOCK_APP(LockOutput);
  return (pos);
}

/* Gestion des caractères non affichable */
static int           ShellKeySpecial(char *entry, int pos, char *buff, int *a, int rd)
{
  if ((*a) + 1 >= rd)
    return (pos);
  ++(*a);
  switch (buff[*a]) {
  case 'O': break;
  case '[': break;
  default : return (pos);
  }
  if ((*a) + 1 >= rd)
    return (pos);
  ++(*a);
  switch (buff[*a]) {
  case 'A': pos = ShellKeyUp(entry, pos); break;
  case 'B': pos = ShellKeyDown(entry, pos); break;
  case 'C': pos = ShellKeyRight(entry, pos); break;
  case 'D': pos = ShellKeyLeft(entry, pos); break;
  case 'F': pos = ShellKeyEnd(entry, pos); break;
  case 'H': pos = ShellKeyHome(entry, pos); break;
  case '5': /* page up */
    if ((*a) + 1 >= rd)
      break;
    ++(*a);
    break;
  case '6': /* page down */
    if ((*a) + 1 >= rd)
      break;
    ++(*a);
    break;
  case 'E': /* pavé num 5 */
  case 'P': /* F1 */
  case 'Q': /* F2 */
  case 'R': /* F3 */
  case 'S': /* F4 */
    break;
  case '2': /* F9 -> F12 */
  case '1': /* F5 -> F8 */
    if ((*a) + 1 >= rd)
      break;
    ++(*a);
    if ((*a) + 1 >= rd)
      break;
    ++(*a);
    break;
  case '3': /* suppr */
    if ((*a) + 1 >= rd)
      break;
    ++(*a);
    if (buff[*a] == '~') {
      ShellKeySuppr(entry, pos);
      break;
    }
    else
      --(*a);
    break;
  default: printf("\nunknown input sequence '%c'\n", buff[*a]);
  }
  return (pos);
}

/* Ajout de la ligne de commande dans l'historique */
static void          AddToShellHistory(char *entry, char *buff)
{
  int                a;

  for (a = 0; a < SIZE_BUFFER_SHELL && (buff[a] == ' ' || buff[a] == '\t'); ++a);
  if (!buff[a])
    return;
  memcpy(buff, entry, strlen(entry) + 1);
  for (a = 0; a < DEFAULT_NB_HISTORY; ++a)
    if (!app->ShellHistory[a]) {
      app->ShellHistory[a] = buff;
      app->ShellHistPos = a + 1;
      return;
    }
  free(app->ShellHistory[0]);
  memcpy(app->ShellHistory, &app->ShellHistory[1], sizeof(char *) * DEFAULT_NB_HISTORY);
  app->ShellHistory[DEFAULT_NB_HISTORY - 1] = buff;
  app->ShellHistPos = DEFAULT_NB_HISTORY;
}

/* Récupère la commande */
char                 *GetEntry()
{
  int                a, pos, rd;
  fd_set             slrd;
  char               *buff, *entry;

  if ((buff = malloc(SIZE_BUFFER_SHELL + 1)) == NULL) {
    PrintError(9, ERR_TYPE_ERRNO, 0);
    return (0);
  }
  if ((entry = malloc(SIZE_BUFFER_SHELL + 1)) == NULL) {
    free(buff);
    PrintError(9, ERR_TYPE_ERRNO, 0);
    return (0);
  }
  pos = 0;
  *entry = 0;
  app->Entry = entry;
  while (1) {
    FD_ZERO(&slrd);
    FD_SET(0, &slrd);
    app->ShellPos = pos;
    if (select(1, &slrd, NULL, NULL, NULL) < 0)
      break;
    if (FD_ISSET(0, &slrd)) {
      if ((rd = read(0, buff, SIZE_BUFFER_SHELL)) < 0) {
	PrintError(2, ERR_TYPE_ERRNO, 0);
	break;
      }
      else {
	buff[rd] = 0;
	for (a = 0; a < rd; ++a) {
	  switch (buff[a]) {
	  case '\t': pos = ShellKeyTab(entry, pos); break;
	  case '\n': pos = strlen(entry);
	    if (pos) {
	      for (--pos; pos && (entry[pos] == ' ' || entry[pos] == '\t'); --pos);
	      entry[pos + 1] = 0;
	      AddToShellHistory(entry, buff);
	    }
	    if (app->ShellPos > pos)
	      app->ShellPos = pos;
	    write(1, "\n", 1);
	    return (entry);
	  case 1: pos = ShellKeyHome(entry, pos); break;
	  case 4: ShellKeyCtrlD(); break;
	  case 5: pos = ShellKeyEnd(entry, pos); break;
	  case 11: ShellKeyCtrlK(entry, pos); break;
	  case 12: ShellKeyCtrlL(); break;
	  case 20: pos = ShellKeyCtrlT(entry, pos); break;
	  case 21: pos = ShellKeyCtrlU(entry, pos); break;
	  case 25: pos = ShellKeyCtrlY(entry, pos); break;
	  case 27: pos = ShellKeySpecial(entry, pos, buff, &a, rd); break;
	  case 127: pos = ShellKeyBackspace(entry, pos); break;
	  default: pos = ShellKeyOther(entry, pos, buff, &a, rd); break;
	  } /* switch */
	  if (buff[a] != 4)
	    app->Flags &= ~(APP_FLG_CTRLD);
	  app->TabCount = 0;
	} /* for a < rd */
      } /* else read */
    } /* if FD_ISSET */
  }
  free(buff); free(entry);
  app->Entry = 0;
  return (0);
}
