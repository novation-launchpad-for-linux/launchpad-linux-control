/*
 *  'launchpadctrl' allows you to use your Novation launchpad (NVLPD01)
 *  Copyright (C) 2014-2017  Vincent Deca. known as Bigcake@ubuntu-fr.org
 *  (user@forum not email)
 *
 *  vincent.deca@neutralite.org
 *
 *  This file is part of 'launchpadctrl'.
 *
 * 'launchpadctrl' is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  'launchpadctrl' is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with 'launchpadctrl'.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app.h"

/* Libération des ressources prise par une structure d'infos serveur */
void                       FreeServerStruct(t_svr *svr)
{
  if (svr->Id) {
    LOCK_APP(LockPonctual);
    app->Servers[svr->Id - 1] = 0;
    UNLOCK_APP(LockPonctual);
  }
  LOCK_MUTEX(svr->Lock);
  if (svr->Host) free(svr->Host);
  svr->Host = 0;
  if (svr->Port) free(svr->Port);
  svr->Port = 0;
  if (svr->Buff) free(svr->Buff);
  svr->Buff = 0;
  svr->Size = 0;
  UNLOCK_MUTEX(svr->Lock);
  //#pb
  pthread_mutex_destroy(&svr->Lock);
  free(svr);
}

/* Vérification de l'existance du serveur dans le tableau par son uid */
t_svr                    *CheckExistServerId(unsigned short id)
{
  unsigned short         a;
  
  LOCK_APP(LockPonctual);
  for (a = 0; a < app->MaxServers; ++a)
    if (app->Servers[a])
      if (app->Servers[a]->Uid == id) {
	UNLOCK_APP(LockPonctual);
	return (app->Servers[a]);
      }
  UNLOCK_APP(LockPonctual);
  return (0);
}

/* Vérification de l'existance du serveur dans le tableau */
t_svr                    *CheckExistServer(t_svr *svr)
{
  t_svr                  **tab;
  unsigned short         a;

  tab = app->Servers;
  LOCK_APP(LockPonctual);
  for (a = 0; a < app->MaxServers; ++a) {
    if (tab[a]) {
      LOCK_MUTEX(tab[a]->Lock);
      if (!strcmp(tab[a]->Host, svr->Host) && !strcmp(tab[a]->Port, svr->Port)) {
	UNLOCK_MUTEX(tab[a]->Lock);
	UNLOCK_APP(LockPonctual);
	return (tab[a]);
      }
      UNLOCK_MUTEX(tab[a]->Lock);
    }
  }
  UNLOCK_APP(LockPonctual);
  return (0);
}

/* Ajout d'une structure serveur dans le tableau */
unsigned char            AddServerStruct(t_svr *svr)
{
  t_svr                  **tab;
  unsigned short         a;

  tab = app->Servers;
  if (app->MaxServers + STEP_NB_SERVERS > APP_LIMIT_USHORT) {
    LOCK_APP(LockOutput);
    printf("\r\033[31m%s\033[00m '%s:%s': %s \n", app->Strs[1], svr->Host, svr->Port, app->Strs[95]);
    UNLOCK_APP(LockOutput);
    if (!(app->Flags & APP_FLG_SHELL_CMD))
      PrintLPEntry();
    return (0);
  }
  LOCK_APP(LockPonctual);
  /* Recherche d'un place libre dans le conteneur */
  for (a = 0; a < app->MaxServers; ++a) {
    if (!tab[a]) {
      tab[a] = svr;
      svr->Id = a + 1;
      UNLOCK_APP(LockPonctual);
      return (0);
    }
  }
  UNLOCK_APP(LockPonctual);
  /* Si le tableau n'a pas été créé, on le créer */
  if (!tab) {
    if ((tab = calloc(STEP_NB_SERVERS, sizeof(t_svr *))) == NULL)
      return (1);
  }
  /* Sinon on l'agrandit */
  else if (a == app->MaxServers) {
    if ((tab = calloc(app->MaxServers + STEP_NB_SERVERS, sizeof(t_svr *))) == NULL)
      return (1);
    memcpy(tab, app->Servers, sizeof(t_svr *) * a);
    free(app->Servers);
  }
  LOCK_APP(LockPonctual);
  app->MaxServers += STEP_NB_SERVERS;
  tab[a] = svr;
  app->Servers = tab;
  svr->Id = a + 1;
  UNLOCK_APP(LockPonctual);
  return (0);
}

/* Recherche des touche affecté par le changement de connexion au serveur */
void                     UpdateRelatedKeys(t_svr *svr)
{
  unsigned char          a, b, lp;
  t_key                  *grid, *key;

  for (lp = 0; lp < app->NbLaunchpad; ++lp) {
    if (app->Launchpad[lp]->Flags & LPD_FLG_IN_USE)
      for (a = 0; a < GRID_VOLUME; ++a) {
	grid = app->Launchpad[lp]->Grid[a];
	for (b = 0; b < 72; ++b) {
	  key = &grid[b];
	  LOCK_KEY(key);
	  if (key->Type == KEY_TYPE_SERVER) {
	    if (svr->Uid == key->Count) {
	      if (svr->Socket) {
		key->Flags &= ~(KEY_FLG_RUN_ERROR);
		key->DefColor = STAT_IDLE_CMD;
	      }
	      else {
		key->Flags |= KEY_FLG_RUN_ERROR;
		key->DefColor = STAT_IDLE_ERROR;
	      }
	      UpdateKey(key);
	    }
	  }
	  UNLOCK_KEY(key);
	}
      }
  }
}

/* Surveille la connexion avec les serveurs */
void                     *ThreadSurveyServer(void *param)
{
  fd_set                 slrd;
  unsigned short         a;
  char                   flg, *buff;
  t_svr                  *svr;
  int                    max, ret;
  struct timeval         time;

  if ((buff = malloc(SIZE_BUFFER_LOAD)) == NULL) {
    PrintError(9, ERR_TYPE_ERRNO, 0);
    return (0);
  }
  while (1) {
    FD_ZERO(&slrd);
    for (max = 0, flg = 0, a = 0; a < app->MaxServers; ++a)
      if (app->Servers[a]) {
	svr = app->Servers[a];
	if (svr) {
	  LOCK_MUTEX(svr->Lock);
	  if (svr->Socket) {
	    FD_SET(svr->Socket, &slrd);
	    flg = 1;
	    if (svr->Socket > max)
	      max = svr->Socket;
	  }
	  UNLOCK_MUTEX(svr->Lock);
	}
      }
    if (!flg)
      break;
    time.tv_sec = 0;
    time.tv_usec = 500000;
    if ((ret = select(max + 1, &slrd, NULL, NULL, &time)) < 0)
      PrintError(103, ERR_TYPE_ERRNO, 0);
    else if (ret) {
      for (max = 0, flg = 0, a = 0; a < app->MaxServers; ++a)
	if (app->Servers[a]) {
	  svr = app->Servers[a];
	  if (svr) {
	    LOCK_MUTEX(svr->Lock);
	    if (svr->Socket)
	      if (FD_ISSET(svr->Socket, &slrd)) {
		while ((ret = recv(svr->Socket, buff, SIZE_BUFFER_LOAD, 0)) > 0);
		if (ret == 0) {
		  svr->Socket = 0;
		  LOCK_APP(LockOutput);
		  printf("\r\033[31m%s\033[00m: %s:%s : %s \n", app->Strs[1], svr->Host, svr->Port, app->Strs[104]);
		  UNLOCK_APP(LockOutput);
		  if (!(app->Flags & APP_FLG_SHELL_CMD))
		    PrintLPEntry();
		  UpdateRelatedKeys(svr);
		}
	      }
	    UNLOCK_MUTEX(svr->Lock);
	  }
	}
    }
  }
  free(buff);
  LOCK_APP(LockPonctual);
  app->IdServer = 0;
  UNLOCK_APP(LockPonctual);
  return (0);
}

/* Connexion a un serveur */
int                      ConnectToServer(char *host, char *port)
{
  struct addrinfo        *res, *tmp, hints;
  unsigned char          a, flg;
  int                    ret;

  memset(&hints, 0, sizeof(hints));
  hints.ai_socktype = SOCK_STREAM;
  /* PF_UNSPEC plutot que AF_INET pour une meilleure compatibilité
     des protocoles réseaux (vite fait et en gros) */
  hints.ai_family = PF_UNSPEC;
  /* Recuperation des infos de l'adresse */
  if ((ret = getaddrinfo(host, port, &hints, &res))) {
    LOCK_APP(LockOutput);
    printf("\r%s '%s:%s': %s (%s) \n", app->Strs[93], host, port, app->Strs[51], gai_strerror(ret));
    UNLOCK_APP(LockOutput);
    if (!(app->Flags & APP_FLG_SHELL_CMD))
      PrintLPEntry();
    return (0);
  }
  /* Boucle de tentative de connexion */
  for (flg = 0, a = 1, tmp = res; tmp; tmp = tmp->ai_next, ++a) {
    /* Ouverture du socket */
    if ((ret = socket(tmp->ai_family, tmp->ai_socktype, tmp->ai_protocol)) == -1)
      continue;
    /* Connexion à la partie serveur */
    if (connect(ret, tmp->ai_addr, tmp->ai_addrlen) == -1) {
      LOCK_APP(LockOutput);
      printf("\r%s '%s:%s' (%d): %s (%s) \n", app->Strs[93], host, port, a, app->Strs[51], strerror(errno));
      UNLOCK_APP(LockOutput);
      flg = 2;
      close(ret);
      if (!(app->Flags & APP_FLG_SHELL_CMD))
	PrintLPEntry();
      continue;
    }
    flg = 1;
    break;
  }
  freeaddrinfo(res);
  if (flg != 1) {
    if (!flg) {
      LOCK_APP(LockOutput);
      printf("\r%s '%s:%s': %s \n", app->Strs[93], host, port, app->Strs[51]);
      UNLOCK_APP(LockOutput);
      if (!(app->Flags & APP_FLG_SHELL_CMD))
	PrintLPEntry();
    }
    return (0);
  }
  LOCK_APP(LockOutput);
  printf("\r%s '%s:%s': %s \n", app->Strs[93], host, port, app->Strs[50]);
  UNLOCK_APP(LockOutput);
  if (!(app->Flags & APP_FLG_SHELL_CMD))
    PrintLPEntry();
  return (ret);
}
