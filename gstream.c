/*
 *  'launchpadctrl' allows you to use your Novation launchpad (NVLPD01)
 *  Copyright (C) 2014-2017  Vincent Deca. known as Bigcake@ubuntu-fr.org
 *  (user@forum not email)
 *
 *  vincent.deca@neutralite.org
 *
 *  This file is part of 'launchpadctrl'.
 *
 * 'launchpadctrl' is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  'launchpadctrl' is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with 'launchpadctrl'.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app.h"

/* Libération des ressources prises par la structure t_af */
void                     FreeAudioFile(t_af *af)
{
  if (af->BusId) g_source_remove(af->BusId);
  if (af->Pipe) {
    gst_element_set_state(af->Pipe, GST_STATE_PAUSED);
    gst_element_set_state(af->Pipe, GST_STATE_READY);
    gst_element_set_state(af->Pipe, GST_STATE_NULL);
    gst_object_unref(GST_OBJECT(af->Pipe));
  }
  /*
  if (af->Sink) gst_object_unref(GST_OBJECT(af->Sink));
  if (af->Vol) gst_object_unref(GST_OBJECT(af->Vol));
  if (af->Conv) gst_object_unref(GST_OBJECT(af->Conv));
  if (af->Source) gst_object_unref(GST_OBJECT(af->Source));
  if (af->Resample) gst_object_unref(GST_OBJECT(af->Resample));
  if (af->Demuxer) gst_object_unref(GST_OBJECT(af->Demuxer));
  if (af->Decoder) gst_object_unref(GST_OBJECT(af->Decoder));
  */
  if (af->Loop) g_main_loop_unref(af->Loop);
  if (af->OggFile) free(af->OggFile);
  free(af);
}

/* Bus callback */
static gboolean       CallbackBus(GstBus *bus, GstMessage *msg, gpointer data) 
{
  GMainLoop           *loop = (GMainLoop *)data;
  gchar               *debug;
  GError              *err;

  switch (GST_MESSAGE_TYPE(msg)) {
  case GST_MESSAGE_EOS:
    g_main_loop_quit(loop);
    break;
  case GST_MESSAGE_ERROR:
    //*
    gst_message_parse_error(msg, &err, &debug);
    g_printerr("%s: %s\n", app->Strs[1], err->message);
    g_error_free(err);
    g_main_loop_quit(loop);
    //*/
    break;
  default:
    break;
  }
  return (TRUE);
}

/* on padd added */
static void              CallbackAddedPad(GstElement *element, GstPad *pad, gpointer data)
{
  GstPad                 *sinkpad;
  GstElement             *decoder = (GstElement *) data;

  sinkpad = gst_element_get_static_pad(decoder, "sink");
  gst_pad_link(pad, sinkpad);
  gst_object_unref(sinkpad);
}

#define SIG_ERROR        0
#define SIG_OGG_FILE     1
#define SIG_MP3_FILE     2
#define SIG_WAV_FILE     3
#define SIG_FLAC_FILE    5
#define SIG_UNKNOW       6

/* Rechargement des ficher OGG */
char                     DirtyReloadOgg(t_af *af)
{
  GstBus                 *bus;

  /* Libération */
  gst_element_set_state(af->Pipe, GST_STATE_NULL);
  g_source_remove(af->BusId);
  gst_object_unref(GST_OBJECT(af->Pipe));
  g_main_loop_unref(af->Loop);

  /* Recréation */
  if (!(af->Loop = g_main_loop_new(NULL, FALSE)))
    goto end_ReloadOgg;
  if (!(af->Pipe = gst_pipeline_new("audio-player")))
    goto end_ReloadOgg;
  if (!(af->Source = gst_element_factory_make("filesrc", "file-source")))
    goto end_ReloadOgg;
  g_object_set(G_OBJECT(af->Source), "location", af->OggFile, NULL); 

  bus = gst_pipeline_get_bus(GST_PIPELINE(af->Pipe));
  af->BusId = gst_bus_add_watch(bus, CallbackBus, af->Loop);
  gst_object_unref(bus);

  if (!(af->Conv = gst_element_factory_make("audioconvert", "converter")))
    goto end_ReloadOgg;
  if (!(af->Resample = gst_element_factory_make("audioresample", "audioresample")))
    goto end_ReloadOgg;
  if (!(af->Vol = gst_element_factory_make("volume", "volume")))
    goto end_ReloadOgg;
  if (!(af->Sink = gst_element_factory_make("autoaudiosink", "audio-output")))
    goto end_ReloadOgg;
  if (!(af->Demuxer = gst_element_factory_make("oggdemux", "ogg-demuxer")))
    goto end_ReloadOgg;
  if (!(af->Decoder = gst_element_factory_make("vorbisdec", "vorbis-decoder")))
    goto end_ReloadOgg;

  gst_bin_add_many(GST_BIN(af->Pipe), af->Source, af->Demuxer, af->Decoder, af->Conv, af->Resample, af->Vol, af->Sink, NULL);
  gst_element_link(af->Source, af->Demuxer);
  gst_element_link(af->Source, af->Vol);
  g_signal_connect(af->Demuxer, "pad-added", G_CALLBACK(CallbackAddedPad), af->Decoder);
  gst_element_link_many(af->Decoder, af->Conv, af->Resample, af->Vol, af->Sink, NULL);

  return (0);
 end_ReloadOgg:
  return (1);
}
//*/

/* Vérification du type de fichier audio */
static unsigned char     CheckAudioSig(const char *file)
{
  int                    fd, ret = SIG_UNKNOW;
  char                   sig[15];

  if ((fd = open(file, O_RDONLY)) < 0)
    return (SIG_ERROR);
  if (read(fd, sig, 15) < 0)
    ret = SIG_ERROR;
  else if (!strncmp(sig, "OggS", 4))
    ret = SIG_OGG_FILE;
  else if (!strncmp(sig, "ID3", 3))
    ret = SIG_MP3_FILE;
  else if (!strncmp(sig, "RIFF", 3)) {
    if (!strncmp(&sig[8], "WAVEfmt", 7))
      ret = SIG_WAV_FILE;
  }
  else if (!strncmp(sig, "fLaC", 4))
    ; //ret = SIG_FLAC_FILE;
  close(fd);
  return (ret);
}

/* Chargement d'un fichier audio */
t_af                     *LoadAudioFile(const char *file)
{
  t_af                   *tmp;
  GstBus                 *bus;
  unsigned char          sig;
  
  if ((sig = CheckAudioSig(file)) == SIG_ERROR) {
    PrintError(146, ERR_TYPE_ERRNO, 0);
    return (0);
  }
  if (sig == SIG_UNKNOW) {
    PrintError(147, ERR_TYPE_FILE, file);
    return (0);
  }
  if ((tmp = calloc(1, sizeof(t_af))) == NULL) {
    PrintError(9, ERR_TYPE_ERRNO, 0);
    return (0);
  }
  if (sig == SIG_OGG_FILE) {
    if ((tmp->OggFile = strdup(file)) == NULL) {
      free(tmp);
      PrintError(9, ERR_TYPE_ERRNO, 0);
      return (0);
    }
  }
  if (!(tmp->Loop = g_main_loop_new(NULL, FALSE)))
    goto end_LoadAudioFile;

  if (!(tmp->Pipe = gst_pipeline_new("audio-player")))
    goto end_LoadAudioFile;
  if (!(tmp->Source = gst_element_factory_make("filesrc", "file-source")))
    goto end_LoadAudioFile;
  g_object_set(G_OBJECT(tmp->Source), "location", file, NULL); 

  bus = gst_pipeline_get_bus(GST_PIPELINE(tmp->Pipe));
  tmp->BusId = gst_bus_add_watch(bus, CallbackBus, tmp->Loop);
  gst_object_unref(bus);

  if (!(tmp->Conv = gst_element_factory_make("audioconvert", "converter")))
    goto end_LoadAudioFile;
  if (!(tmp->Resample = gst_element_factory_make("audioresample", "audioresample")))
    goto end_LoadAudioFile;
  if (!(tmp->Vol = gst_element_factory_make("volume", "volume")))
    goto end_LoadAudioFile;
  if (!(tmp->Sink = gst_element_factory_make("autoaudiosink", "audio-output")))
    goto end_LoadAudioFile;

  switch (sig) {
  case SIG_OGG_FILE:
    if (!(tmp->Demuxer = gst_element_factory_make("oggdemux", "ogg-demuxer")))
      goto end_LoadAudioFile;
    if (!(tmp->Decoder = gst_element_factory_make("vorbisdec", "vorbis-decoder")))
      goto end_LoadAudioFile;
    gst_bin_add_many(GST_BIN(tmp->Pipe), tmp->Source, tmp->Demuxer, tmp->Decoder, tmp->Conv, tmp->Resample, tmp->Vol, tmp->Sink, NULL);
    gst_element_link(tmp->Source, tmp->Demuxer);
    gst_element_link(tmp->Source, tmp->Vol);
    g_signal_connect(tmp->Demuxer, "pad-added", G_CALLBACK(CallbackAddedPad), tmp->Decoder);
    gst_element_link_many(tmp->Decoder, tmp->Conv, tmp->Resample, tmp->Vol, tmp->Sink, NULL);
    break;
  case SIG_MP3_FILE:
    if (!(tmp->Decoder = gst_element_factory_make("mad", "mp3-decoder")))
      goto end_LoadAudioFile;
    /*
    if (!(tmp->Decoder = gst_element_factory_make("flump3dec", "fluendo-decoder")))
      goto end_LoadAudioFile;
    */
    gst_element_link(tmp->Source, tmp->Vol);
    gst_bin_add_many(GST_BIN(tmp->Pipe), tmp->Source, tmp->Decoder, tmp->Conv, tmp->Resample, tmp->Vol, tmp->Sink, NULL);
    if (!gst_element_link_many(tmp->Source, tmp->Decoder, tmp->Conv, tmp->Resample, tmp->Vol, tmp->Sink, NULL))
      goto end_LoadAudioFile;
    break;
  case SIG_WAV_FILE:
    if (!(tmp->Decoder = gst_element_factory_make ("wavparse", "wav-decoder")))
      goto end_LoadAudioFile;
    gst_bin_add_many(GST_BIN(tmp->Pipe), tmp->Source, tmp->Decoder, tmp->Conv, tmp->Resample, tmp->Vol, tmp->Sink, NULL);
    gst_element_link(tmp->Source, tmp->Decoder);
    gst_element_link_many(tmp->Conv, tmp->Resample, tmp->Vol, tmp->Sink, NULL);
    g_signal_connect(tmp->Decoder, "pad-added", G_CALLBACK(CallbackAddedPad), tmp->Conv);
    break;
    /*
      case SIG_FLAC_FILE:
    if (!(tmp->Decoder = gst_element_factory_make ("flacdec", "flac-decoder")))
      goto end_LoadAudioFile;
    gst_bin_add_many(GST_BIN(tmp->Pipe), tmp->Source, tmp->Decoder, tmp->Conv, tmp->Resample, tmp->Vol, tmp->Sink, NULL);
    gst_element_link(tmp->Source, tmp->Decoder);
    gst_element_link_many(tmp->Conv, tmp->Resample, tmp->Vol, tmp->Sink,NULL);
    //g_signal_connect(tmp->Decoder, "pad-added", G_CALLBACK(CallbackAddedPad), tmp->Conv);
    break;
    */
  }
  gst_element_set_state (tmp->Pipe, GST_STATE_READY);

  return (tmp);
  
 end_LoadAudioFile:
  FreeAudioFile(tmp);
  PrintError(146, ERR_TYPE_FILE, file);
  return (0);
}

/* THREAD - Jouer un son */
void                       *PlaySound(void *param)
{
  t_key                    *key = param;
  unsigned char            grid, pos;
  t_lpd                    *lpd = key->lpd;
  t_af                     *af = key->Audio;

  if (lpd->CurrGrid < GRID_VOLUME)
    grid = lpd->CurrGrid;
  else
    grid = lpd->LastGrid;
  if (key->Flags & KEY_FLG_SINGLE)
    while (key->Flags & KEY_FLG_INTERRUPTED)
      usleep(50);
  LOCK_KEY(key);
  for (pos = 0; pos < KEY_NB_SIMULTANEOUS; ++pos)
    if (!af->Flg[pos]) {
      af->Flg[pos] = 1;
      break;
    }
  g_object_set(GST_OBJECT(af->Vol), "volume", lpd->Mixer.Vol[key->Id % 16], NULL);
  UNLOCK_KEY(key);
 repeat_PlaySound:
  if (key->Tempo != KEY_TMO_INSTANT) {
    LOCK_APP(LockCond);
    pthread_cond_wait(&app->WaitTempo, &app->LockCond);
    UNLOCK_APP(LockCond);
  }
  LOCK_KEY(key);
  gst_element_set_state(af->Pipe, GST_STATE_PAUSED);
  if (key->Flags & KEY_FLG_STOP_LOOP)
    key->Flags |= KEY_FLG_IN_USE;
  gst_element_set_state(af->Pipe, GST_STATE_PLAYING);
  UNLOCK_KEY(key);
  g_main_loop_run(af->Loop);
  LOCK_KEY(key);
  gst_element_set_state(af->Pipe, GST_STATE_PAUSED);

  if (key->Audio->OggFile) //#!
    if (DirtyReloadOgg(key->Audio))
      key->Flags |= KEY_FLG_RUN_ERROR;

  gst_element_set_state(af->Pipe, GST_STATE_READY);
  if (key->Flags & KEY_FLG_STOP_LOOP)
    key->Flags &= ~(KEY_FLG_STOP_LOOP);
  else if ((key->Flags & KEY_FLG_LOOP) && !(key->Flags & KEY_FLG_INTERRUPTED) && !(key->Flags & KEY_FLG_RUN_ERROR)) {
    if (key->Audio->OggFile) //#!
      g_object_set(GST_OBJECT(af->Vol), "volume", lpd->Mixer.Vol[key->Id % 16], NULL);
    UNLOCK_KEY(key);
    goto repeat_PlaySound;
  }
  af->Flg[pos] = 0;
  --key->Count;
  if (key->Flags & KEY_FLG_INTERRUPTED)
    key->Flags &= ~(KEY_FLG_INTERRUPTED);
  else if (!key->Count)
    EndRunSwitchLight(key, STAT_IDLE_AUDIO, grid);
  UNLOCK_KEY(key);
  if (key->Link && !(key->Flags & KEY_FLG_LNK_SIMUL)) {
    t_key  *lnk = key->Link;
    if (key->Flags & KEY_FLG_LNK_PUSHED) {
      lnk->Flags &= ~(KEY_FLG_LNK_PUSHED);
      key->Flags |= KEY_FLG_LOCK;
      PushedKey(lnk);
      lnk->DefColor = ReleaseKey(lnk);
      key->Flags &= ~(KEY_FLG_LOCK);
      UpdateKey(lnk);
    }
    else if (key->Flags & KEY_FLG_LNK_LOOP) {
      key->Flags |= KEY_FLG_LOCK;
      PushedKey(lnk);
      lnk->DefColor = ReleaseKey(lnk);
      key->Flags &= ~(KEY_FLG_LOCK);
      UpdateKey(lnk);
    }
  }
  return (0);
}

/* Initialisation du système sonore */
void                InitSoundSystem()
{
  int               ac = 0;
  char              **av;

  gst_init (&ac, &av);
}
