/*
 *  'launchpadctrl' allows you to use your Novation launchpad (NVLPD01)
 *  Copyright (C) 2014-2017  Vincent Deca. known as Bigcake@ubuntu-fr.org
 *  (user@forum not email)
 *
 *  vincent.deca@neutralite.org
 *
 *  This file is part of 'launchpadctrl'.
 *
 * 'launchpadctrl' is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  'launchpadctrl' is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with 'launchpadctrl'.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app.h"

/* Allume ou eteint les boutons de flèche */
void               LightMenuArrows(t_lpd *lpd, char flg)
{
  unsigned char    cmd[5];

  cmd[0] = LP_MENU;
  cmd[1] = LP_TOP_LEARN;
  cmd[3] = LP_TOP_VIEW;
  if (flg) { /* Réactivation des boutons */
    LOCK_APP(LockFlg);
    lpd->Flags &= ~(LPD_FLG_MENU_LIGHT);
    UNLOCK_APP(LockFlg);
    if (lpd->CurrGrid < 3)
      cmd[2] = STAT_OFF;
    else
      cmd[2] = STAT_IDLE_MENU;
    if (lpd->CurrGrid > 5)
      cmd[4] = STAT_OFF;
    else
      cmd[4] = STAT_IDLE_MENU;
    SendToDevice(lpd, cmd, 5);
    cmd[1] = LP_TOP_PAGE_L;
    if ((lpd->CurrGrid % 3) == 0)
      cmd[2] = STAT_OFF;
    else
      cmd[2] = STAT_IDLE_MENU;
    cmd[3] = LP_TOP_PAGE_R;
    if ((lpd->CurrGrid % 3) == 2)
      cmd[4] = STAT_OFF;
    else
      cmd[4] = STAT_IDLE_MENU;
  }
  else { /* Déactivation des boutons */
    if (lpd->Flags & LPD_FLG_MENU_LIGHT)
      return;
    LOCK_APP(LockFlg);
    lpd->Flags |= LPD_FLG_MENU_LIGHT;
    UNLOCK_APP(LockFlg);
    cmd[2] = STAT_OFF;
    cmd[4] = STAT_OFF;
    SendToDevice(lpd, cmd, 5);
    cmd[1] = LP_TOP_PAGE_R;
    cmd[3] = LP_TOP_PAGE_L;
  }
  SendToDevice(lpd, cmd, 5);
}

/* Allume un bouton menu */
void               LightMenuMode(t_lpd *lpd, unsigned char id)
{
  unsigned char    cmd[5];
  
  /* Arrêt de l'ancien bouton et allumage du nouveau bouton */
  LOCK_APP(LockFlg);
  if (lpd->Flags & LPD_FLG_SESSION)
    cmd[1] = LP_TOP_SESSION;
  else if (lpd->Flags & LPD_FLG_MIXER)
    cmd[1] = LP_TOP_MIXER;
  else if (lpd->Flags & LPD_FLG_USER1)
    cmd[1] = LP_TOP_USER1;
  else if (lpd->Flags & LPD_FLG_USER2)
    cmd[1] = LP_TOP_USER2;
  else 
    ASSERT(1, "lpd->Flags non initialisé pour le menu");
  lpd->Flags &= ~(LPD_FLG_USER1 | LPD_FLG_USER2 | LPD_FLG_MIXER | LPD_FLG_SESSION);
  switch (id) {
  case LP_TOP_SESSION: lpd->Flags |= LPD_FLG_SESSION; break;
  case LP_TOP_MIXER: lpd->Flags |= LPD_FLG_MIXER; break;
  case LP_TOP_USER1: lpd->Flags |= LPD_FLG_USER1; break;
  case LP_TOP_USER2: lpd->Flags |= LPD_FLG_USER2; break;
  }
  UNLOCK_APP(LockFlg);
  cmd[0] = LP_MENU;
  cmd[2] = STAT_OFF;
  cmd[3] = id;
  cmd[4] = STAT_ACTIVE_MENU;
  SendToDevice(lpd, cmd, 5);
}


/* Activation de la grille User1 */
static unsigned char        ActiveUser1(t_lpd *lpd, char flg)
{
  if (lpd->Flags & LPD_FLG_USER1)
    return (STAT_ACTIVE_MENU);
  if (!flg)
    return (STAT_OFF);
  if (flg != 2)
    ChangeGrid(lpd, 9);
  LightMenuMode(lpd, LP_TOP_USER1);
  LightMenuArrows(lpd, 0);
  return (STAT_ACTIVE_MENU);
}

/* Activation de la grille User2 */
static unsigned char        ActiveUser2(t_lpd *lpd, char flg)
{
  if (lpd->Flags & LPD_FLG_USER2)
    return (STAT_ACTIVE_MENU);
  if (!flg)
    return (STAT_OFF);
  if (flg != 2)
    ChangeGrid(lpd, 10);
  LightMenuMode(lpd, LP_TOP_USER2);
  LightMenuArrows(lpd, 0);
  return (STAT_ACTIVE_MENU);
}

/* Changement de grille avec mise a jour du launchpad */
void               ChangeGrid(t_lpd *lpd, unsigned char nb)
{
  /* Sortie du mode mixer */
  if ((nb != GRID_VOLUME) && (lpd->Mixer.Flags & LPD_MIXER_VOL)) {
    lpd->Grid[GRID_VOLUME][SIDE_VOLUME].DefColor = 0;
    lpd->Mixer.Flags &= ~(LPD_MIXER_VOL);
  }
  /* Changement de Grille et mise à jour */
  if (lpd->CurrGrid < GRID_USER1)
    lpd->LastGrid = lpd->CurrGrid;
  if (nb == GRID_USER1)
    ActiveUser1(lpd, 2);
  else if (nb == GRID_USER2)
    ActiveUser2(lpd, 2);
  UpdateGrid(lpd, lpd->CurrGrid, nb);
  lpd->CurrGrid = nb;
  /* Mise à jour du shell */
  if (!(app->Flags & APP_FLG_SHELL_CMD))
    PrintLPEntry();
}

/* Réinitialise la couleur de tout les touches de la grille */
static void         ResetGridDefColor(t_key *grid)
{
  unsigned char     a;

  for (a = 0; a < 72; ++a) {
    if ((grid[a].ActualColor != STAT_OFF) && (a % 9) != 8) {
      grid[a].DefColor = STAT_OFF;
      UpdateKey(&grid[a]);
    }
  }
}

/* Gestion du menu en mode de dessin */
static unsigned char      MenuDrawMode(t_lpd *lpd, unsigned char key, char flg)
{
  unsigned char           col = 0;

  switch (key) {
  case LP_TOP_LEARN: col = LP_FULL_GREEN; break;
  case LP_TOP_VIEW: col = LP_MED_GREEN; break;
  case LP_TOP_PAGE_L: col = LP_LOW_GREEN; break;
  case LP_TOP_PAGE_R: col = LP_LOW_RED; break;
  case LP_TOP_SESSION: col = LP_MED_RED; break;
  case LP_TOP_USER1: col = LP_FULL_RED; break;
  case LP_TOP_USER2: ResetGridDefColor(lpd->LightGrid);
    return (STAT_OFF);
  case LP_TOP_MIXER: DisableDrawMode(lpd);
    return (LP_FULL_GREEN);
  }
  if (!flg)
    return (col);
  lpd->TempColor = col;
  lpd->LightGrid[SIDE_ARM].DefColor = col;
  if (lpd->LightGrid[SIDE_ARM].DefColor != lpd->LightGrid[SIDE_ARM].ActualColor)
    UpdateKey(&lpd->LightGrid[SIDE_ARM]);
  return (col);
}

/* Gestion du menu */
unsigned char      ManageMenu(t_lpd *lpd, unsigned char key, char flg)
{
  unsigned char    cmd[3];

  if (lpd->Flags & LPD_FLG_DRAW_MODE)
    return (MenuDrawMode(lpd, key, flg));
  cmd[0] = LP_MENU;
  switch (key) {
  case LP_TOP_LEARN:
    if ((lpd->Flags & LPD_FLG_MENU_LIGHT) || (lpd->CurrGrid < 3))
      return (flg ? STAT_PUSH_ERROR : STAT_OFF);
    if (flg) {
      ChangeGrid(lpd, lpd->CurrGrid - 3);
      if (lpd->CurrGrid > 2) {
	cmd[1] = LP_TOP_VIEW;
	cmd[2] = STAT_IDLE_MENU;
	SendToDevice(lpd, cmd, 3);
      }
      else break;
      return (STAT_ACTIVE_MENU);
    }
    if (lpd->CurrGrid > 2)
      return (STAT_IDLE_MENU);
    break;

  case LP_TOP_VIEW:
    if ((lpd->Flags & LPD_FLG_MENU_LIGHT) || (lpd->CurrGrid > 5))
      return (flg ? STAT_PUSH_ERROR : STAT_OFF);
    if (flg) {
      ChangeGrid(lpd, lpd->CurrGrid + 3);
      if (lpd->CurrGrid < 6) {
	cmd[1] = LP_TOP_LEARN;
	cmd[2] = STAT_IDLE_MENU;
	SendToDevice(lpd, cmd, 3);
      }
      else break;
      return (STAT_ACTIVE_MENU);
    }
    if (lpd->CurrGrid < 6)
      return (STAT_IDLE_MENU);
    break;

  case LP_TOP_PAGE_L:
    if ((lpd->Flags & LPD_FLG_MENU_LIGHT) || ((lpd->CurrGrid % 3) == 0))
      return (flg ? STAT_PUSH_ERROR : STAT_OFF);
    if (flg) {
      ChangeGrid(lpd, lpd->CurrGrid - 1);
      if ((lpd->CurrGrid % 3) == 1) {
	cmd[1] = LP_TOP_PAGE_R;
	cmd[2] = STAT_IDLE_MENU;
	SendToDevice(lpd, cmd, 3);
      }
      else break;
      return (STAT_ACTIVE_MENU);
    }
    if ((lpd->CurrGrid % 3) > 0)
      return (STAT_IDLE_MENU);
    break;

  case LP_TOP_PAGE_R:
    if ((lpd->Flags & LPD_FLG_MENU_LIGHT) || ((lpd->CurrGrid % 3) == 2))
      return (flg ? STAT_PUSH_ERROR : STAT_OFF);
    if (flg) {
      ChangeGrid(lpd, lpd->CurrGrid + 1);
      if ((lpd->CurrGrid % 3) == 1) {
	cmd[1] = LP_TOP_PAGE_L;
	cmd[2] = STAT_IDLE_MENU;
	SendToDevice(lpd, cmd, 3);
      }
      else break;
      return (STAT_ACTIVE_MENU);
    }
    if ((lpd->CurrGrid % 3) < 2)
      return (STAT_IDLE_MENU);
    break;
    /* Bouton 'Session' */
  case LP_TOP_SESSION:
    if (lpd->Flags & LPD_FLG_SESSION) {
      if (flg) {
	ChangeGrid(lpd, 0);
	LightMenuArrows(lpd, 1);
      }
      return (STAT_ACTIVE_MENU);
    }
    if (!flg)
      return (STAT_OFF);
    ChangeGrid(lpd, lpd->LastGrid);
    LightMenuMode(lpd, LP_TOP_SESSION);
    LightMenuArrows(lpd, 1);
    return (STAT_ACTIVE_MENU);

  case LP_TOP_USER1:
    return (ActiveUser1(lpd, flg));

  case LP_TOP_USER2:
    return (ActiveUser2(lpd, flg));

  case LP_TOP_MIXER:
    if (lpd->Flags & LPD_FLG_MIXER) {
      if (lpd->Mixer.Flags & LPD_MIXER_2ND_PUSH)
	CancelSecondPush(lpd);
      return (STAT_ACTIVE_MENU);
    }
    if (!flg)
      return (STAT_OFF);
    //# possibilité de garder la dernière page activé du mixer 
    lpd->Mixer.Flags |= LPD_MIXER_VOL;
    lpd->Grid[GRID_VOLUME][SIDE_VOLUME].DefColor = STAT_IDLE_MENU;
    ChangeGrid(lpd, GRID_VOLUME);
    LightMenuMode(lpd, LP_TOP_MIXER);
    LightMenuArrows(lpd, 0);
    return (STAT_ACTIVE_MENU);
  }
  return (0);
}
