NAME = launchpadctrl

SRC = main.c \
	cmd_mode.c \
	cmd_help.c \
	cmd_link.c \
	cmd_save.c \
	cmd_set.c \
	get_options.c \
	get_shell_entry.c \
	gstream.c \
	handle_events.c \
	launchpads.c \
	lights.c \
	load_file.c \
	network_cmd.c \
	manage_menu.c \
	midi.c \
	mixer_vol.c \
	shell.c \
	side_keys.c \
	strings.c \
	tools.c

OBJ = $(SRC:.c=.o)

CFLAGS = -O3 -D_POSIX_C_SOURCE=200112L -D_XOPEN_SOURCE -D_XOPEN_SOURCE_EXTENDED -std=gnu99 -Wno-unused-result `pkg-config --cflags gstreamer-0.10`
LIB = -L/usr/local/lib -lpthread `pkg-config --libs gstreamer-0.10` -lasound

all: $(OBJ)
	gcc -O3 -o $(NAME) $(OBJ) $(LIB)
	strip $(NAME)

install:
	chmod +x install.sh
	./install.sh

remove:
	@echo "[FR] Suppression de l'executable et de la page de man"
	@echo "[EN] Removing executable file and man page"
	@rm /usr/bin/$(NAME)
	@rm /usr/share/man/man1/launchpadctrl.1.gz
	@echo "********************"
	@echo "[FR] Désinstallation terminé"
	@echo "[EN] Uninstall complete"

clean :
	rm -rf $(NAME) *.o *~  \#*