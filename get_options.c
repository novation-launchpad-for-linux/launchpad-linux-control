/*
 *  'launchpadctrl' allows you to use your Novation launchpad (NVLPD01)
 *  Copyright (C) 2014-2017  Vincent Deca. known as Bigcake@ubuntu-fr.org
 *  (user@forum not email)
 *
 *  vincent.deca@neutralite.org
 *
 *  This file is part of 'launchpadctrl'.
 *
 * 'launchpadctrl' is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  'launchpadctrl' is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with 'launchpadctrl'.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app.h"

/* Affichage de l'aide */
static void         Usage(const char *name)
{
  printf("\nUsage: %s [OPTIONS] [-d </dev/nlpXXX>] [-l <language file>] [file.nlp]...\n\nOPTIONS:\n", name);
  printf(" -C, --no-auto-connect\t%s\n", app->Strs[87]);
  printf(" -d, --device\t\t%s\n", app->Strs[15]);
  printf(" -h, --help\t\t%s\n", app->Strs[13]);
  printf(" -l, --language\t\t%s\n", app->Strs[195]);
  printf(" -m, --midi\t\t%s\n", app->Strs[135]);
  printf(" -Q, --quiet\t\t%s\n", app->Strs[151]);
  printf(" -S, --no-shell\t\t%s\n", app->Strs[46]);
  printf(" -v, --version\t\t%s\n", app->Strs[196]); /* Show version and quit */
  printf("\nVersion %s\n", APP_VERSION);
  QuitApp(0);
}

static void         option_v()
{
  printf("%s\n", APP_VERSION);
  app->Flags |= APP_FLG_SHELL_CMD;
  QuitApp(0);
}

/* Récupération des options */
void                GetOptions(int ac, char **av)
{
  int               a, b;
  char              flg;
  t_lpd             *lpd;

  for (lpd = 0, a = 1; av[a]; ++a) {
    if (av[a][0] == '-') {
      if (av[a][1] == '-') {
	if (!strcmp(&av[a][2], "no-shell"))
	  app->Flags |= APP_FLG_NO_SHELL;
	else if (!strcmp(&av[a][2], "no-auto-connect"))
	  app->Flags |= APP_FLG_NO_CHECK;
	else if (!strncmp(&av[a][2], "language", 8)) {
	  if (av[a][10] == '=')
	    LoadLangFile(&av[a][11]);
	  else if (av[a][10]) {
	    printf("\n%s : '--%s'\n", app->Strs[26], &av[a][2]);
	    Usage(av[0]);
	  }
	  else if (!av[a + 1]) {
	    printf("\n\033[31m%s\033[00m: %s: '%s'\n\n", app->Strs[1], app->Strs[27], &av[a][2]);
	    exit(EXIT_FAILURE);
	  }
	  else
	    LoadLangFile(av[++a]);
	}
	else if (!strcmp(&av[a][2], "midi"))
	  app->Flags |= APP_FLG_MIDI_MODE;
	else if (!strcmp(&av[a][2], "quiet")) {
	  close(2);
	  if (open("/dev/null", O_RDWR) < 0)
	    ExitWithMsg(app->Strs[155]);
	  app->Flags |= APP_FLG_QUIET;
	}
	else if (!strncmp(&av[a][2], "device", 6)) {
	  if (av[a][8] == '=') {
	    if (!(lpd = GetLaunchpadStruct()))
	      if (!(lpd = InitLaunchpad()))
		ExitWithMsg(app->Strs[0]);
	    OpenDevice(lpd, &av[a][9]);
	  }
	  else if (av[a][8]) {
	    printf("\n%s : '--%s'\n", app->Strs[26], &av[a][2]);
	    Usage(av[0]);
	  }
	  else if (!av[a + 1]) {
	    printf("\n\033[31m%s\033[00m: %s: '%s'\n\n", app->Strs[1], app->Strs[27], &av[a][2]);
	    exit(EXIT_FAILURE);
	  }
	  else {
	    if (!(lpd = GetLaunchpadStruct()))
	      if (!(lpd = InitLaunchpad()))
		ExitWithMsg(app->Strs[0]);
	    OpenDevice(lpd, av[++a]);
	  }
	}
	else if (!strcmp(&av[a][2], "help"))
	  Usage(av[0]);
	else if (!strcmp(&av[a][2], "version"))
	  option_v();
	else {
	  printf("\n%s : '--%s'\n", app->Strs[26], &av[a][2]);
	  Usage(av[0]);
	}
      }
      else {
	for (flg = 0, b = 1; !flg && av[a][b]; ++b) {
	  switch (av[a][b]) {
	  case 'C': app->Flags |= APP_FLG_NO_CHECK; break;
	  case 'd':
	    if (!av[a + 1]) {
	      printf("\n\033[31m%s\033[00m: %s: '%c'\n\n", app->Strs[1], app->Strs[27], av[a][b]);
	      exit(EXIT_FAILURE);
	    }
	    if (!(lpd = GetLaunchpadStruct()))
	      if (!(lpd = InitLaunchpad()))
		ExitWithMsg(app->Strs[0]);
	    OpenDevice(lpd, av[++a]);
	    flg = 1;
	    break;
	  app->Flags |= APP_FLG_MIDI_MODE;
	  default : printf("\n%s : '%c'\n", app->Strs[26], av[a][b]);
	  case 'h': Usage(av[0]);
	  case 'l':
	    if (!av[a + 1]) {
	      printf("\n\033[31m%s\033[00m: %s: '%c'\n\n", app->Strs[1], app->Strs[27], av[a][b]);
	      exit(EXIT_FAILURE);
	    }
	    LoadLangFile(av[++a]);
	    flg = 1;
	    break;
	  case 'm': app->Flags |= APP_FLG_MIDI_MODE; break;
	  case 'Q':
	    close(2);
	    if (open("/dev/null", O_RDWR) < 0)
	      ExitWithMsg(app->Strs[155]);
	    app->Flags |= APP_FLG_QUIET;
	    break;
	  case 'S': app->Flags |= APP_FLG_NO_SHELL; break;
	  case 'v': option_v();
	  }
	}
      }
    }
    else
      LoadConfigFile(av[a]);
  }
}
