#!/bin/bash

OLDIFS=$IFS
IFS=$(echo -en "\n\b")
if [ -e /usr/bin/sudo ] ; then
    SUDO="sudo"
else
    SUDO=""
fi
if [ -e /usr/bin/apt-get ] ; then
    distrib=1
elif [ -e /usr/bin/yum ] ; then
    distrib=2
else
    distrib=3
fi

pack=0

#***** langue
echo -e "*************** Launchpadctrl v0.23 ****************\n"
echo -e "\t1) Français"
echo -e "\t2) English\n"
while true; do
    read -p "Language ? [1/2] " ans
    case $ans in
        1 ) lang=1
            break;;
        2 ) lang=2
            break;;
    esac
done
echo

#***** licence
if [ $lang = 1 ] ; then
    echo "Ce logiciel est distribué sous licence GPL"
    echo -e "Cette licence est disponible dans le fichier 'LICENSE'\n"
    ask="Avez-vous lu, compris, accepté cette licence ? [oui/non]"
elif [ $lang = 2 ] ; then
    echo "This software is distributed under the GNU GPL License"
    echo -e "This license is available in the 'LICENSE' file\n"
    ask="Did you read, understood, accepted it ? [yes/no]"
fi

while true; do
    read -p "$ask" ans
    case $ans in
        [yoYO]* )
            break;;
        * ) exit;
    esac
done


#******************************** PREREQUIS *******************************************
function diy_install {
    if [ $lang = 1 ] ; then
	echo -e "\nDes paquets sont manquant pour la compilation :$name (ou superieur)"
	echo "Impossible d'utiliser /usr/bin/apt-get ou /usr/bin/yum pour les installer"
	echo "Il faut que vous installiez manuellement les pré-requis qui ont eu un 'nok'"
    elif [ $lang = 2 ] ; then
	echo -e "\nFailed to install the missing packages :$name (ou superieur)"
	echo "Failed to use /usr/bin/apt-get or /usr/bin/yum to install them"
	echo "You have to install manually the prerequisite which got 'nok'"
    fi
    exit
}

function requiered {
    if [ $lang = 1 ] ; then
	echo -e "\n=> Vérification des pré-requis:"
    elif [ $lang = 2 ] ; then
	echo -e "\n=> Checking prerequisite:"
    fi

    #======= gstreamer
    if [ -e /usr/include/gstreamer-0.10/gst/gst.h ] ; then
	echo -e "- gstreamer-dev : \\033[1;32mok\\033[0;39m"
    else
	echo -e "- gstreamer-dev : \\033[1;31mok\\033[0;39m"
	if [ $distrib = 1 ] ; then
	    name=" libgstreamer0.10-dev"
	    missing=" $(apt-cache search libgstreamer0 | grep "\-dev" | grep -v cil | cut -d ' ' -f1)"
	else
	    name=" gstreamer-devel"
	    missing=" $(yum search gstreamer-devel | grep '^gstreamer-devel\.' | cut -d ' ' -f1)"
	fi
	pack=1
    fi

    #======= alsa
    if [ -e /usr/include/alsa/asoundlib.h ] ; then
	echo -e "- alsa lib dev : \\033[1;32mok\\033[0;39m"
    else
	echo -e "- alsa lib dev : \\033[1;31mnok\\033[0;39m"
	if [ $distrib = 1 ] ; then
	    name="$name libasound2-dev"
	    missing="$missing $(apt-cache search libasound | grep "\-dev" | grep -v oss | grep -v ocaml | cut -d ' ' -f1)"
	else
	    name="$name alsa-lib-devel"
	    missing="$missing $(yum search alsa-lib-devel | grep 'alsa-lib-devel\.' | cut -d ' ' -f1)"
	fi
	pack=1
    fi

    #======= plugin mp3
    if [ $distrib = 1 ] ; then
	test=$(dpkg -l | grep gstreamer | grep plugin | grep ugly)
    else
	test=$(yum list gstreamer* | grep plugin | grep ugly | grep -v "=====")
    fi
    if [[ "$test" != "" ]] ; then
	echo -e "- gstreamer mp3 plugin : \\033[1;32mok\\033[0;39m"
    else
	echo -e "- gstreamer mp3 plugin : \\033[1;31mnok\\033[0;39m"
	if [ $distrib = 1 ] ; then
	    name="$name gstreamer0.10-plugins-ugly"
	    missing="$missing $(apt-cache search gstreamer | grep plugin | grep ugly | grep -v dbg | grep -v doc | cut -d ' ' -f1)"
	    pack=1
	else
	    if [ $lang = 1 ] ; then
		echo -e "\nLe plugin mp3 de gstreamer n'est pas installé, il faudra l'installer manuellement\n"
	    else
		echo -e "\nThe mp3 gstreamer plugin is not installed, you must install it mannually\n"
	    fi
	fi
    fi

    #====== résultat prérequis
    if [ $pack = 0 ] ; then
	if [ $lang = 1 ] ; then
	    echo -e "Tout semble présent pour la compilation\n"
	elif [ $lang = 2 ] ; then
	    echo -e "Everything seems to be there for the compilation\n"
	fi
    else
	if [ -e /usr/bin/apt-get ] ; then
	    if [ $lang = 1 ] ; then
		echo -e "\n=> Installation des paquets manquants :$missing"
	    elif [ $lang = 2 ] ; then
		echo -e "\n=> Missing packages installation :$missing"
	    fi
	    eval "$SUDO apt-get install$missing"
	    if [ $? -ne 0 ] ; then
		if [ $lang = 1 ] ; then
		    echo -e "\nQuelque chose s'est mal passé lors de l'installation des pré-requis :$missing"
		elif [ $lang = 2 ] ; then
		    echo -e "\nSomething went wrong when installing missing packages :$missing"
		fi
		diy_install
	    fi
	elif [ -e /usr/bin/yum ] ; then
	    eval "$SUDO yum -y update; sudo yum -y install gcc $(yum search kernel-devel | grep devel | grep -v '====' | cut -d ' ' -f1) $missing"
	else
	    diy_install
	fi
    fi
}

#******************************* COMPILATION ******************************************
if [ -e launchpadctrl ] ; then
    if [ $lang = 1 ] ; then
	echo -e "Exécutable déjà créé, je saute la compilation !\n"
    elif [ $lang = 2 ] ; then
	echo -e "Executable file already created, skipping compilation !\n"
    fi
else
    requiered
    if [ $lang = 1 ] ; then
	echo -e "=> Création de l'exécutable (compilation) :\n"
    elif [ $lang = 2 ] ; then
	echo -e "=> Creating the executable file (compilation) :\n"
    fi
    CORE=`grep -c "model name" /proc/cpuinfo`
    make -j$CORE
    if [ -e launchpadctrl ] ; then
	if [ $lang = 1 ] ; then
	    echo -e "\n=> Compilation terminé avec succès\n"
	elif [ $lang = 2 ] ; then
	    echo -e "\n=> Compilation finished with success\n"
	fi
    else
	if [ $lang = 1 ] ; then
	    echo -e "\nErreur lors de la compilation\n=> Arrêt de l'installation"
	elif [ $lang = 2 ] ; then
	    echo -e "\nA problème occured while compiling\n=> Stopping installation"
	fi
	exit
    fi
fi

#********************************** COPIE *********************************************
if [ $lang = 1 ] ; then
    echo -e "=> Copie de l'executable dans '/usr/bin' :"
elif [ $lang = 2 ] ; then
    echo -e "=> Copying executable into '/usr/bin' :"
fi
eval "$SUDO cp launchpadctrl /usr/bin"
if [ $? = 1 ] ; then
    if [ $lang = 1 ] ; then
        echo -e "\nProblème lors de la copie... manque de droit root/sudo ?\nArrêt de l'installation"
    elif [ $lang = 2 ] ; then
        echo -e "\nProblem when copying... missing root/sudo right?\nStopping installation"
    fi
    exit
fi
#********************************* COPIE DU MAN ********************************************
if [ $lang = 1 ] ; then
    echo -e "\n=> Copie de la page 'man' dans '/usr/share/man/man1'\n"
    eval "$SUDO cp launchpadctrl.fr.gz /usr/share/man/man1/launchpadctrl.1.gz"
elif [ $lang = 2 ] ; then
    echo -e "\n=> Copying man page into '/usr/share/man/man1'"
    eval "$SUDO cp launchpadctrl.en.gz /usr/share/man/man1/launchpadctrl.1.gz"
fi
if [ $? = 1 ] ; then
    if [ $lang = 1 ] ; then
	echo -e "\nUn erreur est apparu, impossible de copier la page de man"
    elif [ $lang = 2 ] ; then
	echo -e "\nSomehing went wrong, failed to copy man page"
    fi
fi
if [ $lang = 1 ] ; then
    echo -e "=> Installation terminée, tu peux maintenant lancer la commande 'launchpadctrl'\n"
    echo "- Jete un coup d'oeil au fichier changes-fr.log"
    echo "  Ca donne pas mal d'infos sur les fonctionnalités ajoutées"
    echo "- N'hésite pas à me contacter pour toute idée / correction / aide"
    echo "  Pour plus d'infos : forum ubuntu-fr.org, section 'Vos développements libres'"
    echo -e " Sujet : 'Driver et logiciel de gestion du launchpad de Novation'\n"
elif [ $lang = 2 ] ; then
    echo -e "=> Installation done, you can now run 'launchpadctrl' command\n"
    echo "Don't hesitate to read the changes-en.log file"
    echo "It brings a lots of informations about added fonctionnalities"
    echo -e "You can contact me for some information / correction / idea / help\n"
fi

IFS=$OLDIFS
