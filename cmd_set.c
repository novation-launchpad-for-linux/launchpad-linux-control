/*
 *  'launchpadctrl' allows you to use your Novation launchpad (NVLPD01)
 *  Copyright (C) 2014-2017  Vincent Deca. known as Bigcake@ubuntu-fr.org
 *  (user@forum not email)
 *
 *  vincent.deca@neutralite.org
 *
 *  This file is part of 'launchpadctrl'.
 *
 * 'launchpadctrl' is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  'launchpadctrl' is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with 'launchpadctrl'.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app.h"

/* Finalise la configuration de la touche si tout est ok */
static void          FinalizeKey(t_key *key)
{
  unsigned short     a;

  LOCK_KEY(key);
  if (key->CmdFile) {
    key->CmdLen = strlen(key->CmdFile);
    switch (key->Type) {
    case KEY_TYPE_AUDIO:
      key->PushedColor = STAT_PUSH_SUCCESS;
      if (!(key->Flags & (KEY_FLG_HOLD_IT | KEY_FLG_KEEP | KEY_FLG_LOOP | KEY_FLG_PAUSE | KEY_FLG_SINGLE))) {
	PrintError(152, ERR_TYPE_SIMPLE, 0);
	key->Flags |= KEY_FLG_KEEP;
      }
      GetAudioFile(key);
      break;
    case KEY_TYPE_EXEC:
      if (key->CmdTab) {
	free(key->CmdTab);
	key->CmdTab = 0;
      }
      if (!(key->CmdTab = LineToTab(key->CmdFile))) {
	PrintError(9, ERR_TYPE_ERRNO, 0);
	key->CmdLen = 0;
	return;
      }
      key->PushedColor = STAT_PUSH_SUCCESS;
      key->DefColor = STAT_IDLE_CMD;
      if (key->Flags & KEY_FLG_PAUSE) {
	PrintError(152, ERR_TYPE_SIMPLE, 0);
	key->Flags &= ~(KEY_FLG_PAUSE);
	key->Flags |= KEY_FLG_KEEP;
      }
      break;
    case KEY_TYPE_SERVER: 
      key->PushedColor = STAT_PUSH_SUCCESS;
      key->DefColor = STAT_IDLE_ERROR;
      for (a = 0; a < app->MaxServers; ++a)
	if (app->Servers[a])
	  if (app->Servers[a]->Uid == key->Count) {
	    if (app->Servers[a]->Socket)
	      key->DefColor = STAT_IDLE_CMD;
	    break;
	  }
      key->Flags = KEY_FLG_KEEP;
      break;
    default: UNLOCK_KEY(key); return;
    }
    key->Flags &= ~(KEY_FLG_NOT_INIT);
    UpdateKey(key);
  }
  UNLOCK_KEY(key);
}

/* Modification de la touche */
static unsigned char ModifyKey(t_key *key, char *param)
{
  unsigned int       flag;
  char               *mode;

  for (; *param && (*param != ' ') && (*param != '\t'); ++param);
  for (; *param && (*param == ' ' || *param == '\t'); ++param);
  if (!(*param) || (*param == '?')) {
    CmdHelp("set 1");
    return (1);
  }
  mode = param;
  for (; *param && (*param != ' ') && (*param != '\t'); ++param);
  for (; *param && (*param == ' ' || *param == '\t'); ++param);
  if (!strncmp(mode, "work", 4)) {
    switch (*param) {
    case '0': flag = 0; break;
    case 'H': flag = KEY_FLG_HOLD_IT; break;
    case 'K': flag = KEY_FLG_KEEP; break;
    case 'L': flag = KEY_FLG_LOOP; break;
    case 'P': flag = KEY_FLG_PAUSE; break;
    case 'S': flag = KEY_FLG_SINGLE; break;
    case '?':
    case 0: CmdHelp("set 2"); return (1);
    default: PrintError(19, ERR_TYPE_SIMPLE, 0); return (2);
    }
    LOCK_KEY(key);
    key->Flags &= ~(KEY_FLG_HOLD_IT | KEY_FLG_KEEP | KEY_FLG_LOOP | KEY_FLG_PAUSE | KEY_FLG_SINGLE);
    key->Flags |= flag;
    UNLOCK_KEY(key);
  }
  else if (!strncmp(mode, "dep", 3)) {
    switch (*param) {
    case '0': flag = 0; break;
    case 'B': flag = (KEY_FLG_COL_STOP | KEY_FLG_LINE_STOP); break;
    case 'C': flag = KEY_FLG_COL_STOP; break;
    case 'L': flag = KEY_FLG_LINE_STOP; break;
    case '?':
    case 0: CmdHelp("set 3"); return (1);
    default: PrintError(24, ERR_TYPE_SIMPLE, 0); return (2);
    }
    LOCK_KEY(key);
    key->Flags &= ~(KEY_FLG_COL_STOP | KEY_FLG_LINE_STOP);
    key->Flags |= flag;
    UNLOCK_KEY(key);
  }
  else if (!strncmp(mode, "type", 4)) {
    LOCK_KEY(key);
    switch (*param) {
    case 'A': key->Type = KEY_TYPE_AUDIO; break;
    case 'E': key->Type = KEY_TYPE_EXEC; break;
    case 'S': key->Type = KEY_TYPE_SERVER; break;
    case '?':
    case 0: UNLOCK_KEY(key); CmdHelp("set 4"); return (1);
    default: UNLOCK_KEY(key); PrintError(29, ERR_TYPE_SIMPLE, 0); return (2);
    }
    UNLOCK_KEY(key);
  }
  else if (!strncmp(mode, "file", 4)) {
    if (!(*param) || (*param) == '?') {
      CmdHelp("set 5");
      return (1);
    }
    if ((mode = strdup(param)) == NULL) {
      PrintError(9, ERR_TYPE_ERRNO, 0);
      return (3);
    }
    LOCK_KEY(key);
    if (key->CmdFile)
      free(key->CmdFile);
    key->CmdFile = mode;
    UNLOCK_KEY(key);
  }
  else if (!strncmp(mode, "tempo", 5)) {
    if (!(*param) || (*param) == '?') {
      CmdHelp("set 6");
      return (1);
    }
    LOCK_KEY(key);
    if (*param == '0')
      key->Tempo = KEY_TMO_INSTANT;
    else
      key->Tempo = KEY_TMO_1;
    UNLOCK_KEY(key);
  }
  else if (!strncmp(mode, "light", 5)) {
    CmdHelp("set 1");
    return (1);
  }
  else {
    PrintError(166, ERR_TYPE_FILE, mode);
    return (2);
  }
  return (0);
}

/* Application à toute la grille */
static void          SetToAll(t_key *grid, char *param)
{
  t_key              *key;
  unsigned char      a;

  for (a = 0; a < 72; ++a) {
    if (a % 9 < 8) {
      key = &grid[a];
      if (ModifyKey(key, param))
	break;
      FinalizeKey(key);
    }
  }
}

/* Commande 'set' */
void                 CmdSet(char *param)
{
  int                col, line;
  t_key              *key;
  t_lpd              *lpd;

  lpd = app->Launchpad[app->CurrLpd];
  if (!param || (*param == '?')) {
    CmdHelp("set");
    return;
  }
  if (!strncmp(param, "all", 3)) {
    SetToAll(lpd->Grid[lpd->LastGrid], &param[3]);
    return;
  }
  col = atoi(param);
  line = col / 10;
  if (line < 1 || line > 8) {
    LOCK_APP(LockOutput);
    printf("\033[31m%s\033[00m: '%d', %s\n", app->Strs[1], line, app->Strs[17]);
    UNLOCK_APP(LockOutput);
    return;
  }
  col -= (line * 10);
  if (col < 1 || col > 8) {
    LOCK_APP(LockOutput);
    printf("\033[31m%s\033[00m: '%d', %s\n", app->Strs[1], col, app->Strs[18]);
    UNLOCK_APP(LockOutput);
    return;
  }
  if (lpd->CurrGrid > GRID_USER2)
    key = &lpd->Grid[lpd->LastGrid][(col - 1) * 9 + (line - 1)];
  else
    key = &lpd->Grid[lpd->CurrGrid][(col - 1) * 9 + (line - 1)];
  if (key->Flags & KEY_FLG_IN_USE) {
    PrintError(136, ERR_TYPE_SIMPLE, 0);
    return;
  }
  if (!ModifyKey(key, param))
    FinalizeKey(key);
}
