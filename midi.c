/*
 *  'launchpadctrl' allows you to use your Novation launchpad (NVLPD01)
 *  Copyright (C) 2014-2017  Vincent Deca. known as Bigcake@ubuntu-fr.org
 *  (user@forum not email)
 *
 *  vincent.deca@neutralite.org
 *
 *  This file is part of 'launchpadctrl'.
 *
 * 'launchpadctrl' is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  'launchpadctrl' is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with 'launchpadctrl'.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app.h"

/* Mode MIDI, touche appuyée */
void               HandlePushForMidi(t_lpd *lpd, unsigned char *cmd)
{
  snd_seq_event_t  ev;

  if (cmd[0] == LP_MENU || (cmd[1] % 16) == 8) {
    PrintError(171, ERR_TYPE_SIMPLE, 0);
    cmd[2] = STAT_PUSH_ERROR;
    return;
  }
  cmd[2] = STAT_PUSH_NOT_INIT;

  snd_seq_ev_clear(&ev);
  snd_seq_ev_set_source(&ev, 0);
  snd_seq_ev_set_subs(&ev);
  snd_seq_ev_set_direct(&ev);
  ev.type = SND_SEQ_EVENT_NOTEON;
  ev.data.note.channel = 1;

  ev.data.note.note = (cmd[1] / 16) * 8 + cmd[1] % 16;

  ev.data.note.velocity = 127;
  snd_seq_event_output(app->MidiHandle, &ev);
  snd_seq_drain_output(app->MidiHandle);
}

/* Mode MIDI, touche relachée */
void               HandleReleaseForMidi(t_lpd *lpd, unsigned char *cmd)
{
  snd_seq_event_t  ev;

  cmd[2] = STAT_OFF;
  if (cmd[0] == LP_MENU || (cmd[1] % 16) == 8)
    return;

  snd_seq_ev_clear(&ev);
  snd_seq_ev_set_source(&ev, 0);
  snd_seq_ev_set_subs(&ev);
  snd_seq_ev_set_direct(&ev);
  ev.type = SND_SEQ_EVENT_NOTEOFF;
  ev.data.note.channel = 1;

  ev.data.note.note = (cmd[1] / 16) * 8 + cmd[1] % 16;

  ev.data.note.velocity = 127;
  snd_seq_event_output(app->MidiHandle, &ev);
  snd_seq_drain_output(app->MidiHandle);
}

/* Initialisation du port midi */
void             InitMidi()
{
  snd_seq_t      *handle;
  int            ret;

  if ((ret = snd_seq_open(&handle, "hw", SND_SEQ_OPEN_DUPLEX, 0)) < 0)
    goto end_InitMidi;
  app->MidiHandle = handle;
  if ((ret = snd_seq_set_client_name(handle, "Novation Launchpad")) < 0)
    goto end_InitMidi;
  if ((ret = snd_seq_create_simple_port(handle, "Novation Launchpad",
					SND_SEQ_PORT_CAP_READ | SND_SEQ_PORT_CAP_SUBS_READ,
					SND_SEQ_PORT_TYPE_PORT)) < 0)
    goto end_InitMidi;
  app->MidiPort = ret;
  //CloseSoudSystem();
  return;
 end_InitMidi:
  if (app->MidiHandle) {
    if (app->MidiPort > -1)
      snd_seq_delete_simple_port(app->MidiHandle, app->MidiPort);
      
    snd_seq_close(app->MidiHandle);
    app->MidiHandle = 0;
  }
  printf("\033[31m%s\033[00m: %s\n", app->Strs[1], app->Strs[141]);
}
