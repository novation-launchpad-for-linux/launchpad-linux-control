/*
 *  'launchpadctrl' allows you to use your Novation launchpad (NVLPD01)
 *  Copyright (C) 2014-2017  Vincent Deca. known as Bigcake@ubuntu-fr.org
 *  (user@forum not email)
 *
 *  vincent.deca@neutralite.org
 *
 *  This file is part of 'launchpadctrl'.
 *
 * 'launchpadctrl' is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  'launchpadctrl' is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with 'launchpadctrl'.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app.h"

/* Découpage d'une ligne et retour d'un tableau de mot */
char              **LineToTab(char *line)
{
  char            **tab;
  unsigned int    a, nb;

  /* Comptage des mots */
  for (nb = 1, a = 0; line[a]; ++nb) {
    for (; line[a] == ' ' || line[a] == '\t'; ++a);
    if (!line[a])
      break;
    for (; line[a] && line[a] != ' ' && line[a] != '\t'; ++a)
      if (line[a] == '\\' && line[a + 1] == '"') {
        for (++a; line[a] && line[a] != '\\' && line[a + 1] != '"'; ++a);
        if (!line[a])
          --a;
      }
  }
  if (nb == 1)
    return (0);
  tab = 0;
  if ((tab = malloc(nb * sizeof(char *))) == NULL)
    return (0);
  /* Transformation de la ligne en tableau */
  for (nb = a = 0; line[a]; ++nb) {
    for (; line[a] == ' ' || line[a] == '\t'; ++a);
    if (!line[a])
      break;
    if (a)
      line[a - 1] = '\0';
    tab[nb] = &line[a];
    for (; line[a] && line[a] != ' ' && line[a] != '\t'; ++a)
      if (line[a] == '"') {
        for (++a; line[a] && line[a] != '"'; ++a);
        if (!line[a])
          --a;
      }
  }
  tab[nb] = 0;
  return (tab);
}

/* Transformation d'un unsigned short en chaine de caratère */
void               UShortToStr(unsigned short nb, char *buff)
{
  unsigned char    pos = 0;
  int              div;
  char             c;

  for (div = 10000; div > 1; div /= 10) {
    c = (nb / div);
    if (c) {
      nb -= c * div;
      buff[pos] = '0' + c;
      ++pos;
    }
    else if (pos) {
      buff[pos] = '0';
      ++pos;
    }
  }
  buff[pos] = '0' + nb;
  buff[pos + 1] = '\0';
}

/* Transforme un float xxxx.xxxx en char */
void               FloatToStr(char *buff, const float nb)
{
  int              pr, pl, div;
  float            diff;
  unsigned char    pos = 0;
  char             c;

  memset(buff, 0, 10);
  pr = nb;
  diff = nb * 10000.0000;
  pl = (diff - pr * 10000);
  if (pr) {
    for (div = 10000; div > 1; div /= 10) {
      c = (pr / div);
      if (c) {
	pr -= c * div;
	buff[pos] = '0' + c;
	++pos;
      }
      else if (pos) {
	buff[pos] = '0';
	++pos;
      }
    }
  }
  buff[pos] = '0' + pr;
  ++pos;
  if (pl) {
    buff[pos++] = '.';
    for (div = 1000; div > 1; div /= 10) {
      c = (pl / div);
      if (c) {
	pl -= c * div;
	buff[pos] = '0' + c;
	++pos;
      }
      else if (pos) {
	buff[pos] = '0';
	++pos;
      }
    }
    if (pl) {
      buff[pos] = '0' + pl;
      ++pos;
    }
  }
  buff[pos] = '\0';
}

/* Remplace le 1er '.' par une ',', atof veut une virgule si fr_FR */
void               FrenchFloat(char *str)
{
  for (; *str; ++str)
    if (*str == '.') {
      *str = ',';
      break;
    }
}

#ifdef DEV_MODE
char               *ColorStr(unsigned char nb)
{
  switch (nb) {
  case LP_LOW_ORANGE: return ("LP_LOW_ORANGE");
  case LP_FULL_ORANGE: return ("LP_FULL_ORANGE");
  case LP_LOW_YELLOW: return ("LP_LOW_YELLOW");
  case LP_FULL_YELLOW: return ("LP_FULL_YELLOW");
  case LP_LOW_GREEN: return ("LP_LOW_GREEN");
  case LP_FULL_GREEN: return ("LP_FULL_GREEN");
  case LP_LOW_RED: return ("LP_LOW_RED");
  case LP_FULL_RED: return ("LP_FULL_RED");
  case 0:
  case LP_OFF: return ("LP_OFF");
  default:
    printf("ColorStr %d\n", nb);
  }
  return (0);
}

void               PrintKey(t_key *key)
{
  LOCK_APP(LockOutput);
  printf("key[%d]: (%d, %d)\n", key->Id, key->Id % 16, key->Id / 16);
  printf("- Flags: ");
  if (key->Flags & KEY_FLG_PUSHED) printf("KEY_FLG_PUSHED ");
  if (key->Flags & KEY_FLG_IN_USE) printf("KEY_FLG_IN_USE ");
  if (key->Flags & KEY_FLG_SINGLE) printf("KEY_FLG_SINGLE ");
  if (key->Flags & KEY_FLG_LOOP) printf("KEY_FLG_LOOP ");
  if (key->Flags & KEY_FLG_INTERRUPTED) printf("KEY_FLG_KEEP ");
  if (key->Flags & KEY_FLG_NOT_INIT) printf("KEY_FLG_NOT_INIT ");
  if (key->Flags & KEY_FLG_STOP_LOOP) printf("KEY_FLG_STOP_LOOP ");
  if (key->Flags & KEY_FLG_RUN_ERROR) printf("KEY_FLG_RUN_ERROR ");
  if (key->Flags & KEY_FLG_COL_STOP) printf("KEY_FLG_COL_STOP ");
  if (key->Flags & KEY_FLG_LINE_STOP) printf("KEY_FLG_LINE_STOP ");
  if (key->Flags & KEY_FLG_HOLD_IT) printf("KEY_FLG_HOLD_IT ");
  if (key->Flags & KEY_FLG_LOCK) printf("KEY_FLG_LOCK ");
  if (key->Flags & KEY_FLG_PAUSE) printf("KEY_FLG_PAUSE ");
  if (key->Flags & KEY_FLG_IS_PAUSED) printf("KEY_FLG_IS_PAUSED ");
  switch (key->Type) {
  case KEY_TYPE_EXEC: printf("\n- Type: KEY_TYPE_EXEC\n"); break;
  case KEY_TYPE_AUDIO: printf("\n- Type: KEY_TYPE_AUDIO\n"); break;
  case KEY_TYPE_SERVER: printf("\n- Type: KEY_TYPE_SERVER\n"); break;
  default : printf("\n- Type: INVALIDE(%d)\n", key->Type); break;
  }
  printf("- CmdFile: '%s'\n", key->CmdFile);
  printf("- ActualColor: %s\n", ColorStr(key->ActualColor));
  printf("- DefColor: %s\n", ColorStr(key->DefColor));
  printf("- PushedColor: %s\n", ColorStr(key->PushedColor));
  printf("- Count: %d\n", key->Count);
  UNLOCK_APP(LockOutput);
}
#endif
