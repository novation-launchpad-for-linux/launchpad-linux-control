/*
 *  'launchpadctrl' allows you to use your Novation launchpad (NVLPD01)
 *  Copyright (C) 2014-2017  Vincent Deca. known as Bigcake@ubuntu-fr.org
 *  (user@forum not email)
 *
 *  vincent.deca@neutralite.org
 *
 *  This file is part of 'launchpadctrl'.
 *
 * 'launchpadctrl' is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  'launchpadctrl' is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with 'launchpadctrl'.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app.h"

/* Efface le launchpad et mets à jour la grille courante */
void               ClearGrid(t_lpd *lpd, unsigned char pos)
{
  unsigned char    cmd[3];

  cmd[0] = LP_MENU;
  cmd[1] = 0;
  cmd[2] = 0;
  SendToDevice(lpd, cmd, 3);
  app->Flags &= ~(APP_FLG_IS_FLASHING);
  lpd->Flags &= ~(LPD_FLG_SESSION | LPD_FLG_USER1 | LPD_FLG_USER2 | LPD_FLG_MIXER | LPD_FLG_MENU_LIGHT | LPD_FLG_DRAW_MODE);
  lpd->Mixer.Flags &= ~(LPD_MIXER_2ND_PUSH | LPD_MIXER_VOL);
}

/* Active le mode clignotement */
void               EnableFlash(t_lpd *lpd)
{
  unsigned char    cmd[3];

  app->Flags |= APP_FLG_IS_FLASHING;
  cmd[0] = LP_MENU;
  cmd[1] = 0;
  cmd[2] = LP_START_FLASH;
  SendToDevice(lpd, cmd, 3);
}

/* Désactive le mode clignotement */
void               DisableFlash(t_lpd *lpd)
{
  unsigned char    cmd[3];

  app->Flags &= ~(APP_FLG_IS_FLASHING);
  cmd[0] = LP_MENU;
  cmd[1] = 0;
  cmd[2] = LP_STOP_FLASH;
  SendToDevice(lpd, cmd, 3);
}
