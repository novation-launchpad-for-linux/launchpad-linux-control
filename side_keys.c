/*
 *  'launchpadctrl' allows you to use your Novation launchpad (NVLPD01)
 *  Copyright (C) 2014-2017  Vincent Deca. known as Bigcake@ubuntu-fr.org
 *  (user@forum not email)
 *
 *  vincent.deca@neutralite.org
 *
 *  This file is part of 'launchpadctrl'.
 *
 * 'launchpadctrl' is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  'launchpadctrl' is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with 'launchpadctrl'.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app.h"

/* Mets au niveau de volume correspondant à la touche de coté appuyée */
static void             SameLevelVolume(t_key *key)
{
  unsigned char         a, col;
  t_key                 *grid, *tmp;

  grid = key->lpd->Grid[GRID_VOLUME];
  /* Est-ce que tous les volume sont au même niveau ? */
  col = grid[(key->Id / 16) * 9].DefColor;
  for (a = 1; a < 8; ++a)
    if (grid[(key->Id / 16) * 9 + a].DefColor != col)
      break;
  if (a == 8) { /* oui */
    if (col == STAT_OFF)
      for (a = 0; a < 8; ++a)
	key->lpd->Mixer.Vol[a] = 1.0;
    return;
  }
  /* non, préparation de la mise à niveau */
  if (key->Id == LP_LEFT_ARM)
    col = STAT_MUTE_ON;
  else
    col = STAT_VOL_MIN;
  for (a = 0; a < 8; ++a) {
    tmp = &grid[(key->Id / 16) * 9 + a];
    LOCK_KEY(tmp);
    tmp->DefColor = col;
    UNLOCK_KEY(tmp);
  }
}

/* Gère l'annulation de l'option lors de l'appui sur la touche mixer */
void                   CancelSecondPush(t_lpd *lpd)
{
  unsigned char        a, flg, cmd[6];
  t_key                *grid, *key, *tmp;

  grid = lpd->Grid[GRID_VOLUME];
  lpd->Mixer.Flags &= ~(LPD_MIXER_2ND_PUSH);
  //if (app->Mixer.Flags & APP_MIXER_VOL)
    key = &grid[SIDE_VOLUME];
  //else
  cmd[0] = cmd[3] = LP_GRID;
  for (flg = 0, a = SIDE_VOLUME; a < 72; a += 9) {
    tmp = &grid[a];
    LOCK_KEY(tmp);
    if (!flg) {
      cmd[1] = tmp->Id;
      if (tmp == key)
	cmd[2] = tmp->DefColor = STAT_VOL_MIN;
      else
	cmd[2] = tmp->DefColor = STAT_OFF;
      flg = 1;
    }
    else if (flg == 1) {
      cmd[4] = tmp->Id;
      if (tmp == key)
	cmd[5] = tmp->DefColor = STAT_VOL_MIN;
      else
	cmd[5] = tmp->DefColor = STAT_OFF;
      flg = 2;
    }
    tmp->ActualColor = tmp->DefColor;
    if (flg == 2) {
      SendToDevice(lpd, cmd, 6);
      flg = 0;
    }
    UNLOCK_KEY(tmp);
  }
  cmd[0] = LP_MENU;
  cmd[1] = LP_TOP_MIXER;
  cmd[2] = STAT_ACTIVE_MENU;
  SendToDevice(lpd, cmd, 3);
}

 /* Activation de toutes les touches d'une line */
static void               ActivateAllLineKeys(t_key *key)
{
  unsigned char           a, cmd[3], nb;
  t_lpd                   *lpd = key->lpd;

  nb = (key->Id / 16) * 9;
  for (a = 0; a < 8; ++a) {
    key = &lpd->Grid[lpd->CurrGrid][nb + a];
    LOCK_KEY(key);
    if (!(key->Flags & KEY_FLG_HOLD_IT) && !(key->Flags & KEY_FLG_LINE_STOP)) {
      PushedKey(key);
      cmd[2] = ReleaseKey(key);
      if ((lpd->Fd > 1) && (key->ActualColor != key->DefColor)) {
	cmd[0] = LP_GRID;
	cmd[1] = key->Id;
	key->ActualColor = cmd[2];
	SendToDevice(lpd, cmd, 3);
      }
    }
    UNLOCK_KEY(key);
  }
}

/* Touché sur le coté droit de la grille appuyée */
unsigned char             PushedSideKeys(t_key *key)
{
  t_lpd                   *lpd = key->lpd;
  
  if (lpd->Flags & LPD_FLG_DRAW_MODE) {
    if (key->Id != LP_LEFT_ARM) {
      lpd->TempColor = key->DefColor;
      lpd->LightGrid[SIDE_ARM].DefColor = key->DefColor;
      if (lpd->LightGrid[SIDE_ARM].DefColor != lpd->LightGrid[SIDE_ARM].ActualColor)
	UpdateKey(&lpd->LightGrid[SIDE_ARM]);
    }
    return (key->DefColor);
  }
  else if (lpd->Flags & LPD_FLG_MIXER) {
    if (lpd->Mixer.Flags & LPD_MIXER_VOL) {
      if (!(lpd->Mixer.Flags & LPD_MIXER_2ND_PUSH)) {
	if (key->Id == LP_LEFT_VOL)
	  ActiveVolumeOption(lpd);
	else
	  return (STAT_PUSH_ERROR);
      }
      else {
	SameLevelVolume(key);
	ActivateAllLineKeys(key);
      }
    }
  }
  else
    ActivateAllLineKeys(key);
  return (STAT_PUSH_SUCCESS);
}

/* Touché sur le coté droit de la grille relachée */
unsigned char             ReleaseSideKeys(t_key *key)
{
  //# later
  return (key->DefColor);
}
