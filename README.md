# Novation Launchpad Linux Control

## Preamble

This project is a fork of Novation Launchpad Linux control software found at https://sourceforge.net/projects/controlfornovat/ and initially developed by Vincent Deca known as Bigcake@ubuntu-fr.org.

Useful links:
* Original forums posts [FR]: https://forum.ubuntu-fr.org/viewtopic.php?id=1043581
* Original blog posts [EN]: https://videca.wordpress.com/2012/09/24/endriver-for-novation-launchpad/
* Original blog posts [FR]: https://videca.wordpress.com/2012/09/11/fr-nlp-driver/
* Additionnal post about this software [EN]: https://videca.wordpress.com/2012/12/16/en-how-to-use-launchpadctrl/

## Instructions

1. Français
2. English


### 1. Français
#### Installation


**ATTENTION : l'installeur est prévu pour des distributions basées sur debian.**

- Exécutez la commande 'make install', choisissez la langue.

#### Désinstallation

Pour le désinstaller, si vous avez fait 'make install' : 
- Exécutez la commande 'make remove' en root ou avec sudo

#### Bugs connus

- Aucune lumière ne s'allume avec un kernel low-latency ou temps réel.

- Lors d'un copier/coller, si un caractère est multi-octets et qu'il est coupé par la fin de buffer de lecture de la saisie clavier, ce caractère devrait être mal affiché et la position du curseur devrait être faussée. Pour que l'affichage se réinitialise : ctrl + l

- gstreamer peut afficher des message d'erreur du genre :
   - Cannot connect to server socket
   - jack server is not running or cannot be started
   - -> une option a été rajoutée pour désactiver ces messages : 'Q' ou '--quiet'


### 2. English

#### Install

**WARNING:** The installer is made to work with debian based distributions.

- Execute the command 'make install' and choose your language


#### Uninstall

To uninstall, if you did 'make install': 
- Execute the command 'make remove' while root or with sudo

#### Known bugs
- Lights aren't working with a low-latency or real-time kernel

- While copy/paste, if a character is multi-bytes and is cut by the end of the input read buffer, this character will be badly print and the cursor's position will be wrong. To reinitialize the line print: ctrl + l

- gstreamer can prints errors like:
   - Cannot connect to server socket
   - jack server is not running or cannot be started
   - -> an option has been added to disable those errors : 'Q' or '--quiet'
