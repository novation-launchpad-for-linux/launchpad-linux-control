/*
 *  'launchpadctrl' allows you to use your Novation launchpad (NVLPD01)
 *  Copyright (C) 2014-2017  Vincent Deca. known as Bigcake@ubuntu-fr.org
 *  (user@forum not email)
 *
 *  vincent.deca@neutralite.org
 *
 *  This file is part of 'launchpadctrl'.
 *
 * 'launchpadctrl' is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  'launchpadctrl' is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with 'launchpadctrl'.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app.h"

/* Mise à jour des lumières d'une colonne pour le volume */
void               UpdateVolumeColumn(t_lpd *lpd, unsigned char nb, t_key *ign)
{
  unsigned char    a, flg, cmd[6];
  t_key            *grid, *key;

  /* Mise à jour de la couleur par défaut pour les boutons de volume */
  grid = lpd->Grid[GRID_VOLUME];
  for (a = 0; a < 7; ++a)
    if (lpd->Mixer.Vol[nb] > (0.1428 * (a + 1)) - 0.001)
      grid[54 - (a * 9) + nb].DefColor = STAT_VOL_MAX;
    else if (lpd->Mixer.Vol[nb] > (0.1428 * (a + 1)) - 0.0724)
      grid[54 - (a * 9) + nb].DefColor = STAT_VOL_MIN;
    else
      grid[54 - (a * 9) + nb].DefColor = STAT_OFF;
  /* Mise à jour de la couleur par défaut pour les boutons de mute */
  if (lpd->Mixer.Vol[nb] > 0.00001)
    grid[(a * 9) + nb].DefColor = STAT_MUTE_OFF;
  else
    grid[(a * 9) + nb].DefColor = STAT_MUTE_ON;
  /* Envoi des différences au launchpad */
  if (lpd->Fd > 1 && lpd->CurrGrid == GRID_VOLUME) {
    cmd[0] = cmd[3] = LP_GRID;
    for (flg = 0, a = nb; a < 72; a += 9) {
      key = &grid[a];
      if (ign != key) {
	LOCK_KEY(key);
	if (key->ActualColor != key->DefColor) {
	  if (!flg) {
	    cmd[1] = key->Id;
	    cmd[2] = key->DefColor;
	    flg = 1;
	  }
	  else if (flg == 1) {
	    cmd[4] = key->Id;
	    cmd[5] = key->DefColor;
	    flg = 2;
	  }
	  key->ActualColor = key->DefColor;
	  if (flg == 2) {
	    SendToDevice(lpd, cmd, 6);
	    flg = 0;
	  }
	}
	UNLOCK_KEY(key);
      }
    }
    if (flg == 1)
      SendToDevice(lpd, cmd, 3);
  }
}

/* Changement du volume */
void               ChangeVolume(t_lpd *lpd, const unsigned char column, float vol)
{
  unsigned char    a, b, c;
  t_key            *key;

  /* Normalisation du volume */
  vol = (vol / 0.0714) * 0.0714; 
  if (lpd->Mixer.Vol[column] == vol)
    return;
  /* Gestion du mute */
  if (vol < 0.001)
    vol = 0.0;
  else
    lpd->Mixer.Mute[column] = vol;
  /* Modification du volume des son en cours de lecture */
  for (c = 0; c < GRID_VOLUME; ++c)
    for (a = column; a < SIDE_ARM; a += 9) { /* Parcour de chaque touche verticale */
      key = &lpd->Grid[c][a];
      if (key->Audio)
	for (b = 0; b < KEY_NB_SIMULTANEOUS; ++b)
	  MODIFY_VOL_SOUND(key->Audio, b, vol);
    }
  lpd->Mixer.Vol[column] = vol;
}

/* Change le volume à partir d'une touche de volume et mets a jour les couleurs */
void               ChangeVolumeFromKey(t_key *key)
{
  unsigned char    column;
  float          vol;

  column = key->Id % 16;
  /* Changement du volume et couleur de la touche appuyée */
  if ((key->Id / 16) < 7) {
    vol = (7 - ((key->Id / 16) + 1)) * 0.1428 + 0.0714;
    if (key->DefColor ==  STAT_VOL_MIN || key->lpd->Mixer.Vol[column] > vol + 0.0715)
      vol += 0.0714;
  }
  else if (key->DefColor == STAT_MUTE_OFF)
    vol = 0.0;
  else
    vol = key->lpd->Mixer.Mute[column];
  ChangeVolume(key->lpd, column, vol);
  UpdateVolumeColumn(key->lpd, column, key);
}

/* Gestion de la touche volume en mode mixer */
void                ActiveVolumeOption(t_lpd *lpd)
{
  t_key             *grid;

  grid = lpd->Grid[GRID_VOLUME];
  lpd->Mixer.Flags |= LPD_MIXER_2ND_PUSH;
  grid[SIDE_VOLUME].DefColor = STAT_OPTION_AVAILABLE;
  grid[SIDE_PAN].DefColor = STAT_OPTION_AVAILABLE;
  grid[SIDE_SNDA].DefColor = STAT_OPTION_AVAILABLE;
  UpdateTwoKey(lpd, &grid[SIDE_PAN], &grid[SIDE_SNDA]);
  grid[SIDE_SNDB].DefColor = STAT_OPTION_AVAILABLE;
  grid[SIDE_STOP].DefColor = STAT_OPTION_AVAILABLE;
  UpdateTwoKey(lpd, &grid[SIDE_SNDB], &grid[SIDE_STOP]);
  grid[SIDE_TRKON].DefColor = STAT_OPTION_AVAILABLE;
  grid[SIDE_SOLO].DefColor = STAT_OPTION_AVAILABLE;
  UpdateTwoKey(lpd, &grid[SIDE_TRKON], &grid[SIDE_SOLO]);
  grid[SIDE_ARM].DefColor = STAT_OPTION_AVAILABLE;
  UpdateKey(&grid[SIDE_ARM]);
  UpdateMenu(lpd, LP_TOP_MIXER, STAT_OPTION_AVAILABLE);
}

/* Initialise la partie volume du mixer */
unsigned char      InitMixerVolume(t_lpd *lpd)
{
  t_key            *grid, *key;
  unsigned char    a;

  if ((grid = calloc(72, sizeof(t_key))) == NULL)
    return (1);
  lpd->Grid[GRID_VOLUME] = grid;
  for (a = 0; a < 63; ++a) {
    key = &grid[a];
    if ((a / 9) == 7)
      key->PushedColor = STAT_MUTE_OFF;
    else
      key->PushedColor = STAT_ACTIVE_MENU;
    key->Flags = KEY_FLG_NOT_INIT;
    key->Id = (a / 9) * 16 + (a % 9);
    key->lpd = lpd;
    pthread_mutex_init(&key->Lock, NULL);
  }
  for (a = 0; a < 8; ++a) {
    lpd->Mixer.Vol[a] = 0.4998;
    key = &grid[63 + a];
    key->PushedColor = STAT_MUTE_ON;
    key->Id = ((63 + a) / 9) * 16 + ((63 + a) % 9);
    key->lpd = lpd;
    UpdateVolumeColumn(lpd, a, 0);
  }
  key = &grid[SIDE_ARM];
  key->PushedColor = STAT_ACTIVE_MENU;
  key->Id = LP_LEFT_ARM;
  key->lpd = lpd;
  grid[SIDE_VOLUME].DefColor = STAT_IDLE_MENU;
  memcpy(lpd->Mixer.Mute, lpd->Mixer.Vol, sizeof(lpd->Mixer.Vol));
  return (0);
}
