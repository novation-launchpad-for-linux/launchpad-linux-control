/*
 *  'launchpadctrl' allows you to use your Novation launchpad (NVLPD01)
 *  Copyright (C) 2014-2017  Vincent Deca. known as Bigcake@ubuntu-fr.org
 *  (user@forum not email)
 *
 *  vincent.deca@neutralite.org
 *
 *  This file is part of 'launchpadctrl'.
 *
 * 'launchpadctrl' is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  'launchpadctrl' is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with 'launchpadctrl'.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "app.h"

/* Affiche toute l'aide */
static void            PrintAll()
{
  printf("%s :\n", app->Strs[31]);
  printf("- add %s\n", app->Strs[32]);
  printf("- close : %s\n", app->Strs[71]);
  printf("- connect %s\n", app->Strs[109]);
  printf("- copy %s\n", app->Strs[177]);
  printf("- del %s\n", app->Strs[39]);
  printf("- disconnect %s\n", app->Strs[113]);
  printf("- exit : %s\n", app->Strs[40]);
  printf("- grid %s\n", app->Strs[44]);
  printf("- help, ? %s\n", app->Strs[41]);
  printf("- link %s\n", app->Strs[182]);
  printf("- list %s\n", app->Strs[64]);
  printf("- load %s\n", app->Strs[42]);
  printf("- mode %s\n", app->Strs[172]);
  printf("- move %s\n", app->Strs[132]);
  printf("- mute %s\n", app->Strs[70]);
  printf("- open %s\n", app->Strs[43]);
  printf("- pad %s\n", app->Strs[142]);
  printf("- rlin %s\n", app->Strs[75]);
  printf("- run %s\n", app->Strs[63]);
  printf("- save %s\n", app->Strs[67]);
  printf("- set %s\n", app->Strs[153]);
  printf("- sleep %s\n", app->Strs[80]);
  printf("- slin %s\n", app->Strs[77]);
  printf("- stop %s\n", app->Strs[59]);
  printf("- umute %s\n", app->Strs[76]);
  printf("- vol %s\n", app->Strs[14]);
}

static void     HelpAdd(char *param)
{
  int           nb = 0;

  if (param)
    nb = atoi(param);
  printf("- add %s\n", app->Strs[32]);
  if (nb < 1) /* X Y */
    printf("\t%s\n\n\t%s\n\t%s\n", app->Strs[131], app->Strs[34], app->Strs[35]);
  if (nb < 2) /* tempo */
    printf("\t%s\n", app->Strs[89]);
  if (nb < 3) /* fonctionnement  */
    printf("\t%s\n\t%s\n", app->Strs[36], app->Strs[128]);
  if (nb < 4) /* dépendance */ 
    printf("\t%s\n\t%s\n", app->Strs[78], app->Strs[130]);
  if (nb < 5) /* type */ 
    printf("\t%s\n\t%s\n", app->Strs[45], app->Strs[168]);
  if (nb < 6) /* fichier */
    printf("\t%s\n\t%s\n", app->Strs[38], app->Strs[129]);
}

static void     HelpClose(char *param)
{
  printf("- close : %s\n", app->Strs[71]);
}

static void     HelpCopy(char *param)
{
  printf("- copy %s\n", app->Strs[177]);
}

static void     HelpConnect(char *param)
{
  int           nb = 0;

  if (param)
    nb = atoi(param);
  switch (nb) {
  case 0: printf("- connect %s\n\t%s (0 < id < %d)\n\t%s\n\t%s\n", app->Strs[109]
		 , app->Strs[110], APP_LIMIT_USHORT, app->Strs[126], app->Strs[127]);
    break;
  case 1: printf("- connect id <%s>  <%s>  [%s]\n\t%s\n\t%s\n", app->Strs[157]
		 , app->Strs[159], app->Strs[160], app->Strs[126], app->Strs[127]);
    break;
  case 2: printf("- connect id %s %s [%s]\n\t%s\n", app->Strs[157]
		 , app->Strs[159], app->Strs[160], app->Strs[127]);
    break;
  }
}

static void     HelpDel(char *param)
{
  printf("- del %s\n\t%s\n\t%s\n\tall: %s\n", app->Strs[39], app->Strs[34], app->Strs[35], app->Strs[161]);
}

static void     HelpDisconnect(char *param)
{
  printf("- disconnect %s\n\t%s\n", app->Strs[113], app->Strs[110]);
}

static void     HelpExit(char *param)
{
  printf("- exit : %s\n", app->Strs[40]);
}

static void     HelpGrid(char *param)
{
  printf("- grid %s\n\t\t0 1 2\n\t\t3 4 5\n\t\t6 7 8\n", app->Strs[44]);
}

static void     HelpHelp(char *param)
{
  printf("- help, ? %s\n\t%s : %s\n", app->Strs[41], app->Strs[114], app->Strs[115]);
}

static void     HelpLink(char *param)
{
  int           nb = 0;

  if (param)
    nb = atoi(param);
  switch (nb) {
  case 0 : printf("- link %s\n\t%s\n\t%s\n", app->Strs[182], app->Strs[183], app->Strs[184]);
    break;
  case 1 : printf("- link XY XY <%s>\n\t%s\n", app->Strs[185], app->Strs[186]);
    break;
  }
}

static void     HelpList(char *param)
{
  int           nb = 0;

  if (param)
    nb = atoi(param);
  switch (nb) {
  case 0 : printf("- list %s\n\t[key|server] : %s\n\t%s\n", app->Strs[64], app->Strs[108], app->Strs[125]);
    break;
  case 1 : printf("- list %s\n\t%s\n", app->Strs[64], app->Strs[125]);
    break;
  }
}

static void     HelpLoad(char *param)
{
  printf("- load %s\n\t%s\n", app->Strs[42], app->Strs[122]);
}

static void     HelpMode(char *param)
{
  printf("- mode %s\n\t%s\n", app->Strs[172], app->Strs[173]);
}

static void     HelpMove(char *param)
{
  printf("- move %s\n\t%s\n\t%s\n\t%s\n\t%s : move 35 48\n", app->Strs[132]
	 , app->Strs[134], app->Strs[34], app->Strs[35], app->Strs[133]);
}

static void     HelpMute(char *param)
{
  printf("- mute %s\n\t%s\n", app->Strs[70], app->Strs[116]);
}

static void     HelpOpen(char *param)
{
  printf("- open %s\n\t%s\n", app->Strs[43], app->Strs[124]);
}

static void     HelpPad(char *param)
{
  printf("- pad %s\n\t%s\n", app->Strs[142], app->Strs[143]);
}

static void     HelpRun(char *param)
{
  printf("- run %s\n\t%s\n\t%s\n\t%s\n\t%s\n", app->Strs[63], app->Strs[34]
	 , app->Strs[35], app->Strs[72], app->Strs[73]);
}

static void     HelpRunLine(char *param)
{
  printf("- rlin %s\n\t%s\n", app->Strs[75], app->Strs[123]);
}

static void     HelpSave(char *param)
{
  printf("- save %s\n\t%s\n", app->Strs[67], app->Strs[122]);
}

static void     HelpSet(char *param)
{
  int           nb = 0;

  if (param)
    nb = atoi(param);
  switch (nb) {
  case 0: printf("- set %s\n\t%s\n", app->Strs[153], app->Strs[154]); break;
  case 1: printf("- set XY %s\n  %s\n\t%s\n"
		 , app->Strs[163], app->Strs[164], app->Strs[165]); break;
  case 2: printf("- set XY work <%s>\n\t%s\n", app->Strs[167], app->Strs[128]); break;
  case 3: printf("- set XY dep <%s>\n\t%s\n", app->Strs[167], app->Strs[130]); break;
  case 4: printf("- set XY type <%s>\n\t%s\n", app->Strs[167], app->Strs[168]); break;
  case 5: printf("- set XY file <%s>\n\t%s\n\t%s\n\t%s\n"
		 , app->Strs[167], app->Strs[169], app->Strs[38], app->Strs[129]); break;
  case 6: printf("- set XY tempo <0|1>\n\t%s\n", app->Strs[180]);
  }
}

static void     HelpSleep(char *param)
{
  printf("- sleep %s\n\t%s\n\t%s\n", app->Strs[80], app->Strs[120], app->Strs[121]);
}

static void     HelpStopLine(char *param)
{
  printf("- slin %s\n\t%s\n", app->Strs[77], app->Strs[123]);
}

static void     HelpStop(char *param)
{
  printf("- stop %s\n\t%s\n\t%s\n\t%s\n\t%s\n", app->Strs[59], app->Strs[34]
	 , app->Strs[35], app->Strs[72], app->Strs[119]);
}

static void     HelpUmute(char *param)
{
  printf("- umute %s\n\t%s\n", app->Strs[76], app->Strs[116]);
}

static void     HelpVol(char *param)
{
  int           nb = 0;

  if (param)
    nb = atoi(param);
  switch (nb) {
  case 0: printf("- vol %s\n\t%s\n\t%s\n", app->Strs[14], app->Strs[116], app->Strs[117]); 
    break;
  case 1: printf("- vol X <%s>\n\t%s\n", app->Strs[156], app->Strs[117]); 
    break;
  }
}

/* Tableau de commande */
static t_scd    HelpTab[] = {
  {"?", HelpHelp, 1}
  , {"add", HelpAdd, 3}
  , {"close", HelpClose, 5}
  , {"connect", HelpConnect, 7}
  , {"copy", HelpCopy, 4}
  , {"del", HelpDel, 3}
  , {"disconnect", HelpDisconnect, 10}
  , {"exit", HelpExit, 4}
  , {"grid", HelpGrid, 4}
  , {"help", HelpHelp, 4}
  , {"link", HelpLink, 4}
  , {"list", HelpList, 4}
  , {"load", HelpLoad, 4}
  , {"mode", HelpMode, 4}
  , {"move", HelpMove, 4}
  , {"mute", HelpMute, 4}
  , {"pad", HelpPad, 3}
  , {"open", HelpOpen, 4}
  , {"rlin", HelpRunLine, 4}
  , {"run", HelpRun, 3}
  , {"r", HelpRun, 1}
  , {"save", HelpSave, 4}
  , {"set", HelpSet, 3}
  , {"sleep", HelpSleep, 5}
  , {"slin", HelpStopLine, 4}
  , {"stop", HelpStop, 4}
  , {"s", HelpStop, 1}
  , {"umute", HelpUmute, 5}
  , {"vol", HelpVol, 3}
};

#define SIZE_TAB_CMD (sizeof(HelpTab) / sizeof(t_scd))

/* Commande 'help' ou '?' */
void                   CmdHelp(char *param)
{
  unsigned char        a;

  LOCK_APP(LockOutput);
  if (param) {
    for (a = 0; a < SIZE_TAB_CMD; ++a)
      if (!strncasecmp(HelpTab[a].Cmd, param, HelpTab[a].Len)) {
	for (; *param && (*param != ' ') && (*param != '\t'); ++param);
	for (; *param && (*param == ' ' || *param == '\t'); ++param);
	HelpTab[a].Func(param);
	UNLOCK_APP(LockOutput);
	return;
      }
  }
  PrintAll();
  UNLOCK_APP(LockOutput);
}
