/*
 *  'launchpadctrl' allows you to use your Novation launchpad (NVLPD01)
 *  Copyright (C) 2014-2017  Vincent Deca. known as Bigcake@ubuntu-fr.org
 *  (user@forum not email)
 *
 *  vincent.deca@neutralite.org
 *
 *  This file is part of 'launchpadctrl'.
 *
 * 'launchpadctrl' is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  'launchpadctrl' is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with 'launchpadctrl'.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app.h"

/* LOCK KEY - Gestion de la couleur de la touche après la fin */
void              EndRunSwitchLight(t_key *key, unsigned char color, unsigned char grid)
{
  unsigned char   cmd[3];

  if (!(key->Flags & KEY_FLG_PUSHED) || ((key->Flags & KEY_FLG_PUSHED) && (grid != key->lpd->CurrGrid))) {
    cmd[0] = LP_GRID;
    cmd[1] = key->Id;
    if (key->Flags & KEY_FLG_RUN_ERROR) {
      key->Flags &= ~(KEY_FLG_RUN_ERROR);
      cmd[2] = key->DefColor = STAT_IDLE_ERROR;
      PrintError(20, ERR_TYPE_KEY, key);
      key->PushedColor = STAT_PUSH_ERROR;
    }
    else {
      if (key->CmdFile)
	cmd[2] = key->DefColor = color;
      else
	cmd[2] = key->DefColor = STAT_OFF;
      key->PushedColor = STAT_PUSH_SUCCESS;
    }
    key->ActualColor = key->DefColor;
    if ((grid == key->lpd->CurrGrid) && (key->lpd->Fd > 1))
      SendToDevice(key->lpd, cmd, 3);
  }
  key->Flags &= ~(KEY_FLG_IN_USE);
}

/* Envoi des données au serveur */
void              *ThreadSendData(void *param)
{
  unsigned short  a;
  t_key           *key = param;
  t_lpd           *lpd = key->lpd;
  unsigned char   grid;

  if (lpd->CurrGrid < GRID_VOLUME)
    grid = lpd->CurrGrid;
  else
    grid = lpd->LastGrid;
  /* Attente du tempo */
  if (key->Tempo != KEY_TMO_INSTANT) {
    LOCK_APP(LockCond);
    pthread_cond_wait(&app->WaitTempo, &app->LockCond);
    UNLOCK_APP(LockCond);
  }
  /* Un serveur a t'il déja été ajouté ?*/
  if (!app->Servers) {
    PrintError(98, ERR_TYPE_KEY, key);
    key->Flags |= KEY_FLG_RUN_ERROR;
    goto end_ThreadSendData;
  }
  /* Retrouve-t-on l'uid du serveur ? */
  for (a = 0; a < app->MaxServers; ++a)
    if (app->Servers[a] && (app->Servers[a]->Uid == key->Count))
      break;
  if (a == app->MaxServers) {
    PrintError(98, ERR_TYPE_KEY, key);
    key->Flags |= KEY_FLG_RUN_ERROR;
    goto end_ThreadSendData;
  }
  LOCK_MUTEX(app->Servers[a]->Lock);
  /* Est-on connecté ? */
  if (!app->Servers[a]->Socket) {
    UNLOCK_MUTEX(app->Servers[a]->Lock);
    PrintError(102, ERR_TYPE_SIMPLE, 0);
    key->Flags |= KEY_FLG_RUN_ERROR;
    goto end_ThreadSendData;
  }
  /* Envoi */
  if (send(app->Servers[a]->Socket, key->CmdFile, key->CmdLen, 0) < key->CmdLen) {
    PrintError(101, ERR_TYPE_ERRNO, 0);
    app->Servers[a]->Socket = 0;
    UNLOCK_MUTEX(app->Servers[a]->Lock);
    key->Flags |= KEY_FLG_RUN_ERROR;
    goto end_ThreadSendData;
  }
  UNLOCK_MUTEX(app->Servers[a]->Lock);
 end_ThreadSendData:
  if (key->Link && !(key->Flags & KEY_FLG_LNK_SIMUL))
    if (key->Flags & KEY_FLG_LNK_PUSHED) {
      t_key  *lnk = key->Link;
      lnk->Flags &= ~(KEY_FLG_LNK_PUSHED);
      key->Flags |= KEY_FLG_LOCK;
      PushedKey(lnk);
      lnk->DefColor = ReleaseKey(lnk);
      key->Flags &= ~(KEY_FLG_LOCK);
      UpdateKey(lnk);
    }
  EndRunSwitchLight(key, STAT_IDLE_CMD, grid);
  return (0);
}

/* LOCK KEY - Arrêt d'un processus en cours */
void                       KillRunningOne(t_key *key)
{
  unsigned char            a;

  if (key->Type == KEY_TYPE_SERVER)
    return;
  if (key->Flags & KEY_FLG_IN_USE)
    key->Flags |= KEY_FLG_INTERRUPTED;
  key->Flags &= ~(KEY_FLG_STOP_LOOP | KEY_FLG_IN_USE);
  for (a = 0; a < KEY_NB_SIMULTANEOUS; ++a) {
    if (key->Pid[a]) {
      kill(key->Pid[a], SIGKILL);
      key->Pid[a] = 0;
    }
    else if (key->Audio) {
      if (IN_USE_SOUND(key->Audio, a)) {
	key->Flags &= ~(KEY_FLG_IS_PAUSED);
	g_main_loop_quit(key->Audio->Loop);
      }
    }
  }
  if (key->Type == KEY_TYPE_AUDIO)
    key->DefColor = STAT_IDLE_AUDIO;
  else if ((key->Type == KEY_TYPE_EXEC) || (key->Type == KEY_TYPE_SERVER))
    key->DefColor = STAT_IDLE_CMD;
  else
    key->DefColor = STAT_OFF;
  if ((key->DefColor != key->ActualColor) && (key->lpd->CurrGrid < GRID_VOLUME))
    UpdateKey(key);
}

/* Recherche de dépendances */
static void               SearchAndStopChild(t_key *key)
{
  unsigned char           start, a;
  t_key                   *grid, *tmp;

  if (key->lpd->CurrGrid > GRID_USER2)
    grid = key->lpd->Grid[key->lpd->LastGrid];
  else
    grid = key->lpd->Grid[key->lpd->CurrGrid];
  /* Recherche en ligne */
  start = (key->Id / 16) * 9;
  for (a = 0; a < 8; ++a) {
    tmp = &grid[start + a];
    if ((tmp->Id != key->Id) && !(tmp->Flags & KEY_FLG_LOCK)) {
      LOCK_KEY(tmp);
      if ((tmp->Flags & KEY_FLG_LINE_STOP) && (tmp->Flags & KEY_FLG_IN_USE))
	KillRunningOne(tmp);
      UNLOCK_KEY(tmp);
    }
  }
  /* Recherche en colonne */
  for (a = key->Id % 16; a < 72; a += 9) {
    tmp = &grid[a];
    if ((tmp->Id != key->Id) && !(tmp->Flags & KEY_FLG_LOCK)) {
      LOCK_KEY(tmp);
      if ((tmp->Flags & KEY_FLG_COL_STOP)  && (tmp->Flags & KEY_FLG_IN_USE) )
	KillRunningOne(tmp);
      UNLOCK_KEY(tmp);
    }
  }
}

/* Touche appuyée en mode mixer */
static unsigned char      PushedMixerKeys(t_key *key)
{
  ChangeVolumeFromKey(key);
  return (key->DefColor);
}

/* La touche appuyée concerne des donnees a envoyer a un serveur */
static unsigned char      PushedServerKey(t_key *key)
{
  unsigned short          a;

  if (!(key->Flags & KEY_FLG_IN_USE)) {
    if (key->Tempo) {
      pthread_t id;
      
      key->Flags |= KEY_FLG_IN_USE;
      if (pthread_create(&id, &app->Attr, ThreadSendData, key)) {
	PrintError(7, ERR_TYPE_ERRNO, 0);
	key->Flags &= ~(KEY_FLG_IN_USE);
	return (STAT_PUSH_ERROR);
      }
      else
	pthread_detach(id);
    }
    else {
      /* Un serveur a t'il déja été ajouté ?*/
      if (!app->Servers) {
	PrintError(98, ERR_TYPE_KEY, key);
	key->Flags |= KEY_FLG_RUN_ERROR;
	return (STAT_PUSH_ERROR);
      }
      /* Retrouve-t-on l'uid du serveur ? */
      for (a = 0; a < app->MaxServers; ++a)
	if (app->Servers[a] && (app->Servers[a]->Uid == key->Count))
	  break;
      if (a == app->MaxServers) {
	PrintError(98, ERR_TYPE_KEY, key);
	key->Flags |= KEY_FLG_RUN_ERROR;
	return (STAT_PUSH_ERROR);
      }
      LOCK_MUTEX(app->Servers[a]->Lock);
      /* Est-on connecté ? */
      if (!app->Servers[a]->Socket) {
	UNLOCK_MUTEX(app->Servers[a]->Lock);
	PrintError(102, ERR_TYPE_SIMPLE, 0);
	key->Flags |= KEY_FLG_RUN_ERROR;
	return (STAT_PUSH_ERROR);
      }
      /* Envoi */
      if (send(app->Servers[a]->Socket, key->CmdFile, key->CmdLen, 0) < key->CmdLen) {
	PrintError(101, ERR_TYPE_ERRNO, 0);
	app->Servers[a]->Socket = 0;
	UNLOCK_MUTEX(app->Servers[a]->Lock);
	key->Flags |= KEY_FLG_RUN_ERROR;
	return (STAT_PUSH_ERROR);
      }
      UNLOCK_MUTEX(app->Servers[a]->Lock);
    }
  }
  return (STAT_PUSH_SUCCESS);
}

/* La touche appuyée concerne un fichier audio */
static unsigned char      PushedAudioKey(t_key *key)
{
  unsigned char           cmd;
  pthread_t               id;

  if (!(key->Flags & KEY_FLG_STOP_LOOP)) {
    key->DefColor = STAT_PUSH_SUCCESS;
    if (!key->Audio)
      cmd = STAT_PUSH_ERROR;
    else if (key->Flags & KEY_FLG_IS_PAUSED) {
      key->Flags &= ~(KEY_FLG_IS_PAUSED);
      cmd = STAT_PUSH_SUCCESS;
      PLAY_SOUND(key->Audio, 0);
    }
    else {
      /*
      for (cmd = 0; cmd < KEY_NB_SIMULTANEOUS; ++cmd)
	if (!IN_USE_SOUND(key->Audio, cmd))
	  break;
      if (cmd == KEY_NB_SIMULTANEOUS) {
	PrintError(53, ERR_TYPE_KEY, key);
	cmd = STAT_PUSH_ERROR;
      }
      else {
      */
      cmd = key->PushedColor;
      if (key->Flags & KEY_FLG_IN_USE) {
	if (key->Flags & KEY_FLG_PAUSE) {
	  key->Flags |= KEY_FLG_IS_PAUSED;
	  key->DefColor = STAT_PAUSE_AUDIO;
	  PAUSE_SOUND(key->Audio, 0);
	  return (STAT_PUSH_SUCCESS);
	}
	if (key->Flags & KEY_FLG_SINGLE)
	  KillRunningOne(key);
	++key->Count;
      }
      else
	++key->Count;
      if (pthread_create(&id, &app->Attr, PlaySound, key)) {
	--key->Count;
	PrintError(7, ERR_TYPE_ERRNO, 0);
	cmd = STAT_PUSH_ERROR;
      }
      else {
	pthread_detach(id);
	key->Flags |= KEY_FLG_IN_USE;
      }
      //}
    }
  }
  else
    cmd = key->PushedColor;
  return (cmd);
}

/* Lancement de l'executable assigné a la touche */
void              *ThreadExecuteCmd(void *param)
{
  t_key           *key = param;
  t_lpd           *lpd = key->lpd;
  pid_t           pid = -1;
  unsigned char   grid;
  int             status, pos = 0;
  pthread_t       id;

  if (lpd->CurrGrid < GRID_VOLUME)
    grid = lpd->CurrGrid;
  else
    grid = lpd->LastGrid;
  /* Attente du tempo */
  if (key->Tempo != KEY_TMO_INSTANT) {
    LOCK_APP(LockCond);
    pthread_cond_wait(&app->WaitTempo, &app->LockCond);
    UNLOCK_APP(LockCond);
  }
  if (!(key->Flags & KEY_FLG_INTERRUPTED)) {
    /* Création du processus fils */
    //if ((pid = fork()) == -1) {
    if ((pid = vfork()) == -1) {
      PrintError(12, ERR_TYPE_ERRNO, 0);
      return (0);
    }
    else if (!pid) {
      struct stat   sb;
      /*
	if (!(tab = LineToTab(key->CmdFile)))
	ExitWithMsg(app->Strs[12]);
      */
      if (stat(key->CmdTab[0], &sb) != -1) {
	if (S_ISREG(sb.st_mode))
	  execv(key->CmdTab[0], key->CmdTab);
	else {
	  LOCK_APP(LockOutput);
	  printf("\r\033[31m%s\033[00m: '%s': %s \n", app->Strs[1], key->CmdTab[0], app->Strs[79]);
	  UNLOCK_APP(LockOutput);
	  if (!(app->Flags & APP_FLG_SHELL_CMD))
	    PrintLPEntry();
	}
      }
      else {
	LOCK_APP(LockOutput);
	printf("\r\033[31m%s\033[00m: '%s': %s \n", app->Strs[1], key->CmdTab[0], strerror(errno));
	UNLOCK_APP(LockOutput);
	if (!(app->Flags & APP_FLG_SHELL_CMD))
	  PrintLPEntry();
      }
      _exit(EXIT_FAILURE);
      //ExitWithMsg(app->Strs[12]);
    }
    /* Sauvegarde du PID */
    LOCK_KEY(key);
    for (pos = 0; pos < KEY_NB_SIMULTANEOUS; ++pos)
      if (!key->Pid[pos]) {
	key->Pid[pos] = pid;
	break;
      }
    UNLOCK_KEY(key);
    /* Attente du processus fils */
    waitpid(pid, &status, 0);
  }
  /* Mise a jour de la touche */
  LOCK_KEY(key);
  if (status == 256)
    key->Flags |= KEY_FLG_RUN_ERROR;
  if (key->Flags & KEY_FLG_STOP_LOOP)
    key->Flags &= ~(KEY_FLG_STOP_LOOP);
  else if ((key->Flags & KEY_FLG_LOOP) && !(key->Flags & KEY_FLG_INTERRUPTED) && !(key->Flags & KEY_FLG_RUN_ERROR)) {
    if (pthread_create(&id, &app->Attr, ThreadExecuteCmd, key))
      PrintError(7, ERR_TYPE_ERRNO, 0);
    else {
      pthread_detach(id);
      goto end_ThreadExecuteCmd;
    }
  }
  --key->Count;
  if (key->Flags & KEY_FLG_INTERRUPTED) {
    key->Flags &= ~(KEY_FLG_INTERRUPTED);
    goto end_ThreadExecuteCmd;
  }
  if (key->Link && !(key->Flags & KEY_FLG_LNK_SIMUL)) {
    t_key  *lnk = key->Link;
    if (key->Flags & KEY_FLG_LNK_PUSHED) {
      lnk->Flags &= ~(KEY_FLG_LNK_PUSHED);
      key->Flags |= KEY_FLG_LOCK;
      PushedKey(lnk);
      lnk->DefColor = ReleaseKey(lnk);
      key->Flags &= ~(KEY_FLG_LOCK);
      UpdateKey(lnk);
    }
    else if (key->Flags & KEY_FLG_LNK_LOOP) {
      key->Flags |= KEY_FLG_LOCK;
      PushedKey(lnk);
      lnk->DefColor = ReleaseKey(lnk);
      key->Flags &= ~(KEY_FLG_LOCK);
      UpdateKey(lnk);
    }
  }
  if (!key->Count)
    EndRunSwitchLight(key, STAT_IDLE_CMD, grid);
 end_ThreadExecuteCmd:
  if (pid != -1)
    key->Pid[pos] = 0;
  UNLOCK_KEY(key);
  return (0);
}

/* La touche appuyée concerne un executable */
static unsigned char      PushedExecKey(t_key *key)
{
  unsigned char           cmd;
  pthread_t               id;

  if (!(key->Flags & KEY_FLG_STOP_LOOP)) {
    key->DefColor = STAT_RUNNING_CMD;
    for (cmd = 0; cmd < KEY_NB_SIMULTANEOUS; ++cmd)
      if (!key->Pid[cmd])
	break;
    if (cmd == KEY_NB_SIMULTANEOUS) {
      PrintError(53, ERR_TYPE_KEY, key);
      cmd = STAT_PUSH_ERROR;  /* 3ème octet */
    }
    else {
      cmd = key->PushedColor;  /* 3ème octet */
      ++key->Count;
      if ((key->Flags & KEY_FLG_SINGLE) && (key->Flags & KEY_FLG_IN_USE))
	KillRunningOne(key);
      if (pthread_create(&id, &app->Attr, ThreadExecuteCmd, key)) {
	--key->Count;
	PrintError(7, ERR_TYPE_ERRNO, 0);
	cmd = STAT_PUSH_ERROR;  /* 3ème octet */
      }
      else {
	pthread_detach(id);
	key->Flags |= KEY_FLG_IN_USE;
      }
    }
  }
  else 
    cmd = key->PushedColor;  /* 3ème octet */
  return (cmd);
}

/* Touche appuyée */
unsigned char             PushedKey(t_key *key)
{
  unsigned char           cmd;
  
  key->Flags |= KEY_FLG_PUSHED;
  if (key->Flags & KEY_FLG_LOCK)
    return (key->DefColor);
  if ((key->Id % 16) == 8)
    return (PushedSideKeys(key));
  if (key->lpd->Flags & LPD_FLG_DRAW_MODE)
    return (key->PushedColor);
  if (key->lpd->Flags & LPD_FLG_MIXER)
    return (PushedMixerKeys(key));
  SearchAndStopChild(key);
  if (key->Flags & KEY_FLG_NOT_INIT)
    cmd = STAT_PUSH_NOT_INIT;
  else if ((key->Flags & KEY_FLG_KEEP) && (key->Flags & KEY_FLG_IN_USE))
    cmd = STAT_PUSH_ERROR;
  else {
    /* Activation ou désactivation de boucle */
    if ((key->Flags & KEY_FLG_IN_USE) && (key->Flags & KEY_FLG_LOOP)) {
      if (key->Flags & KEY_FLG_STOP_LOOP) {
	PrintError(61, ERR_TYPE_KEY, key);
	key->Flags &= ~(KEY_FLG_STOP_LOOP);
      }
      else {
	PrintError(62, ERR_TYPE_KEY, key);
	key->Flags |= KEY_FLG_STOP_LOOP;
      }
      return (key->PushedColor);
    }
    /* Gestion de la touche appuyée selon le type  */
    switch (key->Type) {
    case KEY_TYPE_AUDIO : cmd = PushedAudioKey(key); break;
    case KEY_TYPE_EXEC : cmd = PushedExecKey(key); break;
    case KEY_TYPE_SERVER : cmd = PushedServerKey(key); break;
    default: cmd = STAT_PUSH_NOT_INIT;
      ASSERT(1, "key->Type = inconnu");
    }
  }
  if (key->Link && (key->Flags & KEY_FLG_LNK_SIMUL) && (key->Flags & KEY_FLG_LNK_PUSHED)) {
    t_key  *lnk = key->Link;
    lnk->Flags &= ~(KEY_FLG_LNK_PUSHED);
    key->Flags |= KEY_FLG_LOCK;
    PushedKey(lnk);
    lnk->DefColor = ReleaseKey(lnk);
    key->Flags &= ~(KEY_FLG_LOCK);
    UpdateKey(lnk);
  }
  return (cmd);
}

/* Touche relachée */
unsigned char             ReleaseKey(t_key *key)
{
  unsigned char           cmd = STAT_OFF;

  key->Flags &= ~(KEY_FLG_PUSHED);
  if (key->Flags & KEY_FLG_LOCK)
    return (key->DefColor);
  if ((key->Id % 16) == 8)
    return (ReleaseSideKeys(key));
  if (key->lpd->Flags & LPD_FLG_DRAW_MODE)
    return (key->PushedColor);
  if (key->lpd->Flags & LPD_FLG_MIXER)
    return (key->DefColor);
  if (key->Flags & KEY_FLG_NOT_INIT)
    cmd = STAT_OFF;
  else if (key->CmdFile) {
    if (key->Flags & KEY_FLG_RUN_ERROR) {
      key->Flags &= ~(KEY_FLG_RUN_ERROR);
      cmd = STAT_IDLE_ERROR;  /* 3ème octet */
      PrintError(20, ERR_TYPE_KEY, key);
      key->PushedColor = STAT_PUSH_ERROR;
    }
    else if (key->Type == KEY_TYPE_AUDIO) {
      if (key->Flags & KEY_FLG_IN_USE) {
	if (key->Flags & KEY_FLG_HOLD_IT) {
	  KillRunningOne(key);
	  cmd = key->DefColor;  /* 3ème octet */
	}
	else if (key->Flags & KEY_FLG_IS_PAUSED)
	  cmd = key->DefColor;  /* 3ème octet */
	else
	  cmd = STAT_PUSH_SUCCESS;  /* 3ème octet */
      }
      else if (!key->Audio) {
	PrintError(37, ERR_TYPE_KEY, key);
	cmd = STAT_IDLE_ERROR;  /* 3ème octet */
      }
      else
	cmd = STAT_IDLE_AUDIO;  /* 3ème octet */
    }
    else { /* commande local ou serveur */
      if (key->Flags & KEY_FLG_IN_USE) {
	if (key->Flags & KEY_FLG_HOLD_IT) {
	  KillRunningOne(key);
	  cmd = STAT_IDLE_CMD;  /* 3ème octet */
	}
	else
	  cmd = STAT_RUNNING_CMD;  /* 3ème octet */
      }
      else
	cmd = STAT_IDLE_CMD;  /* 3ème octet */
    }
  }
  key->DefColor = cmd;
  return (cmd);
}

/* Gestion des options du driver */
static unsigned char     HandleDriverOptions(t_lpd *lpd, unsigned char *buff)
{
  unsigned char          nb = 0;
  int                    vers;
  
  switch (buff[nb]) {
  case LP_GET_STAT: lpd->Stat = buff[++nb]; break;
  case LP_GET_VERSION:
    vers = buff[++nb] * 100;
    vers += buff[++nb];
    if (vers < DRIVER_REQ_VERSION) {
      printf("\r!! %s !! \n%s : %d.%d\n%s : %d.%d\n%s\n"
	     , app->Strs[137], app->Strs[138], buff[nb - 1], buff[nb]
	     , app->Strs[139], DRIVER_REQ_VERSION / 100, DRIVER_REQ_VERSION % 100
	     , app->Strs[140]);
      PrintLPEntry();
    }
    break;
  case LP_IS_UNPLUG:
    LOCK_APP(LockOutput);
    printf("\r%s \n", app->Strs[118]);
    UNLOCK_APP(LockOutput);
    PrintLPEntry();
    CmdClose((void *)1);
    if (!(app->Flags & APP_FLG_NO_CHECK)) {
      LOCK_APP(LockPonctual);
      if (!lpd->IdCheck) {
	if (pthread_create(&lpd->IdCheck, &app->Attr, ThreadCheckNewLaunchpad, lpd))
	  PrintError(7, ERR_TYPE_ERRNO, 0);
	else
	  pthread_detach(lpd->IdCheck);
      }
      UNLOCK_APP(LockPonctual);
    }
    break;
  }
  return (nb + 1);
}

/* Mode normal, touche appuyée */
static void              HandlePush(t_lpd *lpd, unsigned char *cmd)
{
  t_key                  *key;

  if (cmd[0] == LP_MENU)
    cmd[2] = ManageMenu(lpd, cmd[1], 1);  /* 3ème octet */
  else {
    if (lpd->Flags & LPD_FLG_DRAW_MODE) {
      key = &lpd->LightGrid[(cmd[1] / 16) * 9 + (cmd[1] % 16)];
      if ((key->Id % 16) == 8)
	cmd[2] = PushedSideKeys(key);
      else {
	LOCK_KEY(key);
	cmd[2] = key->DefColor = lpd->TempColor;
	UNLOCK_KEY(key);
      }
    }
    else {
      key = &lpd->Grid[lpd->CurrGrid][(cmd[1] / 16) * 9 + (cmd[1] % 16)];
      if (key->Link) 
	key->Flags |= KEY_FLG_LNK_PUSHED;
      cmd[2] = PushedKey(key);
    }
    if (key->lpd->Flags & (LPD_FLG_USER1 | LPD_FLG_USER2)) {
      if (lpd->Flags & LPD_FLG_RANDOM_USER) {
	switch (rand() % 12) {
	case 0 : cmd[2] = LP_LOW_RED; break;
	case 1 : cmd[2] = LP_MED_RED; break;
	case 2 : cmd[2] = LP_FULL_RED; break;
	case 3 : cmd[2] = LP_LOW_GREEN; break;
	case 4 : cmd[2] = LP_MED_GREEN; break;
	case 5 : cmd[2] = LP_FULL_GREEN; break;
	case 6 : cmd[2] = LP_LOW_YELLOW; break;
	case 7 : cmd[2] = LP_MED_YELLOW; break;
	case 8 : cmd[2] = LP_FULL_YELLOW; break;
	case 9 : cmd[2] = LP_LOW_ORANGE; break;
	case 10 : cmd[2] = LP_MED_ORANGE; break;
	case 11 : cmd[2] = LP_FULL_ORANGE; break;
	}
      }
      else
	cmd[2]  = STAT_PUSH_NOT_INIT;
    }
    LOCK_KEY(key);
    key->ActualColor = cmd[2];
    UNLOCK_KEY(key);
  }
}

/* Mode normal, touche relachée */
static void              HandleRelease(t_lpd *lpd, unsigned char *cmd)
{
  t_key                  *key;
  
  if (cmd[0] == LP_MENU)
    cmd[2] = ManageMenu(lpd, cmd[1], 0);
  else {
    if (lpd->Flags & LPD_FLG_DRAW_MODE) {
      key = &lpd->LightGrid[(cmd[1] / 16) * 9 + (cmd[1] % 16)];
      if ((key->Id % 16) == 8)
	cmd[2] = ReleaseSideKeys(key);
      if (key->lpd->Flags & (LPD_FLG_USER1 | LPD_FLG_USER2))
	cmd[2] = STAT_OFF;
      else
	cmd[2] = key->DefColor;
    }
    else {
      key = &lpd->Grid[lpd->CurrGrid][(cmd[1] / 16) * 9 + (cmd[1] % 16)];
      if (key->lpd->Flags & (LPD_FLG_USER1 | LPD_FLG_USER2))
	cmd[2] = STAT_OFF;
      else
	cmd[2] = ReleaseKey(key);
    }
    LOCK_KEY(key);
    key->ActualColor = cmd[2];
    UNLOCK_KEY(key);
  }
}

/* Réactions à l'événement */
static void              HandleEvent(t_lpd *lpd, unsigned char *buff, int nb)
{
  int                    a, b;
  unsigned char          cmd[6];

  for (b = a = 0; a < nb; ) {
    if (buff[a] == LP_GRID) {
      lpd->Stat = LP_GRID;
      ++a;
    }
    else if (buff[a] == LP_MENU) {
      lpd->Stat = LP_MENU;
      ++a;
    }
    else if (buff[a] == LP_OPTION) {
      ++a;
      a += HandleDriverOptions(lpd, &buff[a]);
      continue;
    }
    cmd[b++] = lpd->Stat; /* 1er octet */
    cmd[b++] = buff[a++]; /* 2ème octet */
    if (buff[a]) { /* Touche appuiée */
      if (app->Flags & APP_FLG_MIDI_MODE)
	HandlePushForMidi(lpd, &cmd[b - 2]);
      else
	HandlePush(lpd, &cmd[b - 2]);
    }
    else { /* Touche relachée */
      if (app->Flags & APP_FLG_MIDI_MODE)
	HandleReleaseForMidi(lpd, &cmd[b - 2]);
      else
	HandleRelease(lpd, &cmd[b - 2]);
    }
    ++b; ++a;
    if (b == 6) {
      SendToDevice(lpd, cmd, 6);
      b = 0;
    }
  }
  if (b == 3)
    SendToDevice(lpd, cmd, 3);
}

/* THREAD - Capture des évenements */
void               *ManageEvents(void *param)
{
  int              nb;
  fd_set           slrd;
  unsigned char    buff[250];
  struct timeval   time;
  t_lpd            *lpd = param;

  if (!lpd->Fd)
    goto end_ManageEvents;
  while (1) {
    time.tv_sec = 1;
    time.tv_usec = 250000;
    FD_ZERO(&slrd);
    LOCK_LPD(lpd);
    if (lpd->Fd > 1)
      FD_SET(lpd->Fd, &slrd);
    else  {
      UNLOCK_LPD(lpd);
      goto end_ManageEvents;
    }
    nb = lpd->Fd + 1;
    UNLOCK_LPD(lpd);
    //# a verif
    select(nb, &slrd, NULL, NULL, &time);
    LOCK_LPD(lpd);
    if (lpd->Fd) {
      if (FD_ISSET(lpd->Fd, &slrd)) {
	if ((nb = read(lpd->Fd, buff, 250)) < 0) {
	  PrintError(2, ERR_TYPE_ERRNO, 0);
	  goto end_ManageEvents;
	}
	UNLOCK_LPD(lpd);
	HandleEvent(lpd, buff, nb);
      }
      else
	UNLOCK_LPD(lpd);
    }
    else
      UNLOCK_LPD(lpd);
  }
 end_ManageEvents:
  LOCK_APP(LockPonctual);
  lpd->IdEvent = 0;
  UNLOCK_APP(LockPonctual);
  return (0);
}
