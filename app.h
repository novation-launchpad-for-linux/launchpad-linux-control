/*
 *  'launchpadctrl' allows you to use your Novation launchpad (NVLPD01)
 *  Copyright (C) 2014-2017  Vincent Deca. known as Bigcake@ubuntu-fr.org
 *  (user@forum not email)
 *
 *  vincent.deca@neutralite.org
 *
 *  This file is part of 'launchpadctrl'.
 *
 * 'launchpadctrl' is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  'launchpadctrl' is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with 'launchpadctrl'.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INC_APP_H
#define INC_APP_H

#include <alsa/asoundlib.h>
#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/select.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <termios.h>
#include <unistd.h>

#include "protocole.h"

#include <gst/gst.h>
#include <glib.h>


/* ------------------- DEFINE --------------------- */
#define APP_VERSION            "0.23:170130"
#define DRIVER_REQ_VERSION 28  /* Chiffre requis pour la version du driver 0.28 */

#define APP_NB_STRINGS     196   /* Nombre de chaines de caractères dans les tableaux de langue */
#define APP_LIMIT_USHORT   65530 /* Limitation lors d'utilisation d'un unsigned short */
#define APP_LIMIT_UCHAR    254   /* Limitation lors d'utilisation d'un unsigned char */
#define SIZE_BUFFER_EVENT  500   /* Taille du buffer des évènements enregistrés */
#define SIZE_BUFFER_TMP    250   /* Taille du buffer de lecture sur le launchpad */
#define SIZE_BUFFER_SHELL  1000  /* Taille du buffer de lecture sur stdin  */
#define SIZE_BUFFER_LOAD   1000  /* Taille du buffer pour la lecture du fichier de configuration */
#define DEFAULT_NB_HISTORY 10    /* Taille de l'historique pour le shell */
#define DEFAULT_SHELL      "/bin/sh -c" /* shell par défaut pour les commandes inconnues */
#define DEFAULT_LAUNCHPAD  "/dev/nlp0"  /* Péripérique par défaut */
#define SIG_SAV_V1         "#nlpv1.0\n" /* Signature du fichier de sauvegarde */
#define STEP_SOUND_VOLUME  0.0714       /* Cran de volume pour la hausse ou la baisse */
#define STEP_NB_SERVERS    5     /* Cran du nombre de case a allouer pour le tableau de serveur */

#define KEY_NB_SIMULTANEOUS 10   /* Maximum de lancment d'une même commande/son (doit être inférieur à 255) */

#define TYPE_FUNC_CMD     (void (*)(char *))  /* */

#define LIST_CMDTAB {"add", "close", "connect", "copy", "del", "disconnect" \
      , "exit", "grid", "help", "link", "list", "load", "mode", "move", "mute" \
      , "open", "pad", "run", "rlin", "set", "stop", "save", "sleep", "slin" \
      , "umute", "vol", 0}

/* Couleurs en fonction du type et de l'état */
#define STAT_OFF             LP_OFF           /* Etat de touche non assigné, non activée, ni appuyée */
#define STAT_IDLE_CMD        LP_LOW_ORANGE    /* Etat de touche de commande, non activée, ni appuyée */
#define STAT_IDLE_AUDIO      LP_LOW_YELLOW    /* Etat de touche de son, non activée, ni appuyée */
#define STAT_IDLE_ERROR      LP_LOW_RED       /* Etat de touche en erreur, non activée, ni appuyée */
#define STAT_IDLE_MENU       LP_LOW_GREEN     /* Etat de touche de menu non activée, ni appuyée */
#define STAT_PAUSE_AUDIO     LP_FULL_YELLOW   /* Etat de touche de son activée mais en pause */
#define STAT_RUNNING_CMD     LP_FULL_ORANGE   /* Etat de touche de commande en cours d'execution, non appuyée */
#define STAT_RUNNING_AUDIO   LP_FULL_GREEN    /* Etat de touche de son en cours de lecture */
#define STAT_PUSH_ERROR      LP_FULL_RED      /* Etat de touche en erreur appuyée */
#define STAT_PUSH_SUCCESS    LP_FULL_GREEN    /* Etat de touche appuyée sans erreur */
#define STAT_PUSH_NOT_INIT   LP_FULL_YELLOW   /* Etat de touche non initialisé appuyée  */
#define STAT_VOL_MAX         LP_FULL_GREEN    /* Touche de volume activée au max */
#define STAT_VOL_MIN         LP_LOW_GREEN     /* Touche de volume activée au minimum */
#define STAT_MUTE_ON         LP_FULL_RED      /* Touche de mute activée */
#define STAT_MUTE_OFF        LP_LOW_RED       /* Touche de mute désactivée */
#define STAT_ACTIVE_MENU     LP_FULL_GREEN    /* Touche de menu activée */
#define STAT_OPTION_AVAILABLE  LP_LOW_ORANGE  /* Touche disponible pour activer une option */

/* Place des touches de cotés dans les grilles */
#define SIDE_VOLUME         8
#define SIDE_PAN            17
#define SIDE_SNDA           26
#define SIDE_SNDB           35
#define SIDE_STOP           44
#define SIDE_TRKON          53
#define SIDE_SOLO           62
#define SIDE_ARM            71

/* t_lpd.Flags */
#define LPD_FLG_SESSION     (1 << 0)  /* Le bouton 'session' est activé */
#define LPD_FLG_USER1       (1 << 1)  /* Le bouton 'user1' est activé */
#define LPD_FLG_USER2       (1 << 2)  /* Le bouton 'user2' est activé */
#define LPD_FLG_MIXER       (1 << 3)  /* Le bouton 'mixer' est activé */
#define LPD_FLG_MENU_LIGHT  (1 << 4)  /* Les flèches sont désactivés */
#define LPD_FLG_IN_USE      (1 << 5)  /* La structure est utilisée */
#define LPD_FLG_DRAW_MODE   (1 << 6)  /* Le launchpad est en mode de dessin */
#define LPD_FLG_RANDOM_USER (1 << 7)  /* La lumière lors de l'appui sur une touche en mode USER est aléatoire */

/* t_mxr.Flags */
#define LPD_MIXER_VOL       (1 << 0)  /* le volume est activé coté mixer */
#define LPD_MIXER_2ND_PUSH  (1 << 1)  /* La touche de coté a été appuyée une 2ème fois en mod mixer */

/* t_app.Flags */
#define APP_FLG_NO_SHELL    (1 << 0)  /* Le shell est désactivé */
#define APP_FLG_CTRLD       (1 << 1)  /* Ctrl+d a été tapé dans le shell */
#define APP_FLG_SHELL_CMD   (1 << 2)  /* Flags pour savoir si l'action viens du shell */
#define APP_FLG_NO_CHECK    (1 << 3)  /* Le thread de connexion auto lors du branchement d'un launchpad ne sera pas lancé */
#define APP_FLG_IS_FLASHING (1 << 4)  /* Le mode de clignotement est activé */
#define APP_FLG_LOAD_FILE   (1 << 5)  /* Un fichier .nlp est en cours de chargement */
#define APP_FLG_MIDI_MODE   (1 << 6)  /* Le mode MIDI est activé */
#define APP_FLG_STOP_TEMPO  (1 << 7)  /* Le mode tempo doit s'arrêter */

#define APP_FLG_QUIET       (1 << 8)  /* Le mode quiet a été activé */
#define APP_FLG_CLOSING     (1 << 9)  /* L'application est en train de se fermer */

/* t_key.Flags */
#define KEY_FLG_PUSHED      (1 << 0)  /* La touche est actuellement appuyé */
#define KEY_FLG_IN_USE      (1 << 1)  /* La commande est actuellement en cours */
#define KEY_FLG_SINGLE      (1 << 2)  /* La commande ne peut pas être lancée plusieurs fois simultanément */
#define KEY_FLG_LOOP        (1 << 3)  /* A la fin de la commande, elle sera relancé */
#define KEY_FLG_KEEP        (1 << 4)  /* Si la commande est en cours, elle est arrêté et relancé */
#define KEY_FLG_INTERRUPTED (1 << 5)  /* La commande en cours a été interrompu(lié a KEY_FLG_RESTART) */
#define KEY_FLG_NOT_INIT    (1 << 6)  /* La touche n'a pas été assigné par l'utilisateur */
#define KEY_FLG_STOP_LOOP   (1 << 7)  /* Arrêt du loop demandé */

#define KEY_FLG_RUN_ERROR   (1 << 8)  /* Une erreur est apparu lors  */
#define KEY_FLG_COL_STOP    (1 << 9)  /* Arrêt si une touche sur la même colonne est appuiée */
#define KEY_FLG_LINE_STOP   (1 << 10) /* Arrêt si une touche sur la même ligne est appuiée */
#define KEY_FLG_HOLD_IT     (1 << 11) /* Lors de l'appuie le son/commande est lancé, lors du relachement, le son/commande est arrêté */
#define KEY_FLG_LOCK        (1 << 12) /* La touche est verrouillée, rien ne peut être lancé */
#define KEY_FLG_PAUSE       (1 << 13) /* le 2ème appui provoque la pause */
#define KEY_FLG_IS_PAUSED   (1 << 14) /* le son est actuellement en pause */
#define KEY_FLG_LNK_SIMUL   (1 << 15) /* La touche est lié a une autre, en simultané */

#define KEY_FLG_LNK_LOOP    (1 << 16) /* La touche est lié a une autre, en boucle */
#define KEY_FLG_LNK_PUSHED  (1 << 17) /* La touche lié est celle qui a réelement été appuyée */

/* ------------------------- ENUM ------------------------- */

/* Grilles spécifiques */
enum {
  GRID_USER1 = 9    /* Grille de 'User 1' */
  , GRID_USER2      /* Grille de 'User 2' */
  , GRID_VOLUME     /* Grille du volume */
  , GRID_NUMBER     /* Nombre de grilles */
};

/* t_key.Type */
enum {
  KEY_TYPE_EXEC = 1    /* CmdFile est un fichier executable */
  , KEY_TYPE_AUDIO     /* CmdFile est un fichier audio */
  , KEY_TYPE_SERVER    /* CmdFile est un serveur avec son port */
};

/* t_key.Tempo */
enum {
  KEY_TMO_INSTANT = 0  /* Lancement instantané */
  , KEY_TMO_8          /* Lancement au bout de 8 temps */
  , KEY_TMO_4          /* Lancement au bout de 4 temps */
  , KEY_TMO_2          /* Lancement au bout de 2 temps */
  , KEY_TMO_1          /* Lancement au bout de 1 temps */
  , KEY_TMO_0_5        /* Lancement au bout de 1/2 temps */
  , KEY_TMO_0_25       /* Lancement au bout de 1/4 temps */
  , KEY_TMO_0_125      /* Lancement au bout de 1/8 temps */
  , KEY_TMO_0_0625     /* Lancement au bout de 1/16 temps */
};

/* PrintError(TYPE) */
enum {
 ERR_TYPE_FILE
 , ERR_TYPE_SIMPLE
 , ERR_TYPE_ERRNO
 , ERR_TYPE_KEY
};

/* ------------------- DEFINE SONORE --------------------- */
#define PAUSE_SOUND(aud, pos)        gst_element_set_state(aud->Pipe, GST_STATE_PAUSED)
#define PLAY_SOUND(aud, pos)         gst_element_set_state(aud->Pipe, GST_STATE_PLAYING)

#define IN_USE_SOUND(val, pos)      (val->Flg[pos])
#define MODIFY_VOL_SOUND(aud, pos, vol)  g_object_set(GST_OBJECT(aud->Vol), "volume", vol, NULL);

/* ------------------- DEFINE FUNCTION --------------------- */
 /* Fait clignoter la couleur */
#define KEY_FLASHING_COLOR(color)  color &= ~(LP_COPY)

#define DEBUG(val)        printf("Debug: %s(): '%s'\n", __FUNCTION__, val)

//#define LOCK_APP(val)     {printf("%s: LOCK APP(%p)\n", __FUNCTION__, &app->val); pthread_mutex_lock(&app->val);}
//#define UNLOCK_APP(val)   {printf("%s: UNLOCK APP(%p)\n", __FUNCTION__, &app->val); pthread_mutex_unlock(&app->val);}
#define LOCK_APP(val)     pthread_mutex_lock(&app->val)
#define UNLOCK_APP(val)   pthread_mutex_unlock(&app->val)

//#define LOCK_KEY(val)     {printf("%s: LOCK KEY(%d)\n", __FUNCTION__, (val)->Id); pthread_mutex_lock(&(val)->Lock);}
//#define UNLOCK_KEY(val)   {printf("%s: UNLOCK KEY(%d)\n", __FUNCTION__, (val)->Id); pthread_mutex_unlock(&(val)->Lock);}
#define LOCK_KEY(val)     pthread_mutex_lock(&(val)->Lock)
#define UNLOCK_KEY(val)   pthread_mutex_unlock(&(val)->Lock)

//#define LOCK_LPD(val)     {printf("%s: LOCK LPD(%p)\n", __FUNCTION__, val); pthread_mutex_lock(&(val)->Lock);}
//#define UNLOCK_LPD(val)   {printf("%s: UNLOCK LPD(%p)\n", __FUNCTION__, val); pthread_mutex_unlock(&(val)->Lock);}
#define LOCK_LPD(val)     pthread_mutex_lock(&(val)->Lock)
#define UNLOCK_LPD(val)   pthread_mutex_unlock(&(val)->Lock)

//#define LOCK_MUTEX(val)     {printf("%s: LOCK MUTEX(%p)\n", __FUNCTION__, &val); pthread_mutex_lock(&val);}
//#define UNLOCK_MUTEX(val)   {printf("%s: UNLOCK MUTEX(%p)\n", __FUNCTION__, &val); pthread_mutex_unlock(&val);}
#define LOCK_MUTEX(val)     pthread_mutex_lock(&val)
#define UNLOCK_MUTEX(val)   pthread_mutex_unlock(&val)

#ifdef DEV_MODE
#define ASSERT(cond, msg)    {						\
    if (cond) {								\
      printf("\nASSERT: %s: %s\n", __FUNCTION__, msg);			\
      QuitApp(0);							\
    }									\
  }
#else
#define ASSERT(cond, msg)     ;
#endif /* DEV_MODE */

/*
#define SendToDevice(lpd, cmd, nb)  {					\
    size_t           rd;						\
    printf("SendToDevice:%s:", __FUNCTION__);				\
    LOCK_LPD(lpd);							\
    if (lpd->Fd > 1) {							\
      printf("\tSend (%d) to (%d): [%d] [%d] [%d]", nb, lpd->Fd, cmd[0], cmd[1], cmd[2]); \
      if (nb > 3)							\
	printf(" [%d] [%d] [%d]", cmd[3], cmd[4], cmd[5]);		\
      printf("\n");							\
      if ((rd = write(lpd->Fd, cmd, nb)) < nb)  {			\
	UNLOCK_LPD(lpd);						\
	LOCK_APP(LockOutput);						\
	printf("%s (fd %d, %d, %d): %s\n", app->Strs[33], lpd->Fd, rd, nb, strerror(errno)); \
	UNLOCK_APP(LockOutput);						\
	PrintLPEntry();							\
	CmdClose((void*)1);						\
	LOCK_APP(LockPonctual);						\
	if (!(app->Flags & APP_FLG_NO_CHECK))				\
	  if (!lpd->IdCheck) {						\
	    if (pthread_create(&lpd->IdCheck, &app->Attr, ThreadCheckNewLaunchpad, lpd)) \
	      PrintError(7, ERR_TYPE_ERRNO, 0);				\
	    else							\
	      pthread_detach(lpd->IdCheck);				\
	  }								\
	UNLOCK_APP(LockPonctual);					\
      }									\
    }									\
    UNLOCK_LPD(lpd);							\
  }
//*/

/* ------------------- STRUCTURE --------------------- */
typedef struct s_Key t_key;         /* Structure lié au touches */
typedef struct s_Server t_svr;      /* Structure pour les infos serveurs */
typedef struct s_Launchpad t_lpd;   /* Structure de launchpad */

/* Structure pour les tempos défilants */
typedef struct      s_Tempo
{
  unsigned char     Flags;           /* Sauvegarde de paramètres */
  unsigned char     Pos;             /* Position actuel du tempo (1-254), 0 = arrêté) */
}                   t_tpo;

/* Structure pour les infos serveurs */
struct              s_Server
{
  char              *Buff;           /* Données à envoyer lors de la connexion au serveur */
  char              *Host;           /* Nom d'hote / IP */
  unsigned short    Id;              /* Place dans le tableau de serveur */
  pthread_mutex_t   Lock;            /* Lock pour la structure */
  char              *Port;           /* Port de connexion sur l'hote */
  unsigned short    Size;            /* Taille de t_svr.Buff*/
  int               Socket;          /* Socket pour l'ecriture */
  unsigned short    Uid;             /* Id donnée par le fichier .nlp */
}; /*               t_svr; */

/* Structure pour les options de mixer */
typedef struct      s_Mixer
{
  unsigned char     Flags;           /* Sauvegarde de paramètres */
  float             Mute[8];         /* Volume avant l'activation de la sourdine */
  float             Vol[8];          /* Volume par ligne ou colonne */
}                   t_mxr;

/* Structure pour la liste de commandes */
typedef struct      s_ShellCommand
{
  char              * restrict Cmd;  /* Chaine de caratère correspondant à la commande */
  void              (*Func)(char *); /* Fonction lancée */
  unsigned char     Len;             /* Taille de Cmd */
}                   t_scd;

/* Structure lié aux fichiers audio */
typedef struct            s_AudioFile
{
  char                    *OggFile;     /* Chemin vers le fichier ogg */
  guint                   BusId;        /* Id du bus */
  GMainLoop               *Loop;        /* Loop */
  GstElement              *Pipe;        /* Pipeline */
  GstElement              *Source;      /* Source */
  GstElement              *Demuxer;     /* Demuxer */
  GstElement              *Decoder;     /* Decoder */
  GstElement              *Conv;        /* Conv */
  GstElement              *Vol;         /* Volume */
  GstElement              *Sink;        /* Sink */
  GstElement              *Resample;    /* resample */
  unsigned char           Flg[KEY_NB_SIMULTANEOUS]; /* Tableau de sources */
}                         t_af;

/* Structure de launchpad */
struct                    s_Launchpad
{
  unsigned short          Flags;              /* Sauvegarde de paramètres */
  pthread_t               IdCheck;            /* ID du thread 'check' */
  unsigned char           CurrGrid;           /* Grille en cours d'utilisation */
  char                    * restrict DevName; /* Nom du périphérique à ouvrir */
  int                     Fd;                 /* File descriptor du launchpad */
  t_key                   ** restrict Grid;   /* Tableau de Grilles */
  pthread_t               IdEvent;            /* ID du thread 'events' */
  unsigned char           LastGrid;           /* Dernière grille utilisée */
  t_key                   *LightGrid;         /* Grille temporaire pour light */
  pthread_mutex_t         Lock;               /* Verrou pour la structure */
  t_mxr                   Mixer;              /* Structure du mixer */
  unsigned char           Stat;               /* = LP_GRID ou LP_MENU */
  unsigned char           TempColor;          /* Couleur temporaire  pour le mode 'draw' */
}; /*                     t_lpd; */

/* Structure lié au touches */
struct                    s_Key
{
  unsigned char           ActualColor;  /* Couleur actuelle de la touche */
  t_af                    *Audio;       /* Pointeur vers la structure audio si fichier audio */
  char                    *CmdFile;     /* Commande ou fichier */
  size_t                  CmdLen;       /* Longueur de t_key.CmdFile */
  char                    **CmdTab;     /* CmdFile converti en tableau */
  unsigned short          Count;        /* Compte d'utilisation / Id pour les commandes serveurs */
  unsigned char           DefColor;     /* Couleur par défaut sur la touche */
  unsigned int            Flags;        /* Sauvegarde de paramètres */
  unsigned char           Id;           /* N° de la touche, valeur connu par le launchpad */
  t_key                   *Link;        /* Pointeur vers la touche lié */
  pthread_mutex_t         Lock;         /* Verrou de la structure */
  pid_t                   Pid[KEY_NB_SIMULTANEOUS]; /* tableau de pid des fils lors de l'execution d'une commande */
  unsigned char           PushedColor;  /* Couleur lors de l'appuie sur la touche */
  unsigned char           Tempo;        /* La fonction de la touche s'activera au prochain tempo */
  unsigned char           Type;         /* Type de commande */
  t_lpd                   *lpd;         /* Structure de launchpad parent */
}; /*                     t_key; */

/* Structure principale de l'application */
typedef struct            s_Application
{
  pthread_attr_t          Attr;               /* Attributs des trheads */
  unsigned char           CurrLpd;            /* Launchpad en cours de configuration */
  char                    *Entry;             /* Ligne de saisie actuel dans le shell */
  unsigned short          Flags;              /* Sauvegarde de paramètres */
  //  pthread_t               IdGui;              /* ID du thread 'graphique' */
  pthread_t               IdTempo;            /* ID du thread 'tempo' */
  pthread_t               IdServer;           /* ID du thread 'serveurs' */
  pthread_t               IdShell;            /* ID du thread 'shell' */
  t_lpd                   **Launchpad;        /* Tableau de structure de launchpad */
  pthread_mutex_t         LockCond;           /* Mutex d'attente de la condition tempo */
  pthread_mutex_t         LockFlg;            /* Verrou pour app.Flags */
  pthread_mutex_t         LockPonctual;       /* Verrou pour les autres éléments utilisés pontuellement */
  pthread_mutex_t         LockOutput;         /* Verrou pour l'ecriture sur la sortie standard */
  unsigned short          MaxServers;         /* Nombre de place disponible dans app.Servers */
  unsigned char           NbLaunchpad;        /* Nombre de launcpad dans la tableau */
  t_svr                   **Servers;          /* Tableau de structure de serveur */
  int                     ShellPos;           /* Position du curseur dans app.Entry */
  char                    * restrict ShellBuffer; /* Buffer de copie pour ctrl+u, ctrl+k, ctrl+y */
  unsigned char           ShellHistPos;       /* Position dans l'historique */
  char                    ** restrict ShellHistory; /* Historique des commandes shell */
  char                    **Strs;             /* Tableau de textes */
  unsigned int            TabCount;           /* Nombre de tabulation effectué à la suite */
  pthread_cond_t          WaitTempo;

  int                     MidiPort;           /* Port Midi*/
  void                    *MidiHandle;        /* Handle pour le sequenceur MIDI */
}                         t_app;

/* ------------------- EXTERN --------------------- */
extern t_app              * restrict app;          /* Pointeur vers la structure principale de l'application */
extern char               *EnglishTab[];           /* Tableau de langue anglaise */
extern char               *FrenchTab[];            /* Tableau de langue française */

/* ---------------- DECLARATION ------------------- */
/* ----- Thread Functions : fonctions lancées comme thread */
/* Capture des évenements sur le launchpad */
void             *ManageEvents(void *param);
/* Gestion des commandes shell */
void             *ManageShell(void *param);
/* Jouer un son */
void             *PlaySound(void *t_key);
/* Ouverture automatique au démarrage - ne vérifie que les 10 premiers /dev/nlpX */
void             *ThreadCheckNewLaunchpad(void *param);
/* Surveille la connexion avec les serveurs */
void             *ThreadSurveyServer(void *param);

/* ---- LOCK_KEY() devrai appelé avant et UNLOCK_KEY() après */
/* Gestion de la couleur de la touche après la fin */
void             EndRunSwitchLight(t_key *key, unsigned char color, unsigned char grid);
/* Arrêt de la comande ou son en cours */
void             KillRunningOne(t_key *key);
/* Remise a zéro de la touche */
void             ResetKey(t_key *key);
/* Touche appuyé ou command 'run' */
unsigned char    PushedKey(t_key *key);
/* Touche relaché ou command 'run' */
unsigned char    ReleaseKey(t_key *key);
/* Touché sur le coté droit de la grille appuyé */
unsigned char    PushedSideKeys(t_key *key);
/* Touché sur le coté droit de la grille appuyée */
unsigned char    ReleaseSideKeys(t_key *key);
/* Mise à jour d'une touche (sauf menu) */
void             UpdateKey(t_key *key);

/* ---- Si LOCK_KEY() a été appelé, dans une fonction parente faire un UNLOCK_KEY() avant */
/* Gère l'annulation lors de l'appui sur la touche mixer si un 2ème appui sur une touche de coté a été fait */
void             CancelSecondPush(t_lpd *lpd);
/* Mise à jour de 2 touches (sauf menu) */
void             UpdateTwoKey(t_lpd *lpd, t_key *key1, t_key *key2);

/* ---- Autres fonctions */
/* Gestion de la touche volume en mode mixer */
void             ActiveVolumeOption(t_lpd *lpd);
/* Ajout d'une structure de launchpad dans le tableau */
unsigned char    AddLaunchpad(t_lpd *lpd);
/* Ajout d'une structure serveur dans le tableau */
unsigned char    AddServerStruct(t_svr *svr);
/* Fermeture du système sonore */
void             CloseSoundSystem();
/* Changement de grille */
void             ChangeGrid(t_lpd *lpd, unsigned char nb);
/* Changement du volume */
void             ChangeVolume(t_lpd *lpd, const unsigned char nb, float vol);
/* Change le volume à partir d'une touche de volume et mets à jour les couleurs */
void             ChangeVolumeFromKey(t_key *key);
/* Vérification de l'existance du serveur dans le tableau */
t_svr            *CheckExistServer(t_svr *svr);
/* Vérification de l'existance du serveur dans le tableau par son uid */
t_svr            *CheckExistServerId(unsigned short id);
/* Ferme la connexion au launchpad */
void             CmdClose(char *param);
/* Affiche l'aide pour les commandes */
void             CmdHelp(char *param);
/* Commande 'link' */
void             CmdLink(char *param);
/* Commande 'mode' */
void             CmdMode(char *param);
/* commande 'save' */
void             CmdSave(char *param);
/* Commande 'set' */
void             CmdSet(char *param);
/* Se connecte à tout les serveurs */
void             ConnectToAllServers();
/* Connexion au serveur */
int              ConnectToServer(char *host, char *port);
/* Désactive le mode de dessin */
void             DisableDrawMode(t_lpd *lpd);
/* Désactive le mode clignotement */
void             DisableFlash(t_lpd *lpd);
/* Active le mode clignotement */
void             EnableFlash(t_lpd *lpd);
/* Ferme l'application en affichant un message sur la sortie d'erreur */
void             ExitWithMsg(const char *msg);
/* Transforme un float xxxx.xxxx en char */
void             FloatToStr(char *buff, const float nb);
/* Libération des ressources prises par un fichier audio */
void             FreeAudioFile(t_af *af);
/* Libération des ressources prise par une structure d'infos serveur */
void             FreeServerStruct(t_svr *svr);
/* Remplace le 1er '.' par une ',' */
void             FrenchFloat(char *str);
/* Préparation de la touche pour le fichier audio */
void             GetAudioFile(t_key *key);
/* Récupère la saisie au clavier */
char             *GetEntry();
/* Récupération de la ligne, colonne et structure de la touche */
char             *GetKeyFromParam(t_lpd *lpd, char *param, t_key **key, char format);
/* Recherche d'une structure de launchpad disponible */
t_lpd            *GetLaunchpadStruct();
/* Récupération des options */
void             GetOptions(int ac, char **av);
/* Création d'une structure de launchpad */
t_lpd            *InitLaunchpad();
/* Initialise la partie volume du mixer */
unsigned char    InitMixerVolume(t_lpd *lpd);
/* Mode MIDI, touche appuyée */
void             HandlePushForMidi(t_lpd *lpd, unsigned char *cmd);
/* Mode MIDI, touche relachée */
void             HandleReleaseForMidi(t_lpd *lpd, unsigned char *cmd);
/* Initialisation du système sonore */
void             InitSoundSystem();
/* Mets à jour des flèches du menu */
void             LightMenuArrows(t_lpd *lpd, char flg);
/* Allume un bouton menu */
void             LightMenuMode(t_lpd *lpd, unsigned char on);
/* Allumage de la grille a la couleur par défaut */
void             LightGridDefColor(t_lpd *lpd, t_key *grid);
/* Découpage d'une ligne et retour d'un tableau de mot */
char              **LineToTab(char *line);
/* Chargement d'un fichier audio */
t_af             *LoadAudioFile(const char *file);
/* Chargement du fichier de configuration */
void             LoadConfigFile(const char *file);
/* Chargement d'un fichier de langue */
void             LoadLangFile(char *file);
/* Gestion des evenements launchpad a renvoyer sur le port MIDI */
void             ManageEventsForMidi(t_lpd *lpd, unsigned char *buff, int nb);
void             *ManageGlobalTempo(void *param);
/* Gestion des touches du menu */
unsigned char    ManageMenu(t_lpd *lpd, unsigned char key, char flg);
/* Ouverture du périphérique */
char             OpenDevice(t_lpd *lpd, const char *dev);
/* Affiche un message */
void             PrintError(unsigned short err, unsigned char type, const void *arg);
/* Réaffiche la ligne du shell */
void             PrintLPEntry();
/* Arrêt des LED et de l'application */
void             QuitApp(int param);
/* NO LOCK_APP FD - Envoi 1 ou 2 commande(s) au launchpad */
#ifndef SendToDevice
char             SendToDevice(t_lpd *lpd, const unsigned char *cmd, unsigned char nb);
#endif
/* Renvoi le nombre de caractère visible */
size_t           StrLen(const char *str);
/* Mise à jour de la grille */
void             UpdateGrid(t_lpd *lpd, unsigned char old, unsigned char new);
/* Mise à jour d'une touche de menu */
void             UpdateMenu(t_lpd *lpd, unsigned char key, unsigned char color);
/* Recherche des touche affecté par le changement de connexion au serveur */
void             UpdateRelatedKeys(t_svr *svr);
/* Mise à jour des lumières d'une colonne pour le volume */
void             UpdateVolumeColumn(t_lpd *lpd, unsigned char nb, t_key *key);
/* Transformation d'un unsigned short en chaine de caratère */
void             UShortToStr(unsigned short nb, char *buff);
/* Initialisation de la partie MIDI */
void             InitMidi();


/* Rechargement des fichers OGG pour éviter le segfault -_-' */
char             DirtyReloadOgg(t_af *af);


#ifdef DEV_MODE
char             *ColorStr(unsigned char nb);
void             PrintKey(t_key *key);
#endif /* DEV_MODE */

#endif /* INC_APP_H */
