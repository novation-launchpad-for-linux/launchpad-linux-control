/*
 *  'launchpadctrl' allows you to use your Novation launchpad (NVLPD01)
 *  Copyright (C) 2014  Vincent Deca. known as Bigcake@ubuntu-fr.org
 *  (user@forum not email)
 *
 *  vincent.deca@neutralite.org
 *
 *  This file is part of 'launchpadctrl'.
 *
 * 'launchpadctrl' is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  'launchpadctrl' is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with 'launchpadctrl'.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app.h"

char    *EnglishTab[] = {
  "Cannot initialize application", "Error", "Error while reading on device"
  , "Connecting to device", "Readind/Writing opening failed", "Failed" /* 5 */
  , "Success", "Couldn't create a thread", "Couldn't open configuration file"
  , "Cannot allocate memory", "Invalid configuration file" /* 10 */
  , "Invalid line in configuration file", "Cannot execute commande"
  , "Print this help", "<column><volume>: Change volume for a column"
  , "Connect to a launchpad (default: /dev/nlp0)" /* 15 */
  , "Invalid grid number", "Invalid grid line position"
  , "Invalid column position (must be between 1 and 8)", "Invalid working mode"
  , "Last run returned an error" /* 20 */
  , "Confirm exit with ctrl+d", "Loading configuration file"
  , "Unknow command", "Invalid dependancy mode"
  , "Unable to open sound device" /* 25 */
  , "Unknow Option", "Argument missing for option", "Unable to initialize a sound system"
  , "Invalid type", "No command or file specified" /* 30 */
  , "Command list : (use '? [command] for more details)"
  , "<X><Y>  <tempo>  <working option>  <dependancy>  <type>  <file>  [arguments]"
  , "Write on the launchpad failed"
  , "X : Position in line, from 1 to 8, from left to right"
  , "Y : Position in column, from 1 to 8, from top to bottom " /* 35 */
  , "working mode : The way a key will react when pushing it"
  , "An error occured when loading audio file"
  , "file : Path to the file (executable with arguments or audio)"
  , "<<X><Y> | all> : Reset one or all keys in the current grid"
  , "Close launchpadctrl" /* 40 */
  , "[LP command] : Print a help for all commands [or give details about one command]"
  , "<file> : Load a configuration file"
  , "</dev/nlpXX> : Connecting to launchpad", "<grid> : Change the current grid"
  , "type : Kind of file adssigned to the ket" /* 45 */
  , "The shell will not be launched at start", "Missing separator character ':'"
  , "Unable to activate audio context", "Searching audio sound system", "Ok" /* 50 */
  , "Fail", "Unable to create audio context"
  , "This command has reach the maximum simultaneous run number"
  , "A problem occured when retrieving samples", "Unknown audio format channel" /* 55 */
  , "A problem occured when preparing buffer", "Loading audio file"
  , "Key already assigned"
  , "<X><Y> : Stop the running command or sound assigned to the key"
  , "Something bad happened the shell input" /* 60 */
  , "Loop actived", "Loop deactived"
  , "<X><Y> : Execute the command or play the sound assigned to the key"
  , "[key | server] [Grid nb] : Print the list of servers or/and assigned keys [of one grid]"
  , "Grid" /* 65 */
  , "No key assigned", "<file> : Save the actual launchpad configuration"
  , "Couldn't open file with write rights", "Volume must be between 0.000 and 1.000"
  , "[column] : Set the volume to 0 [for only one column]" /* 70 */
  , "Close the connection to a launchpad", "this command has a shorcut, it means that,"
  , "for example, 'r15' is equivalent to 'run 15'"
  , "A space should not exists between some arguments"
  , "<column> : Execute and play all commands and sounds of a line" /* 75 */
  , "[column] : Put back the volume to the level it was before the mute [for only one column]"
  , "<column> : Stop all running commands and sounds of a line"
  , "dependancy : The command or sound will stop if an other key is pressed"
  , "Not a valide executable file"
  , "<timer> : wait before next command" /* 80 */
  , "Sleep timer invalid, must be > 0", "Sleeping", "seconds"
  , "The shell could not be initialized", "Not implemented yet" /* 85 */
  , "Write error", "No auto-connect when 1st plug of launchpad"
  , "The key is actually in use, and so can't be reset"
  , "tempo : Allow to syncronize the runs with the global tempo\n"
  "\t  0 = Instant launch\n"
  "\t  1-9 = Syncronized launch"
  , "Tempo no yet implemented, must be 0" /* 90 */
  , "Cannot access to directory", "Cannot retrieve the home user directory ($HOME)"
  , "Server connection", "Invalid line", "Limit number reached" /* 95 */
  , "Invalid value", "invalid server number", "Unknown server assigned to key"
  , "bytes", "No data to send to server" /* 100 */
  , "Problem occured when sending datas to server"
  , "Connection to server closed", "select() problem", "Connection lost"
  , "Server" /* 105 */
  , "No data sent at connection", "invalid argument for this command"
  , "'key' will print only keys, 'server' will print only servers"
  , "<id>  [<hostname/ip>  <port>  [datas]]: [Add and] connect a server"
  , "id : Number given to server, to assign keys with" /* 110 */
  , "Server already connected", "Configuration saved successfuly into"
  , "<id> : Disconnect and delete a server from list", "LP command"
  , "It's a available command in the LP shell\n\tif specified, the 'help' command will give more details about it" /* 115 */
  , "colomn : Column number, from 1 to 8, from left to right"
  , "volume : Value from 0.0 to 1.0", "Launchpad has been unplugged"
  , "for example, 's15' is equivalent to 'stop 15'"
  , "timer : 10 means 1 second" /* 120 */
  , "Mainly useful to put a pause between to commande separated by a ';'"
  , "file : Full or relative path + name of the file (~ not yet implemented)"
  , "line : Line number, from 1 to 8, from top to bottom"
  , "Managing only one launchpad for the moment"
  , "grid : Grid number, from 0 to 8 or 'A' or 'B'" /* 125 */
  , "hostname/ip, port : Hostname or IP and his port"
  , "datas : Datas to send to server at connection, replace if already exist"
  , "  0 = (executable only) every push run the action\n"
  "\t  H = A push run the action, a release stop the action\n"
  "\t  K = 1st push run the action, 2nd push does nothing\n"
  "\t  P = (audio only) the 1st push run the sound, 2nd push pause it, 3rd push continue it\n"
  "\t  S = 1st push run the action, 2nd stop it and restart it\n"
  "\t  L = 1st push run the action in loop, 2nd stop the loop, 3rd push reactivate the loop"
  , "  For a server command, must be like '<id>:<datas>'"
  , "  0 = Default, not affected\n"
  "\t  B = on the same line or column\n"
  "\t  C = on the same column\n"
  "\t  L = on the same line" /* 130 */
  , "Allow to assign a key (audio, executable or server cmd)"
  , "<XY1>  <XY2> : Move the assignement of a key"
  , "example", "Position 1 is source, position 2 is destination"
  , "Activate the MIDI working mode" /* 135 */
  , "The key is being used"
  , "Your actual version of the driver is not the same that should be used with launchpadctrl"
  , "The actual driver version", "The minimum needed driver version"
  , "Some malfunctions could appear" /* 140 */
  , "Sequencer initialization failed"
  , "<Launchpad Nb> : Change current launchpad to configure"
  , "Launchpad number depends on the connection order (1 to 254)"
  , "Invalid launchpad number (1 to 254, if that many launchpad are connected)"
  , "Two (or more) launchpads have not been connected" /* 145 */
  , "Unable to initialize audio file", "Audio format not recognized"
  , "Informations already loaded", "Drawing mode activated"
  , "More fonctionalities will come in next versions for this command" /* 150 */
  , "Desactivate error prints from gstreamer (by closing stderr)"
  , "The working mode is not available for the file type"
  , "<<X><Y> | all>  <parameter> <value> : Modify one or all keys of the current grid"
  , "<X><Y> | all : key position to modify or 'all' to modify all keys in the current grid"
  , "Unable to open /dev/null in read/write mode" /* 155 */
  , "volume", "hostname/ip"
  , "No server connection has this id number", "port", "datas" /* 160 */
  , "Modify all keys of th current grid"
  , "Quit drinking ! The positions of the source and destination key are the identical !"
  , "<parameter> <value>", "Available parameters are :"
  , "\tdep : Dependancy modification\n"
  "\tlight : not yet implemented\n"//Lights' key modification\n"
  "\ttempo : Tempo modification\n"
  "\ttype : File type modification\n"
  "\twork : Working mode modification\n"
  "\tfile : File modification (and arguments)" /* 165 */
  , "Unknown parameter", "value"
  , "  A = Audio file\n"
  "\t  E = Executable file"
  "\t  S = Server command"
  , "Carefull, value will take all everything that follows"
  , "Return events to MIDI port..." /* 170 */
  , "Key not yet available", "<name> : Activate or desactivate a mode"
  , "draw : Allow you to paint lights on launchpad\n"
  "\tquiet : Desactivate error prints from gstreamer\n"
  "\trandom : Colors will be random on user1 and user2 grids\n"
  "\ttempo : Start a rhythm to synchronize launched actions"
  , "Drawing mode desactivated", "Random colors mode activated" /* 175 */
  , "Random color mode desactivated", "<XY1> <XY2> : Copy key1 to key2"
  , "Tempo mode activated", "Tempo mode deactivated"
  , "0 or 1 : 0 means 'instant', 1 means 'follow global tempo'" /* 180 */
  , "No activated mode", "<XY1> <XY2> <type> : Allow you to link 2 keys"
  , "<XY1> <XY2> : Position of the 2 keys to link"
  , "<type> : Type of link between the 2 keys", "type" /* 185 */
  , "break : break the link between the 2 keys\n"
  "\tsingle : When the action of the first key is finished, the action of the second key is started\n"
  "\tsimul : When a key is pushed, the second key is run too\n"
  "\tloop : same as 'single', but in loop", "Warning"
  , "An unassigned key is linked to an other key"
  , "Quiet mode activated (cannot be deactivated)"
  , "Invalid link type (loop) for 2 server's commands" /* 190 */
  , "Problem with the executable file"
  , "Couldn't load language file"
  , "Too much line in language file (max lines:"
  , "One or some lines are missing in the language file"
  , "Load a language file" /* 195 */
};

char    *FrenchTab[] = {
  "Impossible d'initialiser l'application", "Erreur", "Erreur de lecture sur le périphérique"
  , "Connexion au périphérique", "Echec d'ouverture du périphérique en lecture/écriture"
  , "Echec" /* 5 */
  , "Succès", "Echec de création d'un thread", "Echec d'ouverture du fichier de configuration"
  , "Impossible d'allouer de la mémoire", "Fichier de configuration invalide" /* 10 */
  , "Ligne invalide dans le fichier de configuration", "Impossible de lancer la commande"
  , "Affiche cet aide", "<colonne><volume>: Change le volume d'une colonne"
  , "Se connecte à un launchpad (défaut: /dev/nlp0)" /* 15 */
  , "N° de grille invalide", "Position de ligne invalide"
  , "Position de colonne invalide", "Mode de fonctionnement invalide"
  , "Le dernier lancement a généré une erreur" /* 20 */
  , "Confirmer l'arrêt avec ctrl+d", "Chargement du fichier de configuration"
  , "Commande inconnue", "Mode de dépendance invalide"
  , "Impossible d'ouvrir un périphérique audio" /* 25 */
  , "Option inconnu", "Il manque un argument pour l'option", "Impossible d'initialiser un système audio"
  , "Type invalide", "Pas de commande ou fichier spécifié" /* 30 */
  , "Liste de commandes : (utiliser '? [commande]' pour plus de détails)"
  , "<X><Y>  <tempo>  <mode fonctionnement>  <dépendance>  <type>  <fichier>  [arguments]"
  , "Echec d'écriture sur le launchpad"
  , "X : Position dans la ligne, de 1 à 8, de gauche à droite"
  , "Y : Position dans la colonne, de 1 à 8, de haut en bas" /* 35 */
  , "mode fonctionnement : Manière dont va se comporter l'appui sur une touche"
  , "Une erreur est apparu lors du chargement du fichier audio"
  , "fichier : chemin vers le fichier (audio ou executable avec arguments)"
  , "<<X><Y>|all> : Réinitialise une ou toute les touches dans la grille courante"
  , "Ferme l'application" /* 40 */
  , "[commande LP] : Affiche une aide pour toute les commandes [ou une aide détaillée de la commande]"
  , "<fichier> : Charge un fichier de configuration"
  , "</dev/nlpXX> : Se connecte à un launchpad", "<N° de grille> : Change de grille courante"
  , "type : Type de fichier assignée à la touche" /* 45 */
  , "Le shell ne sera pas lancé au démarrage", "Caractère de séparation manquant ':'"
  , "Impossible d'activer le contexte audio", "Recherche d'un système sonore", "Ok" /* 50 */
  , "Echec", "Impossible de créer un contexte audio"
  , "La commande a atteint son maximum de lancement simultanés"
  , "Un problème est apparu lors de la récupération des échantillons", "Canal de format audio inconnu" /* 55 */
  , "Un problème est apparu lors de l'initialisation du tampon", "Chargement du fichier audio"
  , "Touche déjà assignée"
  , "<X><Y> : Arrête la commande ou le son assigné à la touche"
  , "Un problème est apparu sur la saisie clavier du shell" /* 60 */
  , "Boucle activé", "Boucle désactivé"
  , "<X><Y> : Lancement de la commande ou du son assigné à la touche"
  , "[key|server] [N° grille] : Affiche la liste des serveurs et/ou des touches assignées [d'une grille]"
  , "Grille" /* 65 */
  , "Aucune touche assignée", "<fichier> : Sauvegarde la configuration actuel du launchpad"
  , "Impossible d'ouvrir le fichier en écriture", "Le volume doit se situer entre 0.000 et 1.000"
  , "[colonne] : Mise à 0 du volume [sur une seule colonne]" /* 70 */
  , "Ferme la connexion vers le launchpad", "Cette commande a un raccourci, ce qui veut dire que,"
  , "par exemple, 'r15' est équivalent à 'run 15'"
  , "Un espace ne devrai pas exister entre des arguments"
  , "<ligne> : Lancement de toute les commandes et sons d'une ligne"  /* 75 */
  , "[colonne] : Remets le niveau du volume comme avant le mute [pour une seule colonne]"
  , "<ligne> : Arrêt de toute les commandes et sons en cours d'une ligne"
  , "dépendance : L'action s'arrête si une touche est appuyée"
  , "Fichier executable invalide"
  , "<temps>: marque une pause avant de lancer la commande suivante" /* 80 */
  , "Temps de pause invalide, doit être > 0", "En pause", "secondes"
  , "Le shell n'a pas pu être initialisé", "Pas encore implémenté" /* 85 */
  , "Erreur d'écriture", "Pas de connexion automatique lors du 1er branchement du launchpad"
  , "La touche est actuellement utilisée, et ne peut donc pas être réinitialisé"
  , "tempo : Permet de syncroniser le lancement avec le tempo global\n"
  "\t  0 = Lancement instantané\n"
  "\t  1-9 = Lancement syncronisé (modifications à venir)"
  , "Tempo non implementé, doit être 0" /* 90 */
  , "Impossible d'accéder au dossier", "Impossible de récuperer le dossier home de l'utilisateur ($HOME)"
  , "Connexion au serveur", "Ligne invalide", "Nombre limite dépassé" /* 95 */
  , "Valeur invalide", "N° de serveur invalide", "Serveur inconnu assigné à la touche"
  , "octets", "Aucune donnée pour l'envoi au serveur" /* 100 */
  , "Une problème est apparu lors de l'envoi de données au serveur"
  , "La connexion au serveur est fermée", "Problème de select()", "Connexion perdu"
  , "Serveur" /* 105 */
  , "Pas de données envoyés à la connexion", "Argument invalide pour la commande"
  , "'key' n'affiche que les touches, 'server' n'affiche que les serveurs"
  , "<id>  [<nom/ip>  <port>  [données]] : [Ajoute et] se connecte a un serveur"
  , "id : N° donné au serveur pour l'affectation des touches" /* 110 */
  , "Serveur déjà connecté", "Configuration sauvegardée avec succès dans"
  , "<id> : Déconnecte et supprime un serveur de la liste", "commande LP"
  , "Commande disponible du shell LP\n\tSi spécifié, l'explication de la commande sera plus détaillée" /* 115 */
  , "colonne : N° de la colonne, chiffre de 1 a 8, de gauche a droite"
  , "volume : Chiffre à virgule, valeur de 0.0 à 1.0"
  , "Le launchpad a été débranché"
  , "par exemple, 's15' est équivalent à 'stop 15'"
  , "temps : 10 équivaut à 1 seconde" /* 120 */
  , "Surtout utile pour mettre une pause entre 2 commandes séparé par ';'"
  , "fichier: Chemin complet ou relative + nom du fichier (~ non géré pour l'instant)"
  , "ligne : N° de la ligne, de 1 à 8, de haut en bas"
  , "Un seul launchpad géré pour l'instant"
  , "grille : N° de grille, de 0 à 8 ou 'A' ou 'B'" /* 125 */
  , "nom/ip, port : nom ou IP du serveur et son port"
  , "données : Données à envoyer au serveur lors de la connexion, remplace si existe déjà"
  , "  0 = (executable seulement) Chaque appui lance une action\n"
  "\t  H = L'appui lance l'action, le relachement stop l'action\n"
  "\t  K = 1er appui lance l'action, e 2ème ne fait rien\n"
  "\t  P = (audio uniquement) le 1er appui lance le son, le 2ème mets en pause, le 3ème stop la pause\n"
  "\t  S = 1er appui lance l'action, le 2ème stop celui en cours, et relance\n"
  "\t  L = 1er appui lance l'action en boucle, le 2ème appui stop la boucle,\n"
  "\t      le 3ème appui réactive la boucle"
  , "  Pour une commande serveur, doit être de la forme '<id>:<données>'"
  , "  0 = La touche n'est pas affectée\n"
  "\t  B = sur la même ligne ou colonne\n"
  "\t  C = sur la même colonne\n"
  "\t  L = sur la même ligne" /* 130 */
  , "Permet d'assigner une touche (audio, executable ou cmd serveur)"
  , "<XY1>  <XY2> : Déplace l'assignement d'une touche", "exemple"
  , "La position 1 est la source, la position 2 est la destination"
  , "Active le mode de fonctionnement en port MIDI" /* 135 */
  , "La touche est en cours d'utilisation"
  , "La version du driver utilisé n'est pas celle requise par launchpadctrl"
  , "La version du driver actuelle", "La version du driver minimum requise"
  , "Des dysfonctionnements risquent d'apparaitre" /* 140 */
  , "L'initialisation du sequenceur midi a échoué"
  , "<N° launchpad> : Change de launchpad pour la configuration"
  , "Le N° du lanchpad dépend de l'ordre de connexion (de 1 à 254)"
  , "N° de launchpad invalide (de 1 à 254, si autant de launchpad sont connectés)"
  , "Deux (ou plus) launchpads n'ont pas été connectés" /* 145 */
  , "Impossible d'initialiser le fichier audio", "Format audio non connu"
  , "Informations déjà chargés", "Mode de dessin activé"
  , "D'autre options apparaîtront au fur et à mesure des versions pour cette commande" /* 150 */
  , "Désactive l'affichage des messages d'erreurs de gstreamer (ferme la sortie d'erreur)"
  , "Le mode de fonctionnement n'est pas prévu pour le type de fichier"
  , "<<X><Y> | all> <paramètre> <valeur> : Modifie une ou toute les touches de la grille courante"
  , "<X><Y> | all : Position de la touche à modifier, ou 'all' pour modifier toute la grille courante"
  , "Impossible d'ouvrir /dev/null en lecture/écriture" /* 155 */
  , "volume", "nom/ip", "Aucun serveur n'a cet id", "port", "données" /* 160 */
  , "Modifie toute les touches de la grille courante"
  , "Arrête l'alcool ! Les positions des touches sont identiques !"
  , "<paramètre> <valeur>", "Les paramètres disponibles sont : "
  , "dep : Modification de la dépendance\n"
  "\tlight : non implémenté\n"//Modification des lumières\n"
  "\ttempo : Modification du tempo\n"
  "\ttype : Modification du type de fichier\n"
  "\twork : Modification du mode de fonctionnement\n"
  "\tfile : Modification du chemin vers le fichier (et arguments)" /* 165 */
  , "Paramètre inconnu", "valeur"
  , "  A = Fichier audio\n"
  "\t  E = Fichier executable\n"
  "\t  S = Commande serveur"
  , "Attention, la valeur prendra tout ce qui suit ce paramètre"
  , "Renvoi des infos vers le port MIDI" /* 170 */
  , "Touche non géré pour l'instant", "<nom> : Active ou désactive un mode"
  , "draw : Permet de dessiner avec les lumières du launchpad\n"
  "\tlist : Affiche la liste des modes activés\n"
  "\tquiet : Désactive l'affichage des messages d'erreurs de gstreamer\n"
  "\trandom : Sur les grilles user1 ou user2 les couleurs sont aléatoires\n"
  "\ttempo : Démarre un rythme qui permet de syncroniser le lancement des actions"
  , "Mode de dessin désactivé", "Mode de couleur aléatoire activé" /* 175 */
  , "Mode de couleur aléatoire désactivé"
  , "<XY1> <XY2> : Copie de la touche 1 vers la touche 2"
  , "Mode tempo activé", "Mode tempo désactivé"
  , "0 ou 1 : 0 signifie 'instantané', 1 signifie 'suit le tempo global'" /* 180 */
  , "Aucun mode activé", "<XY1> <XY2> <type> : Permet de lié 2 touches"
  , "<XY1> <XY2> : Positions des touches à lier"
  , "<type> : Type de lien entre les 2 touches", "type" /* 185 */
  , "break : Casse le lien entre les 2 touches\n"
  "\tsingle : Quand l'action de la première touche se finit, l'action de la 2ème est lancée\n"
  "\tsimul : Quand une touche est appuyée, l'action de la 2ème est aussi démarrée\n"
  "\tloop : Identique a 'single', mais en boucle", "Attention"
  , "Une touche non initialisée est lié à une autre touche"
  , "Mode quiet activé (non désactivable)"
  , "Type de lien (boucle) invalide entre 2 commandes serveur" /* 190 */
  , "Problème avec le fichier executable"
  , "Impossible de charger le fichier de langue"
  , "Trop de ligne dans le fichier language (max lignes:"
  , "Il manque une/des ligne(s) dans le fichier de language"
  , "Charge un fichier de language" /* 195 */
};

/* Chargement d'un fichier de langue */
void                 LoadLangFile(char *file)
{
  unsigned short     a, b;
  FILE               *fd;
  char               **strs;
  char               *buff;

  if ((fd = fopen(file, "r")) == NULL) {
    PrintError(192, ERR_TYPE_ERRNO, 0);
    return;
  }
  if ((strs = malloc((APP_NB_STRINGS) * sizeof(char *))) == NULL) {
    fclose(fd);
    PrintError(9, ERR_TYPE_ERRNO, 0);
    return;
  }
  if ((buff = malloc(SIZE_BUFFER_LOAD + 1)) == NULL) {
    free(strs);
    fclose(fd);
    PrintError(9, ERR_TYPE_ERRNO, 0);
    return;
  }
  buff[SIZE_BUFFER_LOAD] = '\0';
  for (a = 0; fgets(buff, SIZE_BUFFER_LOAD, fd) != NULL && a <= APP_NB_STRINGS; ++a) {
    for (b = 0; buff[b] != '\n' && buff[b] != '\0'; ++b)
      if (buff[b] == '\\') {
	if (buff[b + 1] == 'n') {
	  buff[b] = '\n';
	  if (SIZE_BUFFER_LOAD - b - 1)
	    memcpy(&buff[b + 1], &buff[b + 2], SIZE_BUFFER_LOAD - b - 1);
	}
	else if (buff[b + 1] == 't') {
	  buff[b] = '\t';
	  if (SIZE_BUFFER_LOAD - b - 1)
	    memcpy(&buff[b + 1], &buff[b + 2], SIZE_BUFFER_LOAD - b - 1);
	}
      }
    buff[b] = '\0';
    if ((strs[a] = strdup(buff)) == NULL) {
      if (a > 1) 
	for (--a; a != 0; --a)
	  free(strs[a]);
      if (strs[0])
	free(strs[0]);
      free(strs); free(buff);
      fclose(fd);
      PrintError(9, ERR_TYPE_ERRNO, 0);
      return;
    }
  }
  free(buff);
  fclose(fd);
  if (a > APP_NB_STRINGS) {
    LOCK_APP(LockOutput);
    printf("\r\033[31m%s\033[00m: %s: %s %d)\n", app->Strs[1], file, app->Strs[193], APP_NB_STRINGS);
    UNLOCK_APP(LockOutput);
  }
  else if (a < APP_NB_STRINGS) {
    LOCK_APP(LockOutput);
    printf("\r\033[31m%s\033[00m: %s (%d/%d)\n", app->Strs[1], app->Strs[194], a + 1, APP_NB_STRINGS);
    UNLOCK_APP(LockOutput);
    free(strs);
    if ((strs[a] = strdup(app->Strs[a])) == NULL) {
      if (a > 1) 
	for (--a; a != 0; --a)
	  free(strs[a]);
      if (strs[0])
	free(strs[0]);
      free(strs); free(buff);
      fclose(fd);
      PrintError(9, ERR_TYPE_ERRNO, 0);
      return;
    }
  }
  if ((&app->Strs[0] != &EnglishTab[0]) && (&app->Strs[0] != &FrenchTab[0])) {
    for (a = APP_NB_STRINGS - 1; a != 0; --a)
      free(app->Strs[a]);
    free(app->Strs[0]);
    free(app->Strs);
  }
  app->Strs = strs;
}

