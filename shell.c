/*
 *  'launchpadctrl' allows you to use your Novation launchpad (NVLPD01)
 *  Copyright (C) 2014-2017  Vincent Deca. known as Bigcake@ubuntu-fr.org
 *  (user@forum not email)
 *
 *  vincent.deca@neutralite.org
 *
 *  This file is part of 'launchpadctrl'.
 *
 * 'launchpadctrl' is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  'launchpadctrl' is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with 'launchpadctrl'.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app.h"

/* Change le ~ en chemin du home dans la chaine de caratères */
static char       *InsertHomeToString(char *str)
{
  unsigned short  a;
  size_t          len_h, len_e;
  char            *tmp, *home;
  
  for (tmp = 0, a = 0; str[a]; ++a)
    if (str[a] == '~') {
      home = getenv("HOME");
      len_h = strlen(home);
      len_e = strlen(app->Entry);
      if (a > 0 && str[a - 1] != ' ' && str[a - 1] != '\t')
	return (0);
      if ((tmp = malloc(len_h + len_e)) == NULL) {
	PrintError(9, ERR_TYPE_ERRNO, 0);
	return (0);
      }
      if (a)
	memcpy(tmp, str, a);
      memcpy(&tmp[a], home, len_h);
      memcpy(&tmp[a + len_h], &str[a + 1], len_e - a - 1);
      tmp[len_h + len_e - 1] = '\0';
      return (tmp);
    }
  return (0);
}

/* Contient le printf de PrintLPEntry() */
static void        PrintIt(char entry)
{
  unsigned char    grid;
  t_lpd          *lpd;

  if (app->Flags & APP_FLG_MIDI_MODE)
    return;
  lpd = app->Launchpad[app->CurrLpd];  
  if (app->NbLaunchpad > 1)
    printf("\rLP%d ", app->CurrLpd + 1);
  else
    printf("\rLP ");
  if (lpd->CurrGrid < GRID_USER1)
    grid = lpd->CurrGrid;
  else
    grid = lpd->LastGrid;
  if (lpd->CurrGrid >= GRID_USER1) {
    if (lpd->CurrGrid > GRID_USER2)
      printf("%d> ", lpd->LastGrid);
    else
      printf("%c> ", 'A' + lpd->CurrGrid - GRID_USER1);
  }
  else
    printf("%d> ", grid);
  if (entry)
    printf("%s ", app->Entry);
  fflush(stdout);
}

/* Réaffiche la ligne du shell */
void               PrintLPEntry()
{
  int              len;
  
  if (app->Flags & (APP_FLG_NO_SHELL | APP_FLG_MIDI_MODE))
    return;
  LOCK_APP(LockOutput);
  if (app->Entry) {
    PrintIt(1);
    len = StrLen(app->Entry) - StrLen(&app->Entry[app->ShellPos]) + 1 - app->ShellPos;
    ASSERT(len < 0, "taille entry - entry[ShellPos] - ShellPos < 0");
    for (; len; --len)
      if (write(1, "\b", 1) < 1)
	break;
  }
  else
    PrintIt(0);
  UNLOCK_APP(LockOutput);
}

/* Récupération de la ligne, colonne et structure de la touche */
char            *GetKeyFromParam(t_lpd *lpd, char *param, t_key **key, char format)
{
  int            col, line;

  if (format) {
    line = (*param) - '1';
    if (line < 0 || line > 7) {
      LOCK_APP(LockOutput);
      printf("\033[31m%s\033[00m: '%c', %s\n", app->Strs[1], *param, *param == ' ' ? app->Strs[74]: app->Strs[17]);
      UNLOCK_APP(LockOutput);
      return (0);
    }
    ++param;
    col = (*param) - '1';
    if (col < 0 || col > 7) {
      LOCK_APP(LockOutput);
      printf("\033[31m%s\033[00m: '%c', %s\n", app->Strs[1], *param, *param == ' ' ? app->Strs[74]: app->Strs[18]);
      UNLOCK_APP(LockOutput);
      return (0);
    }
    ++param;
  }
  else {
    line = atoi(param) - 1;
    if (line < 0 || line > 7) {
      LOCK_APP(LockOutput);
      printf("\033[31m%s\033[00m: '%d', %s\n", app->Strs[1], line + 1, app->Strs[17]);
      UNLOCK_APP(LockOutput);
      return (0);
    }
    for (; *param && (*param != ' ') && (*param != '\t'); ++param);
    for (; *param && (*param == ' ' || *param == '\t'); ++param);
    col = atoi(param) - 1;
    if (col < 0 || col > 7) {
      LOCK_APP(LockOutput);
      printf("\033[31m%s\033[00m: '%d', %s\n", app->Strs[1], col + 1, app->Strs[18]);
      UNLOCK_APP(LockOutput);
      return (0);
    }
  }
  if (lpd->CurrGrid > GRID_USER2)
    *key = &lpd->Grid[lpd->LastGrid][col * 9 + line];
  else
    *key = &lpd->Grid[lpd->CurrGrid][col * 9 + line];
  return (param);
}

/* commande 'del' */
static void     CmdDel(char *param)
{
  t_key         *key, *grid;
  unsigned char a;
  t_lpd          *lpd;

  lpd = app->Launchpad[app->CurrLpd];
  if (!param || (*param == '?')) {
    CmdHelp("del");
    return;
  }
  if (!strncmp("all", param, 3)) {
    if (lpd->CurrGrid > GRID_USER2)
      grid = lpd->Grid[lpd->LastGrid];
    else
      grid = lpd->Grid[lpd->CurrGrid];
    for (a = 0; a < 72; ++a) {
      key = &grid[a];
      LOCK_KEY(key);
      if (key->Flags & KEY_FLG_IN_USE)
	PrintError(88, ERR_TYPE_SIMPLE, 0);
      else if (!(key->Flags & KEY_FLG_NOT_INIT))
	ResetKey(key);
      UNLOCK_KEY(key);
    }
    return;
  }
  if (!(param = GetKeyFromParam(lpd, param, &key, 1)))
    return;
  LOCK_KEY(key);
  if (key->Flags & KEY_FLG_IN_USE)
    PrintError(88, ERR_TYPE_SIMPLE, 0);
  else
    ResetKey(key);
  UNLOCK_KEY(key);
}

/* commande 'grid' */
static void     CmdGrid(char *param)
{
  int           grid;
  t_lpd          *lpd;

  lpd = app->Launchpad[app->CurrLpd];
  if (!param || (*param == '?')) {
    CmdHelp("grid");
    return;
  }
  if (*param == 'A')
    grid = GRID_USER1;
  else if (*param == 'B')
    grid = GRID_USER2;
  else {
    grid = atoi(param);
    if (grid < 0 || grid > 8) {
      LOCK_APP(LockOutput);
      printf("\033[31m%s\033[00m: grid: %s\n", app->Strs[1], app->Strs[16]);
      UNLOCK_APP(LockOutput);
      return;
    }
  }
  ChangeGrid(lpd, grid);
  if (lpd->Fd > 1)
    LightMenuArrows(lpd, 1);
}

/* commande 'add' */
static void     CmdAdd(char *param)
{
  char          work, type, dep, tempo;
  t_key         *key;
  
  /* Récupération des paramètres */
  if (!param || (*param == '?')) {
    CmdHelp("add");
    return;
  }
  /* ligne / colonne */
  if (!(param = GetKeyFromParam(app->Launchpad[app->CurrLpd], param, &key, 1)))
    return;
  if (!(key->Flags & KEY_FLG_NOT_INIT)) {
    PrintError(58, ERR_TYPE_SIMPLE, 0);
    return;
  }
  /* Tempo */
  for (; *param && (*param != ' ') && (*param != '\t'); ++param);
  for (; *param && (*param == ' ' || *param == '\t'); ++param);
  tempo = *param;
  if (!tempo || tempo == '?') {
    CmdHelp("add 1");
    return;
  }
  if (tempo != '0')
    tempo = KEY_TMO_1;
  else
    tempo = KEY_TMO_INSTANT;
  /* Mode fonctionnement */
  for (; *param && (*param != ' ') && (*param != '\t'); ++param);
  for (; *param && (*param == ' ' || *param == '\t'); ++param);
  work = *param;
  if (!work || work == '?') {
    CmdHelp("add 2");
    return;
  }
  if (work != '0' && work != 'K' && work != 'S' && work != 'L' && work != 'H' && work != 'P') {
    LOCK_APP(LockOutput);
    printf("\033[31m%s\033[00m: %s: '%c'\n", app->Strs[1], app->Strs[19], work);
    UNLOCK_APP(LockOutput);
    return;
  }
  /* Dépendance */
  for (; *param && (*param != ' ') && (*param != '\t'); ++param);
  for (; *param && (*param == ' ' || *param == '\t'); ++param);
  dep = *param;
  if (!dep || dep == '?') {
    CmdHelp("add 3");
    return;
  }
  if (dep != '0' && dep != 'L' && dep != 'C' && dep != 'B') {
    LOCK_APP(LockOutput);
    printf("\033[31m%s\033[00m: %s: '%c'\n", app->Strs[1], app->Strs[24], dep);
    UNLOCK_APP(LockOutput);
    return;
  }
  /* Type de fichier */
  for (; *param && (*param != ' ') && (*param != '\t'); ++param);
  for (; *param && (*param == ' ' || *param == '\t'); ++param);
  type = *param;
  if (!type || type == '?') {
    CmdHelp("add 4");
    return;
  }
  if (type != 'A' && type != 'E' && type != 'S') {
    LOCK_APP(LockOutput);
    printf("\033[31m%s\033[00m: %s: %c\n", app->Strs[1], app->Strs[29], type);
    UNLOCK_APP(LockOutput);
    return;
  }
  /* Fichier */
  for (; *param && (*param != ' ') && (*param != '\t'); ++param);
  for (; *param && (*param == ' ' || *param == '\t'); ++param);
  if (!(*param) || *param == '?') {
    CmdHelp("add 5");
    return;
  }
  if (strlen(param) < 1) {
    LOCK_APP(LockOutput);
    printf("\033[31m%s\033[00m: %s\n", app->Strs[1], app->Strs[30]);
    UNLOCK_APP(LockOutput);
    return;
  }
  /* recupération de l'id serveur */
  LOCK_KEY(key);
  key->Count = 0;
  if (type == 'S') {
    key->Count = atoi(param);
    if (key->Count < 1 || key->Count > APP_LIMIT_USHORT) {
      UNLOCK_KEY(key);
      PrintError(97, ERR_TYPE_SIMPLE, 0);
      return;
    }
    for (; (*param >= '0') && (*param <= '9'); ++param);
    if (*param != ':') {
      UNLOCK_KEY(key);
      PrintError(47, ERR_TYPE_SIMPLE, 0);
      return;
    }
    ++param;
  }
  /* Assignation de la touche */
  switch (work) {
  case '0': key->Flags = 0; break;
  case 'H': key->Flags = KEY_FLG_HOLD_IT; break;
  case 'K': key->Flags = KEY_FLG_KEEP; break;
  case 'L': key->Flags = KEY_FLG_LOOP; break;
  case 'P': key->Flags = KEY_FLG_PAUSE; break;
  case 'S': key->Flags = KEY_FLG_SINGLE; break;
  }
  switch (dep) {
  case 'L': key->Flags |= KEY_FLG_LINE_STOP; break;
  case 'C': key->Flags |= KEY_FLG_COL_STOP; break;
  case 'B': key->Flags |= (KEY_FLG_LINE_STOP | KEY_FLG_COL_STOP); break;
  }
  key->CmdLen = strlen(param);
  if ((key->CmdFile = strdup(param)) == NULL) {
    UNLOCK_KEY(key);
    PrintError(9, ERR_TYPE_ERRNO, 0);
    ResetKey(key);
    return;
  }
  key->PushedColor = STAT_PUSH_SUCCESS;
  if (type == 'A') {
    key->Type = KEY_TYPE_AUDIO;
    if (!(key->Flags & (KEY_FLG_HOLD_IT | KEY_FLG_KEEP | KEY_FLG_LOOP | KEY_FLG_PAUSE | KEY_FLG_SINGLE))) {
      PrintError(152, ERR_TYPE_SIMPLE, 0);
      key->Flags |= KEY_FLG_KEEP;
    }
    GetAudioFile(key);
  }
  else {
    if (type == 'E') {
      key->Type = KEY_TYPE_EXEC;
      if (key->Flags & KEY_FLG_PAUSE) {
	PrintError(152, ERR_TYPE_SIMPLE, 0);
	key->Flags &= ~(KEY_FLG_PAUSE);
	key->Flags |= KEY_FLG_KEEP;
      }
      if (!(key->CmdTab = LineToTab(key->CmdFile))) {
	PrintError(9, ERR_TYPE_ERRNO, 0);
	ResetKey(key);
	UNLOCK_KEY(key);
	return;
      }
    }
    else {
      key->Type = KEY_TYPE_SERVER;
      key->Flags = KEY_FLG_KEEP;
    }
    key->DefColor = STAT_IDLE_CMD;
    UpdateKey(key);
  }
  key->Tempo = tempo;
  UNLOCK_KEY(key);
}

/* commande 'close' */
void               CmdClose(char *param)
{
  unsigned char    cmd[3];
  t_lpd            *lpd;

  lpd = app->Launchpad[app->CurrLpd];
  if (param && (param != (void *)1) && (*param == '?')) {
    CmdHelp("close");
    return;
  }
  LOCK_LPD(lpd);
  if (lpd->Fd > 0 && !param) {
    cmd[0] = LP_MENU;
    cmd[1] = 0;
    cmd[2] = 0;
    write(lpd->Fd, cmd, 3);
  }
  if (lpd->Fd > 1)
    close(lpd->Fd);
  lpd->Fd = 0;
  UNLOCK_LPD(lpd);
}

/* commande 'load' */
static void      CmdLoad(char *file)
{
  t_lpd          *lpd;

  lpd = app->Launchpad[app->CurrLpd];
  if (!file || (*file == '?')) {
    CmdHelp("load");
    return;
  }
  LoadConfigFile(file);
  ConnectToAllServers();
  if (lpd->CurrGrid < GRID_VOLUME)
    UpdateGrid(lpd, lpd->CurrGrid, lpd->CurrGrid);
}

/* Réinitialise les ActualColor de toutes les structures t_key */
void                ResetGridColor(t_lpd *lpd)
{
  unsigned char     a, b;
  t_key             *key;

  for (a = 0; a < 12; ++a)
    for (b = 0; b < 72; ++b) {
      key = &lpd->Grid[a][b];
      LOCK_KEY(key);
      key->ActualColor = STAT_OFF;
      UNLOCK_KEY(key);
    }
}

/* commande 'open' */
static void      CmdOpen(char *dev)
{
  t_lpd          *lpd;

  lpd = app->Launchpad[app->CurrLpd];
  if (!dev || (*dev == '?')) {
    CmdHelp("open");
    return;
  }
  if (lpd->Fd) {
    CmdClose(0);
    sleep(1);
  }
  if (OpenDevice(lpd, dev)) {
    ResetGridColor(lpd);
    ChangeGrid(lpd, 0);
    LightMenuMode(lpd, LP_TOP_SESSION);
    LightMenuArrows(lpd, 1);
    LightGridDefColor(lpd, lpd->Grid[lpd->CurrGrid]);
  }
}

/* commande raccourci 's' */
static void      CmdShortStop(char *param)
{
  t_key          *key;

  ++param;
  if (!GetKeyFromParam(app->Launchpad[app->CurrLpd], param, &key, 1))
    return;
  LOCK_KEY(key);
  KillRunningOne(key);
  UNLOCK_KEY(key);
}

/* commande 'stop' */
static void      CmdStop(char *param)
{
  t_key          *key;

  if (!param || (*param == '?')) {
    CmdHelp("stop");
    return;
  }
  if (!GetKeyFromParam(app->Launchpad[app->CurrLpd], param, &key, 1))
    return;
  LOCK_KEY(key);
  KillRunningOne(key);
  UNLOCK_KEY(key);
}

/* commande 'slin' */
static void      CmdStopLine(char *param)
{
  t_key          *key, *tmp, *grid;
  unsigned char  start, a;
  t_lpd          *lpd;

  lpd = app->Launchpad[app->CurrLpd];
  if (!param || (*param == '?')) {
    CmdHelp("slin");
    return;
  }
  if (param[0] < '1' || param[0] > '8') {
    LOCK_APP(LockOutput);
    printf("\033[31m%s\033[00m: slin: %s\n", app->Strs[1], app->Strs[35]);
    UNLOCK_APP(LockOutput);
    return;
  }
  if (lpd->CurrGrid > GRID_USER2)
    grid = lpd->Grid[lpd->LastGrid];
  else
    grid = lpd->Grid[lpd->CurrGrid];
  key = &grid[(param[0] - '0' - 1) * 9 + 8];
  start = (key->Id  / 16) * 9 + (key->Id % 16) - 8;
  for (a = 0; a < 8; ++a) {
    tmp = &grid[start + a];
    LOCK_KEY(tmp);
    KillRunningOne(tmp);
    UNLOCK_KEY(tmp);
  }
}

/* Affiche le contenu d'un buffer */
void      PrintBuffer(char *buff, int len, unsigned char max)
{
  unsigned char  a;

  for (a = 0; a < max && a < len; ++a)
    if (isprint(buff[a]))
      printf("%c", buff[a]);
    else
      printf("[%d]", buff[a]);
  if (a != len)
    printf("...");
}

/* commande 'list' */
static void      CmdList(char *param)
{
  unsigned short c;
  unsigned char  b, flg = 0, show = 0;
  t_key          *key;
  t_svr          *svr;
  int            a, i, nb = -1;
  t_lpd          *lpd;

  lpd = app->Launchpad[app->CurrLpd];
  if (param) {
    if (*param == '?') {
      CmdHelp("list");
      return;
    }
    while (*param) {
      if (*param >= '0' && *param <= '8') {
	nb = atoi(param);
	if (nb < 0 || nb > 8) {
	  PrintError(16, ERR_TYPE_SIMPLE, 0);
	  return;
	}
      }
      else if (*param == 'A')
	nb = GRID_USER1;
      else if (*param == 'B')
	nb = GRID_USER2;
      else if (*param == '?') {
	CmdHelp("list 1");
	return;
      }
      else if (!strncmp(param, "key", 3))
	show = 1;
      else if (!strncmp(param, "server", 6))
	show = 2;
      else {
	printf("\033[31m%s\033[00m: %s\nlist %s\n", app->Strs[1], app->Strs[107], app->Strs[64]);
	return;
      }
      for (; *param && (*param != ' ') && (*param != '\t'); ++param);
      for (; *param && (*param == ' ' || *param == '\t'); ++param);
    }
  }
  LOCK_APP(LockOutput);
  if (show < 2)
    for (a = 0; a < GRID_VOLUME; ++a)  {
      if (nb != -1)
	a = nb;
      for (b = 0; b < 72; ++b) {
	key = &lpd->Grid[a][b];
	if (!(key->Flags & KEY_FLG_NOT_INIT)) {
	  if (a >= GRID_USER1)
	    printf("%s %c", app->Strs[65], 'A' + (a == GRID_USER2));
	  else
	    printf("%s %d", app->Strs[65], a);
	  printf(": (%d,%d) : ", b % 9 + 1, b / 9 + 1);
	  if (key->CmdFile) {
	    if (key->Type == KEY_TYPE_SERVER) {
	      for (c = 0; c < app->MaxServers; ++c)
		if (app->Servers[c] && (app->Servers[c]->Uid == key->Count))
		  break;
	      if (!app->Servers || c == app->MaxServers)
		printf(" %zd %s (%s) (%d: %s) ", key->CmdLen, app->Strs[99], key->CmdFile, key->Count, app->Strs[98]);
	      else {
		printf("(%s:%s) : %zd %s (", app->Servers[c]->Host, app->Servers[c]->Port, key->CmdLen, app->Strs[99]);
		PrintBuffer(key->CmdFile, key->CmdLen, 10);
		printf(") ");
	      }
	    }
	    else {
	      if (key->Flags & KEY_FLG_KEEP)
		printf("K,");
	      else if (key->Flags & KEY_FLG_SINGLE)
		printf("S,");
	      else if (key->Flags & KEY_FLG_LOOP)
		printf("L,");
	      else if (key->Flags & KEY_FLG_HOLD_IT)
		printf("H,");
	      else if (key->Flags & KEY_FLG_PAUSE)
		printf("P,");
	      else if (key->Flags & (KEY_FLG_COL_STOP | KEY_FLG_LINE_STOP))
		printf("0,");

	      if ((key->Flags & KEY_FLG_COL_STOP) && (key->Flags & KEY_FLG_LINE_STOP))
		printf("C+L");
	      else if (key->Flags & KEY_FLG_COL_STOP)
		printf("C");
	      else if (key->Flags & KEY_FLG_LINE_STOP)
		printf("L");
	      else
		printf("\b");
	    }
	    if (key->Link)
	      printf(" : lnk %d,%d", key->Link->Id % 16 + 1, key->Link->Id / 16 + 1);
	    
	    printf(" :");
	    for (i = 0; key->CmdTab[i]; ++i)
	      printf(" %s", key->CmdTab[i]);

	    printf("\n");
	  }
	  else 
	    printf("--\n");
	  flg = 1;
	}
      }
      if (nb != -1)
	break;
    }
  if (!flg && show != 2)
    printf("%s\n", app->Strs[66]);
  if (!show || show == 2)
    for (c = 0; c < app->MaxServers; ++c) {
      svr = app->Servers[c];
      if (svr) {
	if (svr->Buff) {
	  printf("%s %d: %s:%s: %d %s (", app->Strs[105], svr->Uid, svr->Host, svr->Port, svr->Size, app->Strs[99]);
	  PrintBuffer(svr->Buff, svr->Size, 10);
	  printf(")\n");
	}
	else
	  printf("%s %d: %s:%s (%s)\n", app->Strs[105], svr->Uid, svr->Host, svr->Port, app->Strs[106]);
      }
    }
  UNLOCK_APP(LockOutput);
}

/* commande 'vol' */
static void      CmdVol(char *param)
{
  int            col;
  float          vol;

  if (!param || (*param == '?')) {
    CmdHelp("vol");
    return;
  }
  col = atoi(param);
  if (col < 1 || col > 8) {
    PrintError(18, ERR_TYPE_SIMPLE, 0);
    return;
  }
  for (; *param && (*param != ' ') && (*param != '\t'); ++param);
  for (; *param && (*param == ' ' || *param == '\t'); ++param);
  if (!(*param) || (*param) == '?') {
    CmdHelp("vol 1");
    return;
  }
  if (app->Strs == FrenchTab)
    FrenchFloat(param);
  vol = atof(param);
  if (vol < 0.000 || vol > 1.000) {
    PrintError(69, ERR_TYPE_SIMPLE, 0);
    return;
  }
  ChangeVolume(app->Launchpad[app->CurrLpd], col - 1, vol);
  UpdateVolumeColumn(app->Launchpad[app->CurrLpd], col - 1, 0);
}

/* commande 'mute' */
static void      CmdMute(char *param)
{
  int            col;
  t_lpd          *lpd;

  lpd = app->Launchpad[app->CurrLpd];
  if (param) {
    if (*param == '?') {
      CmdHelp("mute");
      return;
    }
    col = atoi(param) - 1;
    if (col < 0 || col > 7) {
      LOCK_APP(LockOutput);
      printf("\033[31m%s\033[00m: mute: %s\n", app->Strs[1], app->Strs[18]);
      UNLOCK_APP(LockOutput);
      return;
    }
    ChangeVolume(lpd, col, 0.0);
    UpdateVolumeColumn(lpd, (LP_8X1 + col) % 16, 0);
  }
  else {
    for (col = 0; col < 8; ++col) {
      ChangeVolume(lpd, col, 0.0);
      UpdateVolumeColumn(lpd, (LP_8X1 + col) % 16, 0);
    }
  }
}

/* commande 'umute' */
static void      CmdUmute(char *param)
{
  int            col;
  t_lpd          *lpd;

  lpd = app->Launchpad[app->CurrLpd];  
  if (param) {
    if (*param == '?') {
      CmdHelp("umute");
      return;
    }
    col = atoi(param) - 1;
    if (col < 0 || col > 7) {
      LOCK_APP(LockOutput);
      printf("\033[31m%s\033[00m: umute: %s\n", app->Strs[1], app->Strs[18]);
      UNLOCK_APP(LockOutput);
      return;
    }
    ChangeVolume(lpd, col, lpd->Mixer.Mute[col]);
    UpdateVolumeColumn(lpd, (LP_8X1 + col) % 16, 0);
  }
  else {
    for (col = 0; col < 8; ++col) {
      ChangeVolume(lpd, col, lpd->Mixer.Mute[col]);
      UpdateVolumeColumn(lpd, (LP_8X1 + col) % 16, 0);
    }
  }
}

/* commande 'rlin' */
static void      CmdRunLine(char *param)
{
  t_key          *key;
  t_lpd          *lpd;

  lpd = app->Launchpad[app->CurrLpd];  
  if (!param || (*param == '?')) {
    CmdHelp("rlin");
    return;
  }
  if (param[0] < '1' || param[0] > '8') {
    LOCK_APP(LockOutput);
    printf("\033[31m%s\033[00m: rlin: %s\n", app->Strs[1], app->Strs[35]);
    UNLOCK_APP(LockOutput);
    return;
  }
  if (lpd->CurrGrid > GRID_USER2)
    key = &lpd->Grid[lpd->LastGrid][(param[0] - '0' - 1) * 9 + 8];
  else
    key = &lpd->Grid[lpd->CurrGrid][(param[0] - '0' - 1) * 9 + 8];
  LOCK_KEY(key);
  PushedSideKeys(key);
  ReleaseSideKeys(key);
  UNLOCK_KEY(key);
}

/* commande 'sleep' */
static void      CmdSleep(char *param)
{
  int            time;

  if (!param || (*param == '?')) {
    CmdHelp("sleep");
    return;
  }
  time = atoi(param);
  if (time < 0) {
    LOCK_APP(LockOutput);
    printf("\033[31m%s\033[00m: sleep: %s\n", app->Strs[1], app->Strs[81]);
    UNLOCK_APP(LockOutput);
    return;
  }
  LOCK_APP(LockOutput);
  printf("%s %d.%d %s\n", app->Strs[82], time / 10, time % 10, app->Strs[83]);
  UNLOCK_APP(LockOutput);
  if (time / 10)
    sleep(time / 10);
  if (time % 10)
    usleep((time % 10) * 100000);
}

/* commande 'disconnect' */
static void      CmdDisconnect(char *param)
{
  t_svr          *svr;
  int            id;

  if (!param || (*param == '?')) {
    CmdHelp("disconnect");
    return;
  }
  id = atoi(param);
  if (id < 0 || id > APP_LIMIT_USHORT) {
    LOCK_APP(LockOutput);
    printf("=> %s (0 < id < %d)\n", app->Strs[97], APP_LIMIT_USHORT);
    UNLOCK_APP(LockOutput);
    return;
  }
  if (!(svr = CheckExistServerId(id))) {
    PrintError(97, ERR_TYPE_SIMPLE, 0);
    return;
  }
  LOCK_MUTEX(svr->Lock);
  if (svr->Socket) {
    close(svr->Socket);
    svr->Socket = 0;
    UpdateRelatedKeys(svr);
  }
  UNLOCK_MUTEX(svr->Lock);
  FreeServerStruct(svr);
}

/* action de 'run' et 'r' */
static void      CmdRunIt(t_key *key)
{
  unsigned char  cmd[3];

  LOCK_KEY(key);
  cmd[0] = LP_GRID;
  cmd[1] = key->Id;
  cmd[2] = PushedKey(key);
  if (!(key->Flags & KEY_FLG_HOLD_IT))
    cmd[2] = ReleaseKey(key);
  else
    key->Flags &= ~(KEY_FLG_PUSHED);
  key->ActualColor = cmd[2];
  SendToDevice(key->lpd, cmd, 3);
  UNLOCK_KEY(key);
}

/* commande 'r' */
static void      CmdShortRun(char *param)
{
  t_key          *key;

  ++param;
  if (!GetKeyFromParam(app->Launchpad[app->CurrLpd], param, &key, 1))
    return;
  CmdRunIt(key);
}

/* Copie une touche */
static void      CopyKey(const t_key *src, t_key *dst)
{
  dst->Audio = src->Audio;
  dst->CmdFile = src->CmdFile;
  dst->CmdLen = src->CmdLen;
  dst->CmdTab = src->CmdTab;
  dst->Count = src->Count;
  dst->DefColor = src->DefColor;
  dst->Flags = src->Flags;
  dst->Link =  src->Link;
  dst->PushedColor = src->PushedColor;
  dst->Tempo = src->Tempo;
  dst->Type =  src->Type;
}

/* commande 'copy' */
static void      CmdCopy(char *param)
{
  t_key          *key1, *key2;

  if (!param || (*param == '?')) {
    CmdHelp("copy");
    return;
  }
  if (!(param = GetKeyFromParam(app->Launchpad[app->CurrLpd], param, &key1, 1)))
    return;
  for (; *param && (*param == ' ' || *param == '\t'); ++param);
  if (!(*param)) {
    CmdHelp("copy");
    return;
  }
  if (!GetKeyFromParam(app->Launchpad[app->CurrLpd], param, &key2, 1))
    return;
  if (key1->Id == key2->Id) {
    PrintError(162, ERR_TYPE_SIMPLE, 0);
    return;
  }
  if ((key1->Flags & (KEY_FLG_PUSHED | KEY_FLG_IN_USE)) ||
      (key2->Flags & (KEY_FLG_PUSHED | KEY_FLG_IN_USE))) {
    PrintError(136, ERR_TYPE_SIMPLE, 0);
    return;
  }
  LOCK_KEY(key2);
  CopyKey(key1, key2);
  UNLOCK_KEY(key2);
}

/* commande 'move' */
static void      CmdMove(char *param)
{
  t_key          *key1, *key2, tmp;

  memset(&tmp, 0, sizeof(t_key));
  if (!param || (*param == '?')) {
    CmdHelp("move");
    return;
  }
  if (!(param = GetKeyFromParam(app->Launchpad[app->CurrLpd], param, &key1, 1)))
    return;
  for (; *param && (*param == ' ' || *param == '\t'); ++param);
  if (!(*param)) {
    CmdHelp("move");
    return;
  }
  if (!GetKeyFromParam(app->Launchpad[app->CurrLpd], param, &key2, 1))
    return;
  if (key1->Id == key2->Id) {
    PrintError(162, ERR_TYPE_SIMPLE, 0);
    return;
  }
  LOCK_KEY(key1);
  if (key1->Flags & (KEY_FLG_PUSHED | KEY_FLG_IN_USE)) {
    UNLOCK_KEY(key1);
    PrintError(136, ERR_TYPE_SIMPLE, 0);
    return;
  }
  key1->Flags |= KEY_FLG_LOCK;
  UNLOCK_KEY(key1);
  LOCK_KEY(key2);
  CopyKey(key2, &tmp);
  CopyKey(key1, key2);
  UNLOCK_KEY(key2);
  LOCK_KEY(key1);
  CopyKey(&tmp, key1);
  key1->Flags &= ~(KEY_FLG_LOCK);
  UNLOCK_KEY(key1);
  UpdateTwoKey(key1->lpd, key1, key2);
}

/* commande 'run' */
static void      CmdRun(char *param)
{
  t_key          *key;

  if (!param || (*param == '?')) {
    CmdHelp("run");
    return;
  }
  if (!GetKeyFromParam(app->Launchpad[app->CurrLpd], param, &key, 1))
    return;
  CmdRunIt(key);
}

/* commande 'pad' */
static void      CmdPad(char *param)
{
  int            nb;

  if (!param) {
    CmdHelp("pad");
    return;
  }
  if (app->NbLaunchpad < 2) {
    PrintError(145, ERR_TYPE_SIMPLE, 0);
    return;
  }
  if (!strncmp(param, "show", 4)) {
    //# affichage du N° du launchpad sur le launchpad
    sleep(2);
    return;
  }
  nb = atoi(param);
  if (nb < 1 || nb > app->NbLaunchpad) {
    PrintError(144, ERR_TYPE_SIMPLE, 0);
    return;
  }
  app->CurrLpd = nb - 1;
  PrintIt(1);
}

/* commande 'cd' */
static void      CmdCd(char *param)
{
  if (!param) {
    if (!(param = getenv("HOME"))) {
      PrintError(92, ERR_TYPE_SIMPLE, 0);
      return;
    }
  }
  if (chdir(param))
    PrintError(91, ERR_TYPE_ERRNO, 0);
}

/* Choses a faire après une connexion a un serveur */
static void         AfterConnecting(t_svr *svr)
{
  /* Envoi des données au serveur */
  LOCK_MUTEX(svr->Lock);
  if (svr->Socket && svr->Size)
    if (send(svr->Socket, svr->Buff, svr->Size, 0) < svr->Size)
      PrintError(101, ERR_TYPE_ERRNO, 0);
  UNLOCK_MUTEX(svr->Lock);
  /* Lancement du thread de surveillance des connexions serveurs */
  LOCK_APP(LockPonctual);
  if (!app->IdServer) {
    if (pthread_create(&app->IdServer, &app->Attr, ThreadSurveyServer, 0))
      PrintError(7, ERR_TYPE_ERRNO, 0);
    else
      pthread_detach(app->IdServer);
  }
  UNLOCK_APP(LockPonctual);
  /* Mise à jour des touches */
  UpdateRelatedKeys(svr);  
}

/* commande 'connect' */
static void      CmdConnect(char *param)
{
  char           *host, *port, *data = 0;
  int            id;
  t_svr          *tmp = 0, *svr;

  if (!param || (*param == '?'))
    goto end_CmdConnect;
  /* Id */
  id = atoi(param);
  if (id < 0 || id > APP_LIMIT_USHORT) {
    LOCK_APP(LockOutput);
    printf("=> %s (0 < id < %d)\n", app->Strs[110], APP_LIMIT_USHORT);
    UNLOCK_APP(LockOutput);
    return;
  }
  for (; *param && (*param != ' ') && (*param != '\t'); ++param);
  for (; *param && (*param == ' ' || *param == '\t'); ++param);
  /* Nom d'hôte */
  host = param;
  for (; *param && (*param != ' ') && (*param != '\t'); ++param);
  if (!(*param)) {
    if ((svr = CheckExistServerId(id))) {
      if (!svr->Socket) {
	LOCK_MUTEX(svr->Lock);
	svr->Socket = ConnectToServer(svr->Host, svr->Port);
	UNLOCK_MUTEX(svr->Lock);
	AfterConnecting(svr);
      }
      else
	PrintError(111, ERR_TYPE_SIMPLE, 0);
      return;
    }
    if (!(*host))
      PrintError(158, ERR_TYPE_SIMPLE, 0);
    else if (*host == '?')
      CmdHelp("connect 1");
    else
      CmdHelp("connect");
    return;
  }
  *param = '\0';
  /* Port */
  for (++param; *param && (*param == ' ' || *param == '\t'); ++param);
  if (!(*param))
    goto end_CmdConnect;
  if (*param == '?') {
    CmdHelp("connect 1");
    return;
  }
  port = param;
  for (; *param && (*param != ' ') && (*param != '\t'); ++param);
  if (*param) {
    *param = '\0';
    /* Données */
    for (++param; *param && (*param == ' ' || *param == '\t'); ++param);
    if (*param) {
      if (*param == '?') {
	CmdHelp("connect 2");
	return;
      }
      data = param;
    }
  }
  /* Création de la structure relatif au serveur */  
  if ((tmp = calloc(1, sizeof(t_svr))) == NULL)
    goto end_CmdConnect_nomem;
  if ((tmp->Host = strdup(host)) == NULL)
    goto end_CmdConnect_nomem;
  if ((tmp->Port = strdup(port)) == NULL)
    goto end_CmdConnect_nomem;
  if (data) {
    if ((tmp->Buff = strdup(data)) == NULL)
      goto end_CmdConnect_nomem;
    tmp->Size = strlen(data);
  }
  tmp->Uid = id;
  /* Vérification de l'existance ou non du serveur */
  if ((svr = CheckExistServer(tmp))) {
    if (svr->Buff)
      free(svr->Buff);
    svr->Buff = tmp->Buff;
    svr->Size = tmp->Size;
    tmp->Buff = 0;
    FreeServerStruct(tmp);
    if (!svr->Socket)
      svr->Socket = ConnectToServer(host, port);
    else {
      PrintError(111, ERR_TYPE_SIMPLE, 0);
      return;
    }
  }
  else {
    pthread_mutex_init(&tmp->Lock, NULL);
    /* Connexion, ajout de la structure et envoi des données */
    tmp->Socket = ConnectToServer(host, port);
    if (AddServerStruct(tmp))
      goto end_CmdConnect_nomem;
    svr = tmp;
  }
  AfterConnecting(svr);
  return;
 end_CmdConnect_nomem:
  if (tmp)
    FreeServerStruct(tmp);
  PrintError(9, ERR_TYPE_ERRNO, 0);
  return;
 end_CmdConnect:
  CmdHelp("connect");
}

/* Tableau de commande */
static t_scd    CmdTab[] = {
  {"?", CmdHelp, 1}
  , {"add", CmdAdd, 3}
  , {"cd", CmdCd, 2}
  , {"close", CmdClose, 5}
  , {"connect", CmdConnect, 7}
  , {"copy", CmdCopy, 4}
  , {"del", CmdDel, 3}
  , {"disconnect", CmdDisconnect, 10}
  , {"exit", TYPE_FUNC_CMD QuitApp, 4}
  , {"grid", CmdGrid, 4}
  , {"help", CmdHelp, 4}
  , {"link", CmdLink, 4}
  , {"list", CmdList, 4}
  , {"load", CmdLoad, 4}
  , {"mode", CmdMode, 4}
  , {"move", CmdMove, 4}
  , {"mute", CmdMute, 4}
  , {"pad", CmdPad, 3}
  , {"open", CmdOpen, 4}
  , {"rlin", CmdRunLine, 4}
  , {"run", CmdRun, 3}
  , {"r", CmdShortRun, 1}
  , {"save", CmdSave, 4}
  , {"set", CmdSet, 3}
  , {"sleep", CmdSleep, 5}
  , {"slin", CmdStopLine, 4}
  , {"stop", CmdStop, 4}
  , {"s", CmdShortStop, 1}
  , {"umute", CmdUmute, 5}
  , {"vol", CmdVol, 3}
};
#define SIZE_TAB_CMD (sizeof(CmdTab) / sizeof(t_scd))

/* FREE - Converti la ligne en tableau de commande */
char              **CommandeLineToTab(char *line)
{
  char            **tab;
  unsigned int    a, last, nb;

  /* Comptage des commands */
  for (nb = 1, a = 0; line[a]; ++a)
    if (line[a] == ';')
      ++nb;
  if ((tab = malloc((nb + 1) * sizeof(char *))) == NULL) {
    PrintError(9, ERR_TYPE_ERRNO, 0);
    return (0);
  }
  /* Transformation de la ligne en tableau */
  for (last = nb = a = 0; line[a]; ++a)
    if (line[a] == ';') {
      tab[nb] = &line[last];
      last = a + 1;
      line[a] = 0;
      ++nb;
    }
  tab[nb] = &line[last];
  tab[nb + 1] = 0;
  return (tab);
}

/* Execute la line de commande avec "sh -c" */
void               ExecuteToSH(char *cmd)
{
  pid_t           pid;
  int             status;

  if ((pid = fork()) == -1) {
    PrintError(12, ERR_TYPE_ERRNO, 0);
    return;
  }
  else if (!pid) {
    char     *tab[4];
    
    tab[0] = "/bin/sh";
    tab[1] = "-c";
    tab[2] = cmd;
    tab[3] = 0;
    execv(tab[0], tab);
    ExitWithMsg(app->Strs[12]);
  }
  waitpid(pid, &status, 0);
}

/* THREAD - Gestion des commandes shell */
void               *ManageShell(void *param)
{
  char             *buff, *line, **tab, *tild;
  unsigned char    a, b, l, cmd;
  struct termios   w;
  
  tcgetattr(0, &w);
  w.c_cc[VMIN] = 0;
  w.c_cc[VTIME] = 0;
  w.c_lflag &= ~(ICANON | ECHO);
  tcsetattr(0, TCSANOW, &w);
  while (1) {
    LOCK_APP(LockOutput);
    PrintIt(0);
    fflush(stdout);
    UNLOCK_APP(LockOutput);
    LOCK_APP(LockFlg);
    app->Flags &= ~(APP_FLG_SHELL_CMD);
    UNLOCK_APP(LockFlg);
    if (!(buff = GetEntry())) {
      PrintError(60, ERR_TYPE_ERRNO, 0);
      sleep(1);
    }
    else {
      if ((tab = CommandeLineToTab(buff))) {
	for (cmd = 0; cmd < 250 && tab[cmd]; ++cmd) {
	  line = tab[cmd];
	  if ((tild = InsertHomeToString(line))) {
	    line = tild;
	    //	    printf("tild : %s\n", tild);
	  }
	  for (b = 0; line[b] && (line[b] == ' ' || line[b] == '\t'); ++b);
	  if (line[b]) {
	    for (l = b, a = 0; a < SIZE_TAB_CMD; ++a) {
	      if (!strncasecmp(CmdTab[a].Cmd, &line[b], CmdTab[a].Len)) {
		for (; line[b] && (line[b] != ' ') && (line[b] != '\t'); ++b);
		for (; line[b] && (line[b] == ' ' || line[b] == '\t'); ++b);
		LOCK_APP(LockFlg);
		app->Flags |= APP_FLG_SHELL_CMD;
		UNLOCK_APP(LockFlg);
		if (CmdTab[a].Len == 1 && line[l] != '?') {
		  b = l;
		  if (line[b + 1] < '0' || line[b + 1] > '9') {
		    a = SIZE_TAB_CMD;
		    break;
		  }
		}
		if (!line[b])
		  CmdTab[a].Func(0);
		else
		  CmdTab[a].Func(&line[b]);
		break;
	      }
	    }
	    if (a == SIZE_TAB_CMD)
	      ExecuteToSH(&line[b]);
	  }
	  if (tild) {
	    free(tild);
	    tild = 0;
	  }
	} /* for */
	free(tab);
      } /* if */
      free(buff);
    } /* else */
  } /* while */
  return (0);
}
