/*
 *  'launchpadctrl' allows you to use your Novation launchpad (NVLPD01)
 *  Copyright (C) 2014-2017  Vincent Deca. known as Bigcake@ubuntu-fr.org
 *  (user@forum not email)
 *
 *  vincent.deca@neutralite.org
 *
 *  This file is part of 'launchpadctrl'.
 *
 * 'launchpadctrl' is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  'launchpadctrl' is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with 'launchpadctrl'.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app.h"

/* Affichage de l'erreur */
static void                PrintFileError(const unsigned char err, const char *file, const int nb)
{
  LOCK_APP(LockOutput);
  printf("\r\033[31m%s\033[00m: %s: %d: %s \n", app->Strs[1], file, nb, app->Strs[err]);
  UNLOCK_APP(LockOutput);
  if (!(app->Flags & APP_FLG_SHELL_CMD))
    PrintLPEntry();
}

/* Vérification de la signature du fichier */
static char                CheckSig(char *sig)
{
  if (strncmp(sig, SIG_SAV_V1, sizeof(SIG_SAV_V1) - 1))
    return (1);
  return (0);
}

/* Préparation de la touche pour le fichier audio */
void                       GetAudioFile(t_key *key)
{
  t_af                     *af;

  af = LoadAudioFile(key->CmdFile);
  if (!af) {
    key->DefColor = STAT_IDLE_ERROR;
    key->PushedColor = STAT_PUSH_ERROR;
  }
  else {
    if (key->Audio)
      FreeAudioFile(key->Audio);
    key->Audio = af;
    key->DefColor = STAT_IDLE_AUDIO;
  }
  if (key->lpd->CurrGrid < GRID_VOLUME)
    if (key->DefColor != key->ActualColor)
      if (key->lpd->Fd > 1)
        UpdateKey(key);
}

/* Récupération d'un niveau sonore */
static unsigned char       RetrieveVolume(char *line, int pos, const char *file, const int nb)
{
  int                      col;
  float                    vol;
  t_lpd                    *lpd;

  col = atoi(&line[pos]) - 1;
  if (col < 0 || col > 7) {
    PrintFileError(18, file, nb);
    return (0);
  }
  ++pos;
  if (line[pos] != ':') {
    PrintFileError(47, file, nb);
    return (0);
  }
  ++pos;
  if (app->Strs == FrenchTab)
    FrenchFloat(&line[pos]);
  vol = atof(&line[pos]);
  if (vol < 0.0000 || vol > 1.0000) {
    PrintFileError(69, file, nb);
    return (0);
  }
  lpd = app->Launchpad[app->CurrLpd];
  lpd->Mixer.Vol[col] = vol;
  ChangeVolume(lpd, col, vol);
  UpdateVolumeColumn(lpd, col, 0);
  return (0);
}

/* Récupération des données pour une connexion a un serveur */
static unsigned char       RetrieveServer(char *line, int pos, const char *file, const int nb)
{
  t_svr                    *tmp;
  int                      last;

  if ((tmp = calloc(1, sizeof(t_svr))) == NULL)
    goto end_RetrieveServer;
  pthread_mutex_init(&tmp->Lock, NULL);
  /* Recuperation de l'uid */
  last = atoi(&line[pos]);
  if (last < 1 || last > APP_LIMIT_USHORT) {
    PrintFileError(96, file, nb);
    return (0);
  }
  tmp->Uid = last;
  for (; line[pos] >= '0' && line[pos] <= '9'; ++pos);
  /* Doit etre ':' */
  if (line[pos] != ':') {
    PrintFileError(47, file, nb);
    return (0);
  }
  /* Récupération du nom d'hote */
  for (last = pos + 1; line[last] == ' ' || line[last] == '\t'; ++last);
  for (pos = last; line[pos] && line[pos] != ':'; ++pos);
  if (!line[pos]) {
    PrintFileError(94, file, nb);
    return (0);
  } 
  line[pos] = '\0';
  if ((tmp->Host = strdup(&line[last])) == NULL)
    goto end_RetrieveServer;
  /* Récupération du port */
  for (last = pos + 1; line[last] == ' ' || line[last] == '\t'; ++last);
  for (pos = last; line[pos] && line[pos] != ':'; ++pos);
  if (!line[pos]) {
    PrintFileError(94, file, nb);
    return (0);
  }
  line[pos] = '\0';
  if ((tmp->Port = strdup(&line[last])) == NULL)
    goto end_RetrieveServer;
  /* Récupération des eventuels données a envoyer au serveur */
  ++pos;
  if (line[pos]) {
    for (last = pos; line[pos]; ++pos);
    if (line[pos - 1] == '\n') {
      --pos;
      line[pos] = '\0';
    }
    tmp->Size = pos - last;
    if (tmp->Size) {
      if ((tmp->Buff = malloc(tmp->Size)) == NULL)
	goto end_RetrieveServer;
      memcpy(tmp->Buff, &line[last], tmp->Size);
    }
  }
  if (CheckExistServer(tmp)) {
    LOCK_APP(LockOutput);
    printf("\r\033[31m%s\033[00m: %s: %d: '%s:%s': %s \n", app->Strs[1], file, nb, tmp->Host, tmp->Port, app->Strs[148]);
    UNLOCK_APP(LockOutput);
    if (!(app->Flags & APP_FLG_SHELL_CMD))
      PrintLPEntry();
    FreeServerStruct(tmp);
    return (0);
  }
  if (AddServerStruct(tmp))
    goto end_RetrieveServer;
  return (0);
 end_RetrieveServer:
  if (tmp)
    FreeServerStruct(tmp);
  PrintError(9, ERR_TYPE_ERRNO, 0);
  return (1);
}

/* Recupération de la structure correspondant a la touche */
static t_key     *GetKey(char *line)
{
  t_key          *key;
  int            col, lin;
  t_lpd            *lpd;

  lpd = app->Launchpad[app->CurrLpd];
  /* position dans la ligne */
  if (*line > '0' && *line < '9')
    lin = *line - '1';
  else
    return (0);
  /* position dans la colonne */
  ++line;
  if (*line > '0' && *line < '9')
    col = *line - '1';
  else
    return (0);
  key = &lpd->Grid[lpd->CurrGrid][lin + col * 9];
  return (key);
}

/* Récupération des liens entre touches */
static void       RetrieveLinks(char *line, int pos, const char *file, const int nb)
{
  t_key           *key1, *key2;
  
  if (!(key1 = GetKey(&line[pos])))
    PrintFileError(96, file, nb);
  if (key1->Flags & KEY_FLG_NOT_INIT) {
    LOCK_APP(LockOutput);
    printf("\r%s: %s: %d: %s \n", app->Strs[187], file, nb, app->Strs[188]);
    UNLOCK_APP(LockOutput);
    if (!(app->Flags & APP_FLG_SHELL_CMD))
      PrintLPEntry();
  }
  pos += 2;
  if (line[pos] != ':') {
    PrintFileError(47, file, nb);
    return;
  }
  ++pos;
  if (!(key2 = GetKey(&line[pos])))
    PrintFileError(96, file, nb);
  if (key2->Flags & KEY_FLG_NOT_INIT) {
    LOCK_APP(LockOutput);
    printf("\r%s: %s: %d: %s \n", app->Strs[187], file, nb, app->Strs[188]);
    UNLOCK_APP(LockOutput);
    if (!(app->Flags & APP_FLG_SHELL_CMD))
      PrintLPEntry();
  }
  pos += 2;
  switch (line[pos]) {
  case 'L':
    if ((key1->Type == KEY_TYPE_SERVER) && (key2->Type == KEY_TYPE_SERVER)) {
      PrintFileError(190, file, nb);
      return; 
    }
    LOCK_KEY(key1);
    key1->Flags |= KEY_FLG_LNK_LOOP;
    UNLOCK_KEY(key1);
    LOCK_KEY(key2);
    key2->Flags |= KEY_FLG_LNK_LOOP;
    UNLOCK_KEY(key2);
    break;
  case 'S':
    LOCK_KEY(key1);
    key1->Flags |= KEY_FLG_LNK_SIMUL;
    UNLOCK_KEY(key1);
    LOCK_KEY(key2);
    key2->Flags |= KEY_FLG_LNK_SIMUL;
    UNLOCK_KEY(key2);
    break;
  case '0': 
    if ((key1->Type == KEY_TYPE_SERVER) && (key2->Type == KEY_TYPE_SERVER)) {
      PrintFileError(190, file, nb);
      return; 
    }
    break;
  default: PrintFileError(29, file, nb);
    return;
  }
  key1->Link = key2;
  key2->Link = key1;
}

/* Récupération des données pour les modes */
static void                RetrieveMode(char *line, int pos, const char *file, const int nb)
{
  t_lpd                    *lpd;

  lpd = app->Launchpad[app->CurrLpd];
  if (line[pos] != ':') {
    PrintFileError(47, file, nb);
    return;
  }
  ++pos;
  switch (line[pos]) {
  case 'Q': 
    close(2);
    if (open("/dev/null", O_RDWR) < 0)
      PrintError(155, ERR_TYPE_ERRNO, 0);
    else {
      LOCK_APP(LockFlg);
      app->Flags |= APP_FLG_QUIET;
      UNLOCK_APP(LockFlg);
    }
    break;
  case 'R': lpd->Flags |= LPD_FLG_RANDOM_USER; break;
  case 'T':
    if (!app->IdTempo) {
      if (pthread_create(&app->IdTempo, &app->Attr, ManageGlobalTempo, 0)) {
        PrintError(7, ERR_TYPE_SIMPLE, 0);
        return;
      }
      pthread_detach(app->IdTempo);
    }
    break;
  case 0: break;
  default: PrintFileError(96, file, nb);
  }
}

/* Récupération des données pour une assignation de touche */
static unsigned char       RetrieveKey(char *line, int pos, const char *file, const int nb)
{
  unsigned char            grid, lin, col, bpm;
  t_key                    *key;
  char                     *str;
  int                      a;

  /* Tempo */
  if (line[pos] != '0')
    bpm = KEY_TMO_1;
  else
    bpm = KEY_TMO_INSTANT;
  /* Grille */
  ++pos;
  if (line[pos] >= '0' && line[pos] <= '9')
    grid = line[pos] - '0';
  else if (line[pos] == 'A')
    grid = 9;
  else if (line[pos] == 'B')
    grid = 10;
  else {
    PrintFileError(15, file, nb);
    return (0);
  }
  /* position dans la ligne */
  ++pos;
  if (line[pos] >= '1' && line[pos] <= '8')
    lin = line[pos] - '1';
  else {
    PrintFileError(17, file, nb);
    return (0);
  }
  /* position dans la colonne */
  ++pos;
  if (line[pos] >= '1' && line[pos] <= '8')
    col = line[pos] - '1';
  else {
    PrintFileError(18, file, nb);
    return (0);
  }
  key = &app->Launchpad[app->CurrLpd]->Grid[grid][lin + col * 9];
  if (!(key->Flags & KEY_FLG_NOT_INIT)) {
    PrintFileError(58, file, nb);
    return (0);
  }
  LOCK_KEY(key);
  /* Mode de fonctionnement */
  ++pos;
  switch (line[pos]) {
  case '0': key->Flags = 0; break;
  case 'H': key->Flags = KEY_FLG_HOLD_IT; break;
  case 'K': key->Flags = KEY_FLG_KEEP; break;
  case 'L': key->Flags = KEY_FLG_LOOP; break;
  case 'P': key->Flags = KEY_FLG_PAUSE; break;
  case 'S': key->Flags = KEY_FLG_SINGLE; break;
  default:
    UNLOCK_KEY(key);
    PrintFileError(19, file, nb);
    return (0);
  }
  /* Mode de dépendance */
  ++pos;
  if (key->Flags != KEY_FLG_HOLD_IT)
    switch (line[pos]) {
    case '0': break;
    case 'B': key->Flags |= (KEY_FLG_COL_STOP | KEY_FLG_LINE_STOP); break;
    case 'C': key->Flags |= KEY_FLG_COL_STOP; break;
    case 'L': key->Flags |= KEY_FLG_LINE_STOP; break;
    default:
      a = 24;
      goto end_RetrieveKey;
    }
  /* executable / audio / server cmd */
  ++pos;
  if (line[pos] == 'A') {
    key->Type = KEY_TYPE_AUDIO;
    if (!(key->Flags & (KEY_FLG_HOLD_IT | KEY_FLG_KEEP | KEY_FLG_LOOP | KEY_FLG_PAUSE | KEY_FLG_SINGLE))) {
      PrintFileError(152, file, nb);
      key->Flags |= KEY_FLG_KEEP;
    }
  }
  else if (line[pos] == 'E') {
    key->Type = KEY_TYPE_EXEC;
    if (key->Flags & KEY_FLG_PAUSE) {
      PrintFileError(152, file, nb);
      key->Flags &= ~(KEY_FLG_PAUSE);
      key->Flags |= KEY_FLG_KEEP;
    }
  }
  else if (line[pos] == 'S') {
    key->Type = KEY_TYPE_SERVER;
    key->Flags = KEY_FLG_KEEP;
  }
  else {
    a = 29;
    goto end_RetrieveKey;
  }
  /* doit être ':' */
  ++pos;
  if (line[pos] != ':') {
    a = 47;
    goto end_RetrieveKey;
  }
  /* Récupération de l'uid du serveur */
  if (key->Type == KEY_TYPE_SERVER) {
    ++pos;
    a = atoi(&line[pos]);
    if (a < 1 || a > APP_LIMIT_USHORT) {
      a = 97;
      goto end_RetrieveKey;
    }
    key->Count = a;
    for (; line[pos] >= '0' && line[pos] < '9'; ++pos);
    if (line[pos] != ':') {
      a = 47;
      goto end_RetrieveKey;
    }
  }
  /* Assignation du fichier / de la commande */
  ++pos;
  str = &line[pos];
  for (a = 0; str[a]; ++a);
  if (str[a - 1] == '\n')
    str[a - 1] = '\0';
  if (!strlen(&line[pos])) {
    a = 100;
    goto end_RetrieveKey;
  }
  if ((str = strdup(&line[pos])) == NULL) {
    ResetKey(key);
    UNLOCK_KEY(key);
    PrintError(9, ERR_TYPE_ERRNO, 0);
    return (1);
  }
  if (key->CmdFile)
    free(key->CmdFile);
  if (key->CmdTab) {
    free(key->CmdTab);
    key->CmdTab = 0;
  }
  key->PushedColor = STAT_PUSH_SUCCESS;
  key->CmdFile = str;
  key->CmdLen = strlen(str);
  if (key->Type == KEY_TYPE_AUDIO)
    GetAudioFile(key);
  else if (key->Type == KEY_TYPE_SERVER) {
    key->DefColor = STAT_IDLE_ERROR;
    for (a = 0; a < app->MaxServers; ++a)
      if (app->Servers[a])
	if (app->Servers[a]->Uid == key->Count) {
	  if (app->Servers[a]->Socket)
	    key->DefColor = STAT_IDLE_CMD;
	  break;
	}
  }
  else if (key->Type == KEY_TYPE_EXEC) {
    key->DefColor = STAT_IDLE_CMD;
    if (!(key->CmdTab = LineToTab(key->CmdFile))) {
      a = 9;
      goto end_RetrieveKey;
    }
  }
  else 
    ASSERT(1, "key->Type = inconnu");
  key->Tempo = bpm;
  UNLOCK_KEY(key);
  return (0);
 end_RetrieveKey:
  ResetKey(key);
  UNLOCK_KEY(key);
  PrintFileError(a, file, nb);
  return (0);
}

/* Chargement du fichier de configuration */
void                 LoadConfigFile(const char *file)
{
  FILE               *fd;
  char               *line;
  int                nb, pos;
  
  if (!file) {
    PrintError(10, ERR_TYPE_SIMPLE, 0);
    return;
  }
  LOCK_APP(LockOutput);
  printf("\r%s : '%s' \n", app->Strs[22], file);
  UNLOCK_APP(LockOutput);
  if ((line = malloc(SIZE_BUFFER_LOAD)) == NULL) {
    PrintError(9, ERR_TYPE_ERRNO, 0);
    return;
  }
  if (!(fd = fopen(file, "r"))) {
    PrintError(8, ERR_TYPE_ERRNO, 0);
    free(line);
    return;
  }
  if (fgets(line, SIZE_BUFFER_LOAD, fd))
    if (CheckSig(line)) {
      PrintError(10, ERR_TYPE_SIMPLE, 0);
      goto end_LoadConfigFile;
    }
  for (nb = 2; fgets(line, SIZE_BUFFER_LOAD, fd); ++nb) {
    if (line[0] == '#')
      continue;
    line[SIZE_BUFFER_LOAD - 1] = 0;
    for (pos = 0; line[pos] && (line[pos] == ' ' || line[pos] == '\t'); ++pos);
    if (!line[pos] || line[pos] == '\n')
      continue;
    pos = 0;
    /* Type de configuration */
    switch (line[pos]) {
    case 'K': 
      if (RetrieveKey(line, pos + 1, file, nb))
	goto end_LoadConfigFile;
      continue;
    case 'L':
      RetrieveLinks(line, pos + 1, file, nb);
      continue;
    case 'M':
      RetrieveMode(line, pos + 1, file, nb);
      continue;
    case 'S':
      if (RetrieveServer(line, pos + 1, file, nb))
	goto end_LoadConfigFile;
      continue;
      //case 'G':
    case 'V':
      if (RetrieveVolume(line, pos + 1, file, nb))
	goto end_LoadConfigFile;
      continue;
    default:
      LOCK_APP(LockOutput);
      printf("\033[31m%s\033[00m: %s:%d: %s\n => '%c' %s\n", app->Strs[1], file, nb, app->Strs[11], line[pos], app->Strs[85]);
      UNLOCK_APP(LockOutput);
      continue;
    }
  }
  
 end_LoadConfigFile:
  free(line);
  fclose(fd);
}
