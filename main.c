/*
 *  'launchpadctrl' allows you to use your Novation launchpad (NVLPD01)
 *  Copyright (C) 2014-2017  Vincent Deca. known as Bigcake@ubuntu-fr.org
 *  (user@forum not email)
 *
 *  vincent.deca@neutralite.org
 *
 *  This file is part of 'launchpadctrl'.
 *
 * 'launchpadctrl' is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  'launchpadctrl' is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with 'launchpadctrl'.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app.h"

/* Pointeur vers la structure globale de l'application */
t_app              * restrict app;

/* Affiche une erreur */
void               PrintError(unsigned short err, unsigned char type, const void *arg)
{
  LOCK_APP(LockOutput);
  switch (type) {
  case ERR_TYPE_FILE :
    printf("\r\033[31m%s\033[00m: %s: %s \n", app->Strs[1], (char *) arg, app->Strs[err]);
    break;
  case ERR_TYPE_SIMPLE :
    printf("\r\033[31m%s\033[00m: %s \n", app->Strs[1], app->Strs[err]);
    break;
  case ERR_TYPE_ERRNO :
    printf("\r\033[31m%s\033[00m: %s (%s) \n", app->Strs[1], app->Strs[err], strerror(errno));
    break;
  case ERR_TYPE_KEY :
    printf("\r(%d, %d) : %s ! \n", ((t_key *)arg)->Id % 16 + 1, ((t_key *)arg)->Id / 16 + 1, app->Strs[err]);
    break;
  }
  UNLOCK_APP(LockOutput);
  if (!(app->Flags & APP_FLG_SHELL_CMD))
    PrintLPEntry();
}


/* Ferme l'application en affichant un message d'erreur */
void               ExitWithMsg(const char *msg)
{
  printf("\r\033[31m%s\033[00m: %s (%s) \n", app->Strs[1], msg, strerror(errno));
  exit(EXIT_FAILURE);
}

#ifndef SendToDevice
/* Envoi de 1 a 4 commande(s) au launchpad */
char               SendToDevice(t_lpd *lpd, const unsigned char *cmd, unsigned char nb)
{
  int              rd;

  LOCK_LPD(lpd);
  if (lpd->Fd > 1)
    if ((rd = write(lpd->Fd, cmd, nb)) < nb) {
      UNLOCK_LPD(lpd);
      LOCK_APP(LockOutput);
      printf("\r%s (fd %d, %d, %d): %s \n", app->Strs[33], lpd->Fd, rd, nb, strerror(errno));
      UNLOCK_APP(LockOutput);
      PrintLPEntry();
      CmdClose((void *)1);
      LOCK_APP(LockPonctual);
      if (!(app->Flags & APP_FLG_NO_CHECK))
	if (!lpd->IdCheck) {
	  if (pthread_create(&lpd->IdCheck, &app->Attr, ThreadCheckNewLaunchpad, lpd))
	    PrintError(7, ERR_TYPE_ERRNO, 0);
	  else
	    pthread_detach(lpd->IdCheck);
	}
      UNLOCK_APP(LockPonctual);
      return (0);
    }
  UNLOCK_LPD(lpd);
  return (1);
}
#endif

/* Reveille de l'application */
void               WakeUpApp(int param)
{
  if (!(app->Flags & APP_FLG_NO_SHELL)) {
    struct termios   w;

    tcgetattr(0, &w);
    w.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(0, TCSANOW, &w);
  }
  PrintLPEntry();
}

/* Arrêt du launchpad */
void               QuitApp(int param)
{
  unsigned char    cmd[3], a;
  struct termios   w;
  t_lpd            *lpd;

  cmd[0] = LP_MENU;
  cmd[1] = 0;
  cmd[2] = 0;
  /* on remet ça comme il faut pour bash (et tout autre shell qui ne restore pas automatiquement) */
  tcgetattr(0, &w);
  w.c_lflag |= (ICANON | ECHO);
  tcsetattr(0, TCSANOW, &w);
  if (!(app->Flags & APP_FLG_SHELL_CMD))
    write(1, "\n", 1);
  /* Fermeture propre du port MIDI */
  if (app->MidiHandle) {
    if (app->MidiPort >= 0)
      snd_seq_delete_port(app->MidiHandle, app->MidiPort);
    snd_seq_close(app->MidiHandle);
  }
  /* on éteint les launchpads */
  for (a = 0; a < app->NbLaunchpad; ++a)
    if ((lpd = app->Launchpad[a]))
      if (lpd->Fd > 1)
	SendToDevice(lpd, cmd, 3);
  /* bye bye */
  exit(EXIT_SUCCESS);
}

/* THREAD - Gestion de l'interface graphique * /
void               *ManageGui(void *param)
{
  return (0);
}

/ * THREAD - Gestion du tempo */
void               *ManageGlobalTempo(void *param)
{
  useconds_t       tempo;

  tempo = 499990;
  usleep(tempo);
  while (!(app->Flags & APP_FLG_STOP_TEMPO)) {
    LOCK_APP(LockCond);
    pthread_cond_broadcast(&app->WaitTempo);
    UNLOCK_APP(LockCond);
    usleep(tempo);
  }
  LOCK_APP(LockFlg);
  app->IdTempo = 0;
  app->Flags &= ~(APP_FLG_STOP_TEMPO);
  UNLOCK_APP(LockFlg);
  return (0);
}

/* LOCK KEY - Réinitialisation d'une touche */
void               ResetKey(t_key *key)
{
  unsigned char    a, cmd[3];

  if ((key->lpd->Fd > 1) && (key->ActualColor != STAT_OFF)) {
    cmd[0] = LP_GRID;
    cmd[1] = key->Id;
    cmd[2] = key->ActualColor = key->DefColor = STAT_OFF;
    SendToDevice(key->lpd, cmd, 3);
  }
  for (a = 0; a < KEY_NB_SIMULTANEOUS; ++a)
    if (key->Pid[a]) {
      kill(key->Pid[a], SIGKILL);
      key->Pid[a] = 0;
    }
    else if ((key->Audio) && IN_USE_SOUND(key->Audio, a))
      g_main_loop_quit(key->Audio->Loop);
  if (key->Audio) {
    FreeAudioFile(key->Audio);
    key->Audio = 0;
  }
  if (key->CmdFile) {
    free(key->CmdFile);
    key->CmdFile = 0;
  }
  if (key->CmdTab) {
    free(key->CmdTab);
    key->CmdTab = 0;
  }
  key->CmdLen = 0;
  key->Type = 0;
  key->Link = 0;
  key->Count = 0;
  key->Tempo = 0;
  key->ActualColor = STAT_PUSH_NOT_INIT;
  key->PushedColor = STAT_PUSH_NOT_INIT;
  key->Flags = KEY_FLG_NOT_INIT;
}

/* Mise à jour d'une touche de menu */
void               UpdateMenu(t_lpd *lpd, unsigned char key, unsigned char color)
{
  unsigned char    cmd[3];

  cmd[0] = LP_MENU;
  cmd[1] = key;
  cmd[2] = color;
  SendToDevice(lpd, cmd, 3);
}

/* Mise à jour d'une touche */
void               UpdateKey(t_key *key)
{
  unsigned char    cmd[3];

  cmd[0] = LP_GRID;
  cmd[1] = key->Id;
  cmd[2] = key->DefColor;
  SendToDevice(key->lpd, cmd, 3);
  key->ActualColor = key->DefColor;
}

/* Mise à jour de 2 touches */
void               UpdateTwoKey(t_lpd *lpd, t_key *key1, t_key *key2)
{
  unsigned char    cmd[5];

  cmd[0] = LP_GRID;
  cmd[1] = key1->Id;
  cmd[2] = key1->DefColor;
  cmd[3] = key2->Id;
  cmd[4] = key2->DefColor;
  SendToDevice(lpd, cmd, 5);
  key1->ActualColor = key1->DefColor;
  key2->ActualColor = key2->DefColor;
}

/* Ouverture du périphérique */
char               OpenDevice(t_lpd *lpd, const char *dev)
{
  int              fd;
  unsigned char    cmd[7];

  if ((fd = open(dev, O_RDWR)) < 0) {
    LOCK_APP(LockOutput);
    printf("\r%s '%s' : %s (%s) \n", app->Strs[3], dev, app->Strs[5], strerror(errno));
    UNLOCK_APP(LockOutput);
    if (!(app->Flags & APP_FLG_SHELL_CMD))
      PrintLPEntry();
    return (0);
  }
  LOCK_APP(LockOutput);
  printf("\r%s '%s': %s \n", app->Strs[3], dev, app->Strs[6]);
  UNLOCK_APP(LockOutput);
  if (!(app->Flags & APP_FLG_SHELL_CMD))
    PrintLPEntry();
  if (lpd->DevName) {
    unsigned char a;
    t_key         *grid, *key;

    free(lpd->DevName);
    grid = lpd->Grid[lpd->CurrGrid];
    for (a = 0; a < 72; ++a) {
      key = &grid[a];
      LOCK_KEY(key);
      key->ActualColor = STAT_OFF;
      UNLOCK_KEY(key);
    }
  }
  if ((lpd->DevName = strdup(dev)) == NULL)
    PrintError(9, ERR_TYPE_ERRNO, 0);
  CmdClose(0);
  LOCK_LPD(lpd);
  lpd->Fd = fd;
  UNLOCK_LPD(lpd);
  LOCK_APP(LockPonctual);
  if (!lpd->IdEvent) {
    if (pthread_create(&lpd->IdEvent, &app->Attr, ManageEvents, lpd))
      PrintError(7, ERR_TYPE_ERRNO, 0);
    else
      pthread_detach(lpd->IdEvent);
  }
  UNLOCK_APP(LockPonctual);
  /* Si l'ecriture est fait trop rapidement erreur E/S */
  usleep(10000);
  /* Ne fonctionne qu'avec mon driver >= v0.28 */
  /* Requête de récupération de la version du driver */
  cmd[0] = LP_OPTION;
  cmd[1] = LP_GET_VERSION;
  cmd[2] = LP_MENU;
  cmd[3] = 0;
  cmd[4] = 0;
  cmd[5] = LP_OPTION;
  cmd[6] = LP_GET_STAT;
  SendToDevice(lpd, cmd, 7);
  return (1);
}

/* Allumage de la grille à la couleur par défaut */
void              LightGridDefColor(t_lpd *lpd, t_key *grid)
{
  unsigned char   a, flg, cmd[6];
  t_key           *key;

  if (lpd->Fd < 2)
    return;
  cmd[0] = cmd[3] = LP_GRID;
  for (flg = a = 0; a < 72; ++a) {
    key = &grid[a];
    LOCK_KEY(key);
    if (key->DefColor != key->ActualColor) {
      if (!flg) {
	cmd[1] = key->Id;
	cmd[2] = key->DefColor;
	flg = 1;
      }
      else if (flg == 1) {
	cmd[4] = key->Id;
	cmd[5] = key->DefColor;
	flg = 2;
      }
      key->ActualColor = key->DefColor;
      if (flg == 2) {
	SendToDevice(lpd, cmd, 6);
	flg = 0;
      }
    }
    UNLOCK_KEY(key);
  }
  if (flg == 1)
    SendToDevice(lpd, cmd, 3);
}

/* Mise à jour de toute la grille */
void                 UpdateGrid(t_lpd *lpd, unsigned char old, unsigned char new)
{
  unsigned char      cmd[6];
  unsigned char      a, flg;
  t_key              *grid0, *grid1, *key;

  grid0 = lpd->Grid[old];
  grid1 = lpd->Grid[new];
  cmd[0] = cmd[3] = LP_GRID;
  for (flg = a = 0; a < 72; ++a) {
    key = &grid1[a];
    LOCK_KEY(key);
    if (grid0[a].ActualColor != key->DefColor) {
      if (!flg) {
	cmd[1] = key->Id;
	cmd[2] = key->DefColor;
	flg = 1;
      }
      else if (flg == 1) {
	cmd[4] = key->Id;
	cmd[5] = key->DefColor;
	flg = 2;
      }
      key->ActualColor = key->DefColor;
      if (flg == 2) {
	SendToDevice(lpd, cmd, 6);
	flg = 0;
      }
    }
    UNLOCK_KEY(key);
  }
  if (flg == 1)
    SendToDevice(lpd, cmd, 3);
}

/* Initialisation du shell */
static void        InitShell()
{
  if ((app->ShellHistory = calloc(1, sizeof(char *) * (DEFAULT_NB_HISTORY + 1))) == NULL)
    goto end_InitShell;
  if ((app->ShellBuffer = malloc(SIZE_BUFFER_SHELL + 1)) == NULL)
    goto end_InitShell;
  if (pthread_create(&app->IdShell, &app->Attr, ManageShell, 0))
    goto end_InitShell;
  pthread_detach(app->IdShell);
  signal(SIGCONT, WakeUpApp);
  return;
 end_InitShell:
  if (app->ShellHistory) free(app->ShellHistory);
  if (app->ShellBuffer) free(app->ShellBuffer);
  app->ShellBuffer = 0;
  app->ShellHistory = 0;
  app->Flags |= APP_FLG_NO_SHELL;
  PrintError(84, ERR_TYPE_ERRNO, 0);
}

/* Initialisation de l'application */
static void          InitApp()
{
  pthread_condattr_t attr;

  /* Initialisation de base */
  if ((app = calloc(1, sizeof(t_app))) == NULL)
    ExitWithMsg(app->Strs[0]);
  /* Initialisation d'un premier launchpad */
  if (!InitLaunchpad())
    ExitWithMsg(app->Strs[0]);
  /* Initialisation de la langue */
  if (!strncasecmp(getenv("LANG"), "fr_fr.", 6))
    app->Strs = FrenchTab;
  else
    app->Strs = EnglishTab;
  /* Autres initialisations*/
  pthread_attr_init(&app->Attr);
  pthread_condattr_init(&attr);
  pthread_cond_init(&app->WaitTempo, &attr);
  pthread_mutex_init(&app->LockCond, NULL);
  pthread_mutex_init(&app->LockFlg, NULL);
  pthread_mutex_init(&app->LockOutput, NULL);
  pthread_mutex_init(&app->LockPonctual, NULL);
  signal(SIGINT, QuitApp);
  srand(time(NULL) * getpid());
  InitSoundSystem();
}

/* Ouverture automatique au démarrage - ne vérifie que les 10 premiers /dev/nlpX */
void               *ThreadCheckNewLaunchpad(void *param)
{
  struct stat      st;
  char             a, dev[9];
  t_lpd            *lpd = param;

  if (!lpd)
    if ((lpd = InitLaunchpad())) {
      PrintError(9, ERR_TYPE_ERRNO, 0);
      return (0);
    }
  memcpy(dev, "/dev/nlp0", 9);
  while (1) {
    for (a = '0'; a <= '9'; ++a) {
      dev[8] = a;
      if (!stat(dev, &st)) {
	/*//# Parfois le système n'a pas le temps de changer les droits sur /dev/nlpX */
	usleep(100000);
	OpenDevice(lpd, dev);
	if (!(app->Flags & APP_FLG_MIDI_MODE)) {
	  PrintLPEntry();
	  if (!(lpd->Flags & (LPD_FLG_SESSION | LPD_FLG_USER1 | LPD_FLG_USER2 | LPD_FLG_MIXER)))
	    lpd->Flags |= LPD_FLG_SESSION;
	  if (lpd->Flags & LPD_FLG_SESSION) {
	    LightMenuMode(lpd, LP_TOP_SESSION);
	    if (lpd->CurrGrid < 9)
	      LightMenuArrows(lpd, lpd->CurrGrid + 1);
	  }
	  else if (lpd->Flags & LPD_FLG_USER1)
	    LightMenuMode(lpd, LP_TOP_USER1);
	  else if (lpd->Flags & LPD_FLG_USER2)
	    LightMenuMode(lpd, LP_TOP_USER2);
	  else if (lpd->Flags & LPD_FLG_MIXER)
	    LightMenuMode(lpd, LP_TOP_MIXER);
	  LightGridDefColor(lpd, lpd->Grid[lpd->CurrGrid]);
	  UpdateGrid(lpd, lpd->CurrGrid, lpd->CurrGrid);
	}
	LOCK_APP(LockPonctual);
	lpd->IdCheck = 0;
	UNLOCK_APP(LockPonctual);
	return (0);
      }
    }
    sleep(1);
  }
  return (0);
}

/* Se connecte a tout les serveurs */
void                    ConnectToAllServers()
{
  unsigned short        a;
  t_svr                 *svr;
  char                  flg;

  if (app->Servers) {
    for (flg = 0, a = 0; a < app->MaxServers; ++a) {
      svr = app->Servers[a];
      if (svr) {
	LOCK_MUTEX(svr->Lock);
	if (!svr->Socket) {
	  flg = 1;
	  svr->Socket = ConnectToServer(svr->Host, svr->Port);
	  /* Envoi des données au serveur */
	  if (svr->Socket) {
	    if (svr->Size) {
	      if (send(svr->Socket, svr->Buff, svr->Size, 0) < svr->Size) {
		PrintError(101, ERR_TYPE_ERRNO, 0);
		svr->Socket = 0;
	      }
	      else
		UpdateRelatedKeys(svr);
	    }
	    else
	      UpdateRelatedKeys(svr);
	  }
	}
	UNLOCK_MUTEX(svr->Lock);
      }
    }
    LOCK_APP(LockPonctual);
    if (!app->IdServer && flg) {
      if (pthread_create(&app->IdServer, &app->Attr, ThreadSurveyServer, 0))
	PrintError(7, ERR_TYPE_ERRNO, 0);
      else
	pthread_detach(app->IdServer);
    }
    UNLOCK_APP(LockPonctual);
  }
}

/* Lancement des threads : graphique, capture, tempo, shell */
static void             LaunchThreads()
{
  unsigned char         a;

  /* Thread de capture des évenements sur les launchpads */
  for (a = 0; a < app->NbLaunchpad; a++)
    if (!app->Launchpad[a]->IdEvent) {
      if (pthread_create(&app->Launchpad[a]->IdEvent, &app->Attr, ManageEvents, app->Launchpad[a]))
	ExitWithMsg(app->Strs[7]);
      pthread_detach(app->Launchpad[a]->IdEvent);
    }
  /* Thread d'auto-connexion */
  if (!(app->Flags & APP_FLG_NO_CHECK))
    for (a = 0; a < app->NbLaunchpad; a++)
      if (!app->Launchpad[a]->DevName) {
	if (pthread_create(&app->Launchpad[a]->IdCheck, &app->Attr, ThreadCheckNewLaunchpad, app->Launchpad[a]))
	  PrintError(7, ERR_TYPE_ERRNO, 0);
	else
	  pthread_detach(app->Launchpad[a]->IdCheck);
      }
  /* Thread graphique * /
  if (pthread_create(&app->IdGui, &app->Attr, ManageGui, 0))
    ExitWithMsg(app->Strs[7]);
    pthread_detach(app->IdGui);
  */
  ConnectToAllServers();
}

/* Initialisation du launchpad */
static void         InitLaunchpadLights()
{
  unsigned char     cmd[3], a;
  t_lpd             *lpd;

  cmd[0] = LP_MENU;
  cmd[1] = 0;
  cmd[2] = 0;
  for (a = 0; a < app->NbLaunchpad; a++)
    if ((lpd = app->Launchpad[a]))
      if (lpd->Fd > 1) {
	SendToDevice(lpd, cmd, 3);
	LightMenuMode(lpd, LP_TOP_SESSION);
	LightMenuArrows(lpd, 1);
	LightGridDefColor(lpd, lpd->Grid[lpd->CurrGrid]);
      }
}

/* Fonction main */
int                main(int ac, char **av)
{
  InitApp();
  GetOptions(ac, av);
  if (app->Flags & APP_FLG_MIDI_MODE)
    InitMidi();
  if (!(app->Flags & APP_FLG_NO_SHELL))
    InitShell();
  if ((app->Flags & APP_FLG_NO_CHECK) && !(app->Flags & APP_FLG_MIDI_MODE))
    InitLaunchpadLights();
  LaunchThreads();
  while (1)
    sleep(50000);
  return (0);
}
