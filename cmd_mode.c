/*
 *  'launchpadctrl' allows you to use your Novation launchpad (NVLPD01)
 *  Copyright (C) 2014-2017  Vincent Deca. known as Bigcake@ubuntu-fr.org
 *  (user@forum not email)
 *
 *  vincent.deca@neutralite.org
 *
 *  This file is part of 'launchpadctrl'.
 *
 * 'launchpadctrl' is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  'launchpadctrl' is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with 'launchpadctrl'.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app.h"

/* Désactive le mode de dessin */
void                 DisableDrawMode(t_lpd *lpd)
{
  unsigned char      cmd[3];
  unsigned char      a;

  lpd->Flags = LPD_FLG_IN_USE | LPD_FLG_USER1;
  cmd[0] = LP_MENU;
  cmd[1] = 0;
  cmd[2] = 0;
  SendToDevice(lpd, cmd, 3);
  for (a = 0; a < 72; ++a)
    lpd->Grid[0][a].ActualColor = 0;
  ChangeGrid(lpd, 0);
  LightMenuArrows(lpd, 1);
  LightMenuMode(lpd, LP_TOP_SESSION);
  printf("%s\n", app->Strs[174]);
}

/* Initialise la grille temporaire */
static unsigned char   InitDrawGrid(t_lpd *lpd)
{
  unsigned char        a;
  t_key                *key;

  if ((lpd->LightGrid = calloc(72, sizeof(t_key))) == NULL) {
    PrintError(9, ERR_TYPE_ERRNO, 0);
    return (1);
  }
  for (a = 0; a < 72; ++a) {
    key = &lpd->LightGrid[a];
    key->Id = (a / 9) * 16 + (a % 9);
    key->lpd = lpd;
    pthread_mutex_init(&key->Lock, NULL);
  }
  lpd->TempColor = LP_FULL_GREEN;
  lpd->Mixer.Flags &= ~(LPD_MIXER_2ND_PUSH | LPD_MIXER_VOL);
  return (0);
}

/* Initialisation des lumières du launchpad */
static void           InitDrawMode(t_lpd *lpd)
{
  unsigned char       cmd[8];

  lpd->Flags &= ~(LPD_FLG_USER1 | LPD_FLG_USER2);

  lpd->LightGrid[SIDE_VOLUME].DefColor = LP_FULL_YELLOW;
  lpd->LightGrid[SIDE_PAN].DefColor = LP_MED_YELLOW;
  lpd->LightGrid[SIDE_SNDA].DefColor = LP_LOW_YELLOW;
  lpd->LightGrid[SIDE_SNDB].DefColor = LP_LOW_ORANGE;
  lpd->LightGrid[SIDE_STOP].DefColor = LP_MED_ORANGE;
  lpd->LightGrid[SIDE_TRKON].DefColor = LP_FULL_ORANGE;
  lpd->LightGrid[SIDE_ARM].DefColor = LP_FULL_GREEN;

  cmd[0] = LP_MENU;
  cmd[1] = 0;
  cmd[2] = 0;
  cmd[3] = LP_TOP_LEARN;
  cmd[4] = LP_FULL_GREEN;
  cmd[5] = LP_TOP_VIEW;
  cmd[6] = LP_MED_GREEN;
  SendToDevice(lpd, cmd, 7);

  cmd[0] = LP_TOP_PAGE_L;
  cmd[1] = LP_LOW_GREEN;
  cmd[2] = LP_TOP_PAGE_R;
  cmd[3] = LP_LOW_RED;
  cmd[4] = LP_TOP_SESSION;
  cmd[5] = LP_MED_RED;
  cmd[6] = LP_TOP_USER1;
  cmd[7] = LP_FULL_RED;
  SendToDevice(lpd, cmd, 8);

  cmd[0] = LP_TOP_MIXER;
  cmd[1] = LP_FULL_ORANGE;
  cmd[2] = LP_GRID;
  cmd[3] = LP_LEFT_VOL;
  cmd[4] = LP_FULL_YELLOW;
  cmd[5] = LP_LEFT_PAN;
  cmd[6] = LP_MED_YELLOW;
  SendToDevice(lpd, cmd, 7);

  cmd[0] = LP_LEFT_SNDA;
  cmd[1] = LP_LOW_YELLOW;
  cmd[2] = LP_LEFT_SNDB;
  cmd[3] = LP_LOW_ORANGE;
  cmd[4] = LP_LEFT_STOP;
  cmd[5] = LP_MED_ORANGE;
  cmd[6] = LP_LEFT_TRKON;
  cmd[7] = LP_FULL_ORANGE;
  SendToDevice(lpd, cmd, 8);

  cmd[0] = LP_LEFT_ARM;
  cmd[1] = lpd->TempColor;
  SendToDevice(lpd, cmd, 2);
  printf("%s\n", app->Strs[149]);
}

/* commande 'mode' */
void             CmdMode(char *param)
{
  t_lpd          *lpd;
  char           flg = 0;

  lpd = app->Launchpad[app->CurrLpd];
  if (!param || (*param == '?')) {
    CmdHelp("mode");
    return;
  }
  if (!strncmp(param, "draw", 4)) {
    if (lpd->Flags & LPD_FLG_DRAW_MODE) {
      DisableDrawMode(lpd);
    }
    else {
      lpd->Flags |= LPD_FLG_DRAW_MODE;
      if (!lpd->LightGrid)
        if (InitDrawGrid(lpd))
          return;
      InitDrawMode(lpd);
    }
  }
  else if (!strncmp(param, "random", 6)) {
    LOCK_LPD(lpd);
    if (lpd->Flags & LPD_FLG_RANDOM_USER) {
      lpd->Flags &= ~(LPD_FLG_RANDOM_USER);
      printf("%s\n", app->Strs[176]);
    }
    else {
      lpd->Flags |= LPD_FLG_RANDOM_USER;
      printf("%s\n", app->Strs[175]);
    }
    UNLOCK_LPD(lpd);
  }
  else if (!strncmp(param, "tempo", 5)) {
    if (!app->IdTempo) {
      if (pthread_create(&app->IdTempo, &app->Attr, ManageGlobalTempo, 0)) {
	PrintError(7, ERR_TYPE_SIMPLE, 0);
	return;
      }
      pthread_detach(app->IdTempo);
      printf("%s\n", app->Strs[178]);
    }
    else {
      LOCK_APP(LockFlg);
      app->Flags |= APP_FLG_STOP_TEMPO;
      UNLOCK_APP(LockFlg);
      printf("%s\n", app->Strs[179]);
    }
  }
  else if (!strncmp(param, "list", 4)) {
    if (lpd->Flags & LPD_FLG_DRAW_MODE) {
      flg = 1;
      printf("- %s\n", app->Strs[149]);
    }
    if (lpd->Flags & LPD_FLG_RANDOM_USER) {
      flg = 1;
      printf("- %s\n", app->Strs[175]);
    }
    if (app->Flags & APP_FLG_QUIET) {
      flg = 1;
      printf("- %s\n", app->Strs[189]);
    }
    if (app->IdTempo) {
      flg = 1;
      printf("- %s\n", app->Strs[178]);
    }
    if (!flg)
      printf("%s\n", app->Strs[181]);
  }
  else if (!strncmp(param, "quiet", 5)) {
    if (!(app->Flags & APP_FLG_QUIET)) {
      close(2);
      if (open("/dev/null", O_RDWR) < 0)
	PrintError(155, ERR_TYPE_ERRNO, 0);
      else {
	LOCK_APP(LockFlg);
	app->Flags |= APP_FLG_QUIET;
	UNLOCK_APP(LockFlg);
	printf("%s\n", app->Strs[189]);
      }
    }
    else
      printf("%s\n", app->Strs[189]);
  }
  else
    CmdHelp("mode");
}
