/*
 *  'launchpadctrl' allows you to use your Novation launchpad (NVLPD01)
 *  Copyright (C) 2014-2017  Vincent Deca. known as Bigcake@ubuntu-fr.org
 *  (user@forum not email)
 *
 *  vincent.deca@neutralite.org
 *
 *  This file is part of 'launchpadctrl'.
 *
 * 'launchpadctrl' is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  'launchpadctrl' is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with 'launchpadctrl'.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app.h"

/* Libération d'une structure de launchpad  */
void                  FreeLaunchpad(t_lpd *lpd)
{
  unsigned char       a;
  
  /* Libération du launchpad */
  pthread_mutex_destroy(&lpd->Lock);
  for (a = 0; a <= GRID_VOLUME; ++a) {
    pthread_mutex_destroy(&lpd->Grid[a]->Lock);
    free(lpd->Grid[a]);
  }
  free(lpd->Grid);
  free(lpd);
}

/* Recherche d'une structure de launchpad disponible */
t_lpd                 *GetLaunchpadStruct()
{
  t_lpd              **tab;
  unsigned char      a;

  tab = app->Launchpad;
  for (a = 0; a < app->NbLaunchpad; ++a)
    if (!(tab[a]->Flags & LPD_FLG_IN_USE))
      return (tab[a]);
  return (0);
}

/* Initialisation d'une structure de launchpad */
t_lpd                 *InitLaunchpad()
{
  unsigned char       a, b;
  t_key               *key;
  t_lpd               *lpd;

  /* Initialisation de base */
  if ((lpd = calloc(1, sizeof(t_lpd))) == NULL)
    return (0);
  /* Initialisation des grilles */
  if ((lpd->Grid = malloc(sizeof(t_lpd *) * GRID_NUMBER)) == NULL) {
    free(lpd);
    return (0);
  }
  for (a = 0; a < GRID_VOLUME; ++a) {
    if ((lpd->Grid[a] = calloc(72, sizeof(t_key))) == NULL) {
      if (a) {
	for (--a; a; --a)
	  free(lpd->Grid[a]);
	free(lpd->Grid[a]);
      }
      free(lpd->Grid);
      free(lpd);
      return (0);
    }
    for (b = 0; b < 72; ++b) {
      key = &lpd->Grid[a][b];
      if ((b % 9) == 8)
        key->PushedColor = STAT_PUSH_SUCCESS;
      else
        key->PushedColor = STAT_PUSH_NOT_INIT;
      key->DefColor = STAT_OFF;
      key->Flags = KEY_FLG_NOT_INIT;
      key->Id = (b / 9) * 16 + (b % 9);
      key->lpd = lpd;
      pthread_mutex_init(&key->Lock, NULL);
    }
  }
  /* Initialisation du mixer - page du volume par défaut */
  lpd->Mixer.Flags = LPD_MIXER_VOL;
  InitMixerVolume(lpd);
  /* Autres initialisations*/
  lpd->Stat = LP_MENU;
  lpd->TempColor = LP_FULL_GREEN;
  pthread_mutex_init(&lpd->Lock, NULL);
  LOCK_APP(LockPonctual);
  if (AddLaunchpad(lpd)) {
    UNLOCK_APP(LockPonctual);
    FreeLaunchpad(lpd);
    return (0);
  }
  UNLOCK_APP(LockPonctual);
  return (lpd);
}

/* Ajout d'une structure de launchpad dans le tableau */
unsigned char        AddLaunchpad(t_lpd *lpd)
{
  t_lpd              **tab;
  unsigned char      a;

  tab = app->Launchpad;
  if (app->NbLaunchpad + 1 > APP_LIMIT_UCHAR) {
    PrintError(95, ERR_TYPE_SIMPLE, app->Strs[3]);
    return (1);
  }
  /* Recherche d'un place libre dans le tableau */
  tab = app->Launchpad;
  for (a = 0; a < app->NbLaunchpad; ++a)
    if (!(tab[a]->Flags & LPD_FLG_IN_USE)) {
      tab[a] = lpd;
      return (1);
    }
  /* Si le tableau n'a pas été créé, on le créer */
  if (!tab) {
    if ((tab = calloc(1, sizeof(t_svr *))) == NULL)
      return (1);
  }
  /* Sinon on l'agrandit */
  else if (a == app->NbLaunchpad) {
    if ((tab = calloc(app->NbLaunchpad + 1, sizeof(t_svr *))) == NULL)
      return (1);
    memcpy(tab, app->Launchpad, sizeof(t_svr *) * a);
    free(app->Launchpad);
  }
  ++app->NbLaunchpad;
  tab[a] = lpd;
  app->Launchpad = tab;
  return (0);
}
