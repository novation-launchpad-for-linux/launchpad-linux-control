/*
 *  'launchpadctrl' allows you to use your Novation launchpad (NVLPD01)
 *  Copyright (C) 2014-2017  Vincent Deca. known as Bigcake@ubuntu-fr.org
 *  (user@forum not email)
 *
 *  vincent.deca@neutralite.org
 *
 *  This file is part of 'launchpadctrl'.
 *
 * 'launchpadctrl' is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  'launchpadctrl' is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with 'launchpadctrl'.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app.h"

/* Commande 'link' */
void             CmdLink(char *param)
{
  //  t_lpd          *lpd;
  t_key          *key1, *key2;

  //lpd = app->Launchpad[app->CurrLpd];
  if (!param || (*param == '?')) {
    CmdHelp("link");
    return;
  }
  if (!(param = GetKeyFromParam(app->Launchpad[app->CurrLpd], param, &key1, 1)))
    return;
  for (; *param && (*param == ' ' || *param == '\t'); ++param);
  if (!(*param)) {
    CmdHelp("link");
    return;
  }
  if (!(param = GetKeyFromParam(app->Launchpad[app->CurrLpd], param, &key2, 1)))
    return;
  if (key1->Id == key2->Id) {
    PrintError(162, ERR_TYPE_SIMPLE, 0);
    return;
  }
  for (; *param && (*param == ' ' || *param == '\t'); ++param);
  if (!(*param)) {
    CmdHelp("link 1");
    return;
  }
  if (key1->Link) {
    key1->Link->Link = 0;
    LOCK_KEY(key1->Link);
    key1->Link->Flags &= ~(KEY_FLG_LNK_SIMUL | KEY_FLG_LNK_LOOP);
    UNLOCK_KEY(key1->Link);
  }
  key1->Link = key2;
  LOCK_KEY(key1);
  key1->Flags &= ~(KEY_FLG_LNK_SIMUL | KEY_FLG_LNK_LOOP);
  UNLOCK_KEY(key1);
  if (key2->Link) {
    key2->Link->Link = 0;
    LOCK_KEY(key2->Link);
    key2->Link->Flags &= ~(KEY_FLG_LNK_SIMUL | KEY_FLG_LNK_LOOP);
    UNLOCK_KEY(key2->Link);
  }
  key2->Link = key1;
  LOCK_KEY(key2);
  key2->Flags &= ~(KEY_FLG_LNK_SIMUL | KEY_FLG_LNK_LOOP);
  UNLOCK_KEY(key2);
  if (!strncmp(param, "single", 6)) {
    if ((key1->Type == KEY_TYPE_SERVER) && (key2->Type == KEY_TYPE_SERVER)) {
      PrintError(190, ERR_TYPE_SIMPLE, 0);
      return;
    }
  }
  else if (!strncmp(param, "break", 5)) {
    key1->Link = 0;
    key2->Link = 0;
  }
  else if (!strncmp(param, "simul", 5)) {
    LOCK_KEY(key1);
    key1->Flags |= KEY_FLG_LNK_SIMUL;
    UNLOCK_KEY(key1);
    LOCK_KEY(key2);
    key2->Flags |= KEY_FLG_LNK_SIMUL;
    UNLOCK_KEY(key2);
  }
  else if (!strncmp(param, "loop", 4)) {
    if ((key1->Type == KEY_TYPE_SERVER) && (key2->Type == KEY_TYPE_SERVER)) {
      PrintError(190, ERR_TYPE_SIMPLE, 0);
      return;
    }
    LOCK_KEY(key1);
    key1->Flags |= KEY_FLG_LNK_LOOP;
    UNLOCK_KEY(key1);
    LOCK_KEY(key2);
    key2->Flags |= KEY_FLG_LNK_LOOP;
    UNLOCK_KEY(key2);
  }
  else
    CmdHelp("link 1");
}
